import pickle
import numpy as np
import gensim
from scipy import sparse
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Extract Topics given a list of wiki titles.')
parser.add_argument('-i','--input', required=False, default=None, type=str, help = 'input list of titles, this can be pickle objct(with .pickle ext) or a text file with one column.')
parser.add_argument('-o','--output', required=True, type=str, help = 'output sparse matrix.')
parser.add_argument('-m','--model', required=True, type=str, help = 'LDA model.')
parser.add_argument('-b','--bow', required=True, type=str, help = '_bow file, mapping documents to word vectors.')
parser.add_argument('-w','--wordids', required=True, type=str, help = 'Wordids produced by parsing the corpus.')
parser.add_argument('-c','--corpus', required=True, type=str, help = 'original corpus path')

if __name__ == '__main__':
  args = parser.parse_args()
  
  if args.input.endswith(".pickle"):
    fi_arr = open(args.input,'rb')
    data_arr = pickle.load(fi_arr)
  else:
    fi_arr = open(args.input,'r',encoding='utf8')
    data_arr = pd.DataFrame(fi_arr.read().split("\n"),columns=['titles'])
  if args.corpus.endswith('bz2'):
    import bz2
    args.corpus=bz2.open(args.corpus)
    
  #mm = gensim.corpora.MmCorpus(args.bow)
  dictionary = gensim.corpora.Dictionary.load_from_text(args.wordids)
  model = gensim.models.LdaModel.load(args.model)
  rows = []
  cols = []
  data = []
  ind = 0
  accepted = 0
  for title, content, pageid  in gensim.corpora.wikicorpus.extract_pages(args.corpus):
    if (title == data_arr['titles']).sum() > 0:
      new_vec = dictionary.doc2bow(content.lower().split())
      for k in model[new_vec]:
        rows.append(np.nonzero(data_arr['titles']==title)[0][0])
        cols.append(k[0])
        data.append(k[1])
      accepted += 1
      print(title,accepted)
    if accepted == data_arr['titles'].shape[0]:
      break
    ind += 1
  array = sparse.csr_matrix((data,(rows,cols)),shape=(max(rows)+1,model.num_topics))
  with open(args.output,'wb') as fi:
    pickle.dump((array,np.asarray(data_arr['titles'])),fi)
