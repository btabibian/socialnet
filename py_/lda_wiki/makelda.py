""" 
   This scrip makes an lda model from an already preprcoessed dataset. For more info visit:
   https://radimrehurek.com/gensim/wiki.html
"""

import argparse
import os
import pandas as pd
import datetime
import logging, gensim, bz2

parser = argparse.ArgumentParser(description='This creates a lda model from an already prepared corpus.')
parser.add_argument('-i','--input', type=str, required=True,
                   help = 'Input directory of processed data')
parser.add_argument('-o','--output', type=str,required=True,
                   help = 'output directory of the model')
parser.add_argument('-t','--num_topics', type=int,default=100,
                   help='number of topics')
parser.add_argument('-p','--passes', type=int,default=20,help = 'number of passes over data')
parser.add_argument('-u','--updates', type=int,default=0,help = 'number of chucks to consume before every update, 0 means not online')
parser.add_argument('-c','--chunks', type=int,default=10000,help = 'number of chucks to consume before every update.')
parser.add_argument('--logging',default='W',type=str,help = 'logging level, can be [W]arning,[E]rror,[D]ebug,[I]nfo,[C]ritical')
parser.add_argument('--logging_dir',default='./experiments/',help='path for storing log files')

if __name__ == "__main__":
  args = parser.parse_args()
  level = logging.WARNING
  if args.logging == 'D':
    level = logging.DEBUG
  if args.logging == 'I':
    level = logging.INFO
  if args.logging == 'W':
    level = logging.WARNING
  if args.logging == 'E':
    level = logging.ERROR
  if args.logging == 'C':
    level = logging.CRITICAL
  logging.basicConfig(filename=os.path.join(args.logging_dir,"lda_wiki"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+".log"),level=level)
  root = logging.getLogger() 
  id2word = gensim.corpora.Dictionary.load_from_text(os.path.join(args.input,'_wordids.txt'))
  mm = gensim.corpora.MmCorpus(os.path.join(args.input,'_tfidf.mm'))
  root.info(mm)
  root.info("training lda model")
  if args.updates > 0:
    lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=args.num_topics, update_every=args.updates, chunksize=args.chunks, passes=args.passes)
  else:
    lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=args.num_topics, update_every= 0, passes=args.passes)
  root.info("Storing model.")
  lda.save(os.path.join(args.output,datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+"_"+"lda"))



