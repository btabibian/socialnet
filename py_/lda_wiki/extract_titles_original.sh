source venv_new/bin/activate
python -m lda_wiki.extract_titles_original -i ./lda_wiki/output_3/_titles.txt \
                                         -o ./lda_wiki/116k.pickle \
                                        -m ./lda_wiki/subset/2016-01-28_18-16_lda \
                                        -b ./lda_wiki/output_3/_bow.mm \
                                        -w ./lda_wiki/output_3/_wordids.txt \
                                        -c ./lda_wiki/enwiki-20140203-pages-articles.xml.bz2
deactivate

