import pickle
import numpy as np
import gensim
from scipy import sparse
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Extract Topics given a list of wiki titles.')
parser.add_argument('-i','--input', required=False, default=None, type=str, help = 'input list of titles, this can be pickle objct(with .pickle ext) or a text file with one column.')
parser.add_argument('-o','--output', required=True, type=str, help = 'output sparse matrix.')
parser.add_argument('-m','--model', required=True, type=str, help = 'LDA model.')
parser.add_argument('-t','--titles', required=True, type=str, help = 'Corpus titles.')
parser.add_argument('-b','--bow', required=True, type=str, help = '_bow file, mapping documents to word vectors.')
parser.add_argument('-w','--wordids', required=True, type=str, help = 'Wordids produced by parsing the corpus.')

if __name__ == '__main__':
  args = parser.parse_args()
  
  titles = np.array(open(args.titles,encoding="utf8").read().split("\n"))
  if args.input is None:
    data_arr = pd.DataFrame(titles,columns=['titles'])
  elif args.input.endswith(".pickle"):
    fi_arr = open(args.input,'rb')
    data_arr = pickle.load(fi_arr)
  else:
    fi_arr = open(args.input,'r',encoding='utf8')
    data_arr = pd.DataFrame(fi_arr.read().split("\n"),columns=['titles'])
    
  mm = gensim.corpora.MmCorpus(args.bow)
  model = gensim.models.LdaModel.load(args.model)
  rows = []
  cols = []
  data = []
  ind = 0
  accepted = 0
  for d in mm:
    if (titles[ind] == data_arr['titles']).sum() > 0:
      for k in model[d]:
        rows.append(np.nonzero(data_arr['titles']==titles[ind])[0][0])
        cols.append(k[0])
        data.append(k[1])
      accepted += 1
      print(titles[ind],accepted)
    if accepted == data_arr['titles'].shape[0]:
      break
    ind += 1
  array = sparse.csr_matrix((data,(rows,cols)),shape=(max(rows)+1,model.num_topics))
  with open(args.output,'wb') as fi:
    pickle.dump((array,data_arr['titles']),fi)
