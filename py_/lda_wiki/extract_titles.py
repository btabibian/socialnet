import gensim
import argparse

parser = argparse.ArgumentParser(description='Extract titles of documents parsed by gensim.')
parser.add_argument('-i','--input', required=True, type=str, help = 'Input wiki corpus')
parser.add_argument('-o','--output', required=True, type=str, help = 'output file')
parser.add_argument('-w','--wordids', required=True, type=str, help = 'Wordids produced by parsing the corpus.')

if __name__ == '__main__':
  args = parser.parse_args()
  fi_titles = open(args.output,'w')
  id2word = gensim.corpora.Dictionary.load_from_text(args.wordids)
  wiki = gensim.corpora.wikicorpus.WikiCorpus(args.input,dictionary=id2word)
  wiki.metadata = True
  for i in wiki.get_texts():
    fi_titles.write(i[1][1]+"\n")
  fi_titles.close()