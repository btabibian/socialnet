source venv_new/bin/activate
python -m lda_wiki.extract_titles_topic -o ./lda_wiki/116k.pickle \
                                        -m ./lda_wiki/subset/2016-01-28_18-16_lda \
                                        -t ./lda_wiki/output_3/_titles.txt \
                                        -b ./lda_wiki/output_3/_bow.mm \
                                        -w ./lda_wiki/output_3/_wordids.txt
deactivate

