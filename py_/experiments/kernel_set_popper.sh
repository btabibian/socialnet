doc_size=(40 60 80 100 140 200 250) 
source venv/bin/activate
index=$1
python -m experiment kernel -n 'popper' -s 500 -d 500 -i 10 -t ${doc_size[$(($index % ${#doc_size[@]}))]}  -B 1.2 -A 3.0 -K 4 -L 40 -o ./outputs_popper/
deactivate
