source venv_new/bin/activate
count_size=(10 20 30 40 100 200 300 400 500 600 700 800)
#count_size=(1 2 3)
index=$1
step=10
replication=$1
mkdir ./arrival_bernoulli_synthetic_cleaned/s$replication
for count in ${count_size[@]} ; do
  total=$(($count*step))
  python -m model.clean_data -i ./arrival_bernoulli_synthetic/s$replication/syn_merged_all-d-$total -o ./arrival_bernoulli_synthetic_cleaned/s$replication/syn_merged_all-d-$total
done
deactivate
