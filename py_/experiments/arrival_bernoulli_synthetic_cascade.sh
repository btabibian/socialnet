source venv_new/bin/activate
seed_1=61937 
seed_2=61204
seed_3=32192
export OMP_NUM_THREAD=3

cascades=(1 2 3 4 10 20 40 60 80 100) 
#cascades=$1
replication=$(($1 / ${#cascades[@]}))
cascade=${cascades[$(($1 % ${#cascades[@]}))]}
echo replication: $replication
echo cascade: $cascade

#kernels
K=1
#kernels scale
L=1.9
L_min=1.8

t=10            #time
s=1            #Number of source
topics=1        #number of topics
        # Document   ; Source
#Arrival 
         P=1.0        ; G=0.3
         P_min=1.0    ; G_min=0.07
#Removal
         B=3.0        ; A=1.0
         B_min=1.0    ; A_min=0.7

Arrival_bandwidth=3.0

#clear directory

#rm ./outputs_bernoulli_param_rel/*
#rm ./outputs_bernoulli_param_arr/*
#set -e
#iterate over document sizes
#for replication in $(seq 0 $replications); do
#mkdir ./outputs_bernoulli_param_rel/s$replication/
#mkdir ./outputs_bernoulli_param_arr/s$replication/
#doc=1
doc=1
#cascade=1
#cascade=1
#for cascade in ${cascades[@]}; do

mkdir ./outputs_bernoulli_param_rel_small/s$s/
mkdir ./outputs_bernoulli_param_arr_small/s$s/


    index=$doc
#for every  size fix site parameters
    seed_index=$(($index + $replication + $seed_1))
    seed_2=$(($replication+$seed_2))
    seed_3=$(($seed_1+$replication))
    python -m experiment arrival -n 'test' -s $s --verbose --logging I -t $t --seed $seed_index\
                             -K $K -L $L --Length_min $L_min -d $doc                                   -o ./outputs_bernoulli_param_arr_small/s$s/ --print\
                             -P $P --Phi_min $P_min -G $G --Gamma_min $G_min --Arrival_bandwidth $Arrival_bandwidth --lda_syn $topics --replications $cascade\
                             -B $B --Beta_min $B_min -A $A --Alpha_min $A_min                        --iterations 10 --seed $seed_index --seed_2 $seed_2 --seed_3 $seed_3 --fix_phi & 
    sleep 10
    #python -m experiment kernel -n 'test' -s $s --verbose --logging I -t $t --seed $seed_index\
    #                         -K $K -L $L --Length_min $L_min -d $doc                                   -o ./outputs_bernoulli_param_rel_small/s$s/ \
    #                         -P $P --Phi_min $P_min -G $G --Gamma_min $G_min --Arrival_bandwidth $Arrival_bandwidth --lda_syn $topics --replications $cascade \
    #                         -B $B --Beta_min $B_min -A $A --Alpha_min $A_min                        --iterations 350 --seed $seed_index --seed_2 $seed_2 --seed_3 $seed_3 

#  done
#done
deactivate
