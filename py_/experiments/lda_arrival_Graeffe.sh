source venv_par/bin/activate
export OMP_NUM_THREAD=128
python -m experiment arrival -c ./configs/config_real.cfg -r "./full_set_merged-d-118399-s-55732" -o './outputs_graeffe/' --verbose --logging I --store_processed --print \
 --processed_path "./outputs_graeffe/Graeffe-lda_arrival_final-s-55732-d-116020-T-4869-2016-01-31 23:36:59.886991.post.pickle" \
 --restore "./outputs_graeffe/Graeffe-lda_arrival_final-s-55732-d-116020-T-4869-2016-02-02 05:47:07.874966.items.pickle"
 #--restore "./outputs_graeffe/Graeffe-lda_arrival_limit_inf-s-55732-d-116021-T-4838-2016-01-21 02:53:00.974479.items.pickle"
deactivate
