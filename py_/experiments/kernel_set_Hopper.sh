source venv_new/bin/activate
#export LD_LIBRARY_PATH=/lustre/home/btabibian/socialnet/scs/out:$LD_LIBRARY_PATH
export OMP_NUM_THREAD=128
counting=1000
index=$(($counting * $1))
python -m experiment kernel -r "./full_set_merged" -o './outputs_hopper' -n 'Hopper-kernel'.$index.$counting -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb 1.0 --min_count $index --limit_document $counting --min_sites 10
deactivate
