doc_size=(50 75) 
source venv/bin/activate
index=$1
python -m experiment frailty -n 'popper' -s 500 -d 500 -i 10 -t ${doc_size[$(($index % ${#doc_size[@]}))]}  -B 5.2 -G 3.0 -K 8 -o ./outputs_popper_frailty/
deactivate
