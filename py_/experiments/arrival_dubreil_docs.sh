doc_size=(200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000 3200 3400 3600 3800)
source venv_par/bin/activate
echo $seed_index
for doc_index in ${doc_size[@]}; do
  python -m experiment arrival --config ./configs/config_synthetic_cnst.cfg --simulation "./merged_samples_cnst/syn_merged_all-d-"$doc_index -o ./outputs_bernoulli_doc_cnst/ --iterations 80 --print
done
deactivate
