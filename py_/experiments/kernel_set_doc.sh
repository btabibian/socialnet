doc_size=(200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000 3200 3400 3600 3800 4000 4200 4400 4600 4800 5200 5600 6000)
source venv_par/bin/activate
input_path=./merged_samples/
for doc_index in ${doc_size[@]} ; do
python -m experiment kernel -c configs/config_synthetic.cfg --simulation "$input_path/syn_merged_all-d-"$doc_index  -o ./outputs_kernel_doc_em/ --verbose --iterations 1000 --print --lamb 0.1
done
deactivate
rm ./outputs_kernel_doc_2/*.model.pickle
rm ./outputs_kernel_doc_2/*.simul.pickle
