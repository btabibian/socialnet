source venv_new/bin/activate
export LD_LIBRARY_PATH=/lustre/home/btabibian/socialnet/scs/out:$LD_LIBRARY_PATH
export OMP_NUM_THREAD=128
python -m experiment kernel -r "./full_set_merged" -o './outputs_frailty' -n 'Euler-kernel' -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb 1.0 --min_sites 10
deactivate
