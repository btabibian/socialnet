doc_size=(200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000 3200 3400 3600 3800)
source venv_par/bin/activate
input_path=./merged_samples_cnst/
for doc_index in ${doc_size[@]} ; do
  python -m experiment kernel -c configs/config_synthetic_cnst.cfg --simulation "$input_path/syn_merged_all-d-"$doc_index  -o ./outputs_kernel_doc_cnst/ --verbose --iterations 2800 --print --lamb 0&
done
deactivate
rm ./outputs_kernel_doc_cnst/*.model.pickle
rm ./outputs_kernel_doc_cnst/*.simul.pickle
