source venv_par/bin/activate
export OMP_NUM_THREAD=128
files=(so_d-428-s-26 so_d-61917-s-2430 so_d-5265-s-287 so_d-692455-s-16732 so_d-3644487-s-71263)
for doc in ${files[@]}; do
  python -m experiment arrival -c ./configs/config_synthetic_so.cfg -r "./stackoverflow/"$doc -o './outputs_noether_arrival/' --verbose --logging I --store_processed --print 
done
deactivate
