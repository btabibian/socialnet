source venv_new/bin/activate
days_predict=(10 40 60 70 80 90)
index=$1
ind=${days_predict[$(($index % ${#days_predict[@]}))]}
python -m experiment frailty -r "./news_article_incomplete" -o './outputs_rayleigh' -n 'Rayleigh_predictionsi_news_lda_lamb_'$ind -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb 10000.0 --predict $ind --min_items 10  --lda ./lda_wiki/sparse_news.pickle
deactivate
