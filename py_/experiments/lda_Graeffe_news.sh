source venv_par/bin/activate
lamb_size=(0.1 1.0 10 100 1000)
index=$1
#ind=${doc_size[$(($index % ${#doc_size[@]}))]}
OMP_NUM_THREADS=10
python -m experiment kernel --config ./configs/config_real.cfg -r "./full_set_merged-d-118399-s-55732" -o './outputs_graeffe_val' --verbose --logging I --store_processed --processed_path "./outputs_graeffe/Graeffe-final_0.1-s-55732-d-116020-T-4869-2016-02-01 00:05:07.970797.post.pickle" --print
deactivate
