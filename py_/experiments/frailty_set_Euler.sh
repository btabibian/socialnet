source venv_new/bin/activate
python -m experiment frailty -r "./news_article_incomplete" -o './outputs_frailty' -n 'Euler-frailty_prior_news' -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb 100.0 --min_items 10
deactivate
