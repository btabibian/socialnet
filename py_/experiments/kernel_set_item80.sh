item_size=(30 50 90 100 150 200 250 400 700)
source venv/bin/activate
index=$1
python -m experiment kernel -n 'kernel_set' -s 80 -d 10 -i ${item_size[$(($index % ${#item_size[@]}))]}  -t 8  -B 1.2 -A 3.0 -K 4 -L 1 -o ./outputs_kernel_item/
deactivate
