source venv_new/bin/activate
export LD_LIBRARY_PATH=/lustre/home/btabibian/socialnet/scs/out:$LD_LIBRARY_PATH
export OMP_NUM_THREAD=128
python -m experiment kernel -r "./wikidata_parsed_fixed_survived" -o './outputs_rayleigh' -n 'Rayleigh-kernel' -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb 1.0 --predict 0.1
deactivate
