item_size=(2000 4000 6000 8000 10000)
source venv_new/bin/activate
index=$1
seed_index=$(($index / ${#item_size[@]} + 1203))
echo $seed_index
python -m experiment kernel -n 'kernel_set' -s 15 -d 10 -i ${item_size[$(($index % ${#item_size[@]}))]}  -t 10  -B 3.2 -A 3.0 -K 4 -L 1 -o ./outputs_kernel_item/ --seed $seed_index --lda_syn 4
deactivate
rm ./outputs_kernel_item/*.model.pickle
rm ./outputs_kernel_item/*.simul.pickle
