source venv_new/bin/activate
export OMP_NUM_THREAD=128
python -m experiment arrival -r "./full_set_merged-d-118399-s-55732" -o './outputs_graeffe/' -n 'Graeffe-lda_arrival_final_short' -K 19 --Length_min 100.0 --Length 100.1 --uniform --verbose --logging I --discard 0.125 --Arrival_bandwidth 0.085 --lambd 0 --lda ./lda_wiki/116k.pickle --store_processed --iterations 6 --print\
 --processed_path "./outputs_graeffe/Graeffe-lda_arrival_final-s-55732-d-116020-T-4869-2016-01-31 23:36:59.886991.post.pickle" \
 --restore "./outputs_graeffe/Graeffe-lda_arrival_final-s-55732-d-116020-T-4869-2016-02-02 15:02:43.144588.items.pickle"
 #--restore "./outputs_graeffe/Graeffe-lda_arrival_limit_inf-s-55732-d-116021-T-4838-2016-01-21 02:53:00.974479.items.pickle"
deactivate
