source venv_par/bin/activate
count_size=(10 20 30 40 50 60 70 80 90 110 120 130 140 150 160 170 180 190 200 210 220 230 240 260 280 300 320 340 360 380 400 420 440)
step=20
output_path=./merged_samples_cnst
input_path=./samples_cnst
for count in ${count_size[@]} ; do
  total=$(($count*step))
  python -m model.merge_simul -i $input_path/ -o $output_path/syn_merged_all-d-$total --step $step --count $count
done
deactivate
