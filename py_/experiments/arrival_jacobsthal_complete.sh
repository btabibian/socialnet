source venv_new/bin/activate
export OMP_NUM_THREAD=128
python -m experiment arrival -r "./full_set_merged-d-118399-s-55732" -o './outputs_jacobsthal/' -n 'Jacobsthal_complete_prior' -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --discard 0.01 --Arrival_bandwidth 10.0 --lambd 0.1
deactivate
