doc_size=(10 15 20 25 30 35 40 45 100)
source venv_new/bin/activate
index=$1
seed_index=$(($index / ${#doc_size[@]} + 1201))
echo $seed_index
python -m experiment arrival -n 'test' -B 2.6 -s 14 --verbose --logging I -t 10 --seed $seed_index\
                             -K 3 -L 4.1 --Length_min 4.0 -d 10 --replications ${doc_size[$(($index % ${#doc_size[@]}))]} -o ./outputs_bernoulli/ -P 4.2 --Phi_min 3.0 --Arrival_bandwidth 1 --lda_syn 4
deactivate
