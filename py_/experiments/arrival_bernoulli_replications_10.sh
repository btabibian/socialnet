doc_size=(10 100 200 300 400 500 600 700 1000 2000 4000)
source venv_new/bin/activate
index=$1
python -m experiment arrival -n 'test' -B 1.9 -s 10 --verbose --logging I -t 100 \
                             -K 2 -L 900.1 --Length_min 900.0 -d 10 --replications ${doc_size[$(($index % ${#doc_size[@]}))]} -o ./outputs_bernoulli/ -P 10 --Phi_min 9 --Arrival_bandwidth 100
deactivate
