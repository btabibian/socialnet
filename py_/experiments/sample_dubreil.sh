source venv_par/bin/activate
seed_1=20937 
seed_2=61204
seed_3=12292
export OMP_NUM_THREAD=3

out_path=./samples_cnst/
mkdir $out_path/
mkdir $out_path/


index=$doc
#for every  size fix site parameters
seed_index=$(($seed_1+$1))
seed_2=$(($seed_2))
seed_3=$(($seed_1+$1))
python -m experiment arrival --config ./configs/config_synthetic_cnst.cfg --verbose --logging I --seed $seed_index --print\
                             -o ./$out_path/  --iterations 3 --seed $seed_index --seed_2 $seed_2 --seed_3 $seed_3 

#  done
#done
deactivate
