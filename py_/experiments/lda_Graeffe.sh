source venv_new/bin/activate
doc_size=(10 40 60 70 80 90)
lamb_size=(0.1 1.0 10 100 1000)
index=$1
ind=${doc_size[$(($index % ${#doc_size[@]}))]}
lamb_ind=${lamb_size[$(($index % ${#lamb_size[@]}))]}
python -m experiment frailty -r "./wikidata_parsed_fixed_survived" -o './outputs_graeffe' -n Graeffe-lda_lamb_$lamb_ind -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb $lamb_ind --lda ./lda_wiki/sparse.pickle
deactivate
