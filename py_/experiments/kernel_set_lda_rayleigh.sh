source venv_new/bin/activate
lamb_size=(0.1 1.0 10 100 1000)
index=$1
lamb_ind=${lamb_size[$(($index % ${#lamb_size[@]}))]}
export LD_LIBRARY_PATH=/lustre/home/btabibian/socialnet/scs/out:$LD_LIBRARY_PATH
export OMP_NUM_THREAD=128
python -m experiment kernel -r "./wikidata_parsed_fixed_survived" -o './outputs_rayleigh/' -n Rayleigh-lda_kernel -K 19 --Length_min 10000.0 --Length 10000.1 --uniform --verbose --logging I --lamb $lamb_ind  --lda ./lda_wiki/sparse.pickle --predict 0.1
deactivate
