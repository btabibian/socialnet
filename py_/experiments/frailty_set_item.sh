item_size=(30 50 90 100 150 200 250 400 700)
source venv/bin/activate
index=$1
python -m experiment frailty -n 'frailty_set' -s 50 -d 10 -i ${item_size[$(($index % ${#item_size[@]}))]}  -t 8  -B 1.2 -G 3.0 -K 4 -L 1 -o  ./outputs_frailty_item
deactivate
