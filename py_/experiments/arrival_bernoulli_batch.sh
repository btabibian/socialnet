source venv_new/bin/activate
index=$1
replications=10
for replication in $(seq 0 $replications); do
  seed_index=$(($index +  6129812 + $replication))
  echo $seed_index
  mkdir ./outputs_bernoulli_batch/s$replication
  seed_2=$(($replication+9812))
  echo $seed_2
  python -m experiment arrival -n 'test' -s 10 --verbose --logging I -t 5 --seed $seed_index \
                             -K 3 -L 0.6 --Length_min 0.5 -d 10                                   -o ./outputs_bernoulli_batch/s$replication \
                             -P 6.0 --Phi_min 0.5 -G 0.6 --Gamma_min 0.4 --Arrival_bandwidth 3.0 --lda_syn 4 \
                             -B 0.6 --Beta_min 0.4 -A 0.6 --Alpha_min 0.4                         --iterations 1 --seed $seed_index --seed_2 $seed_2

 #python -m experiment arrival -n 'test' -s 10 --verbose --logging I -t 15 --seed $seed_index \
 #                            -K 2 -L 2.1 --Length_min 3.0 -d 10                                   -o ./outputs_bernoulli_batch/s$replication \
 #                            -P 10.0 --Phi_min 1.0 -G 0.4 --Gamma_min 0.3 --Arrival_bandwidth 3.0 --lda_syn 4 \
 #                            -B 2.0 --Beta_min 1.0 -A 0.4 --Alpha_min 0.3                         --iterations 1 --seed $seed_index --seed_2 $seed_2

done
deactivate
