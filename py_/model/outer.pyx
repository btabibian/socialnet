import cython
import numpy as np
cimport numpy as np

from libc.math cimport exp
from libc.string cimport memset

from cpython cimport PyCapsule_GetPointer # PyCObject_AsVoidPtr

REAL = np.float64
ctypedef np.float32_t REAL_t
ctypedef np.uint32_t  INT_t

cdef int ONE = 1
cdef REAL_t ONEF = <REAL_t>1.0

ctypedef void (*sger_ptr) (const int *M, const int *N, const float *alpha, const float *X, const int *incX, float *Y, const int *incY, float *A, const int * LDA) nogil


def outer_prod(_x, _y, _output, _i ,_j, s, normaliz):
    cdef INT_t *i = <INT_t *>(np.PyArray_DATA(_i))
    cdef INT_t *j = <INT_t *>(np.PyArray_DATA(_j))
    
    cdef REAL_t *x = <REAL_t *>(np.PyArray_DATA(_x))
    cdef int M = _y.shape[0]
    cdef int N = _x.shape[0]
    cdef REAL_t *y = <REAL_t *>(np.PyArray_DATA(_y))
    
    cdef REAL_t *output = <REAL_t *>(np.PyArray_DATA(_output))
    for ind in range(s):
      if ind % 100000==0:
        print('%d of %d finished.' % (ind,s))
      output[ind] = _x[i[ind],0]*_y[0,j[ind]]/normaliz[j[ind]]
