import numpy as np
from scipy.stats import norm
import scipy as sp
from scipy import optimize

def intensity(model,d,t,events,temporal_only=False,isMax=False, history=False,replications=1,only_hawkes=False):
  temporal = 0
  # Needed for close to zero evaluation of maximum finder
  if t<0:
    return 0

  if model['kernel_type_arrival'] == 'RBF' :
    temporal = (norm.pdf(t,loc = model['kernels_arrival'],scale = model['length_arrival'])*model['phi'][d//replications,:]).sum()
  elif model['kernel_type_arrival'] == 'CONST' :
    kern_ = np.sum(model['kernels_arrival']<=t)-1
    if kern_ == model['kernels_arrival'].shape[0]-1:
      space = model['T'] - model['kernels_arrival'][-1]
    else:
      space = model['kernels_arrival'][1:] - model['kernels_arrival'][:-1]
      space = space[kern_]
    temporal = model['phi'][d//replications,kern_]
  else:
    raise Exception("unsupported kernel %s" %  model['arrival_kernel_type'])
  if temporal_only:
    return temporal
  else:
    nodes = events['nodes']
    times = events['times']
    sites = events['sites']
    if history is False:
      history = np.logical_and(nodes[:,0] == d ,times[:,1] < t)
    else:
      history = times[:,1] < t
    if 'membership' in model:
      members = model['gamma'][sites[history]].dot(model['membership'][d//replications,:].T)
    else:
      members = model['gamma'][sites[history]]
    if model['hawkes_kernel_type'] == 'RBF':
      joint = norm.pdf(t,loc = times[history,1],
               scale = model['arrival_bandwidth'])*members
    else:
      joint = np.ones(times[history,1].shape[0])*members
    if only_hawkes:
      return joint.sum()
    if model['arrival_mode'] == "INH":
      res = temporal-joint.sum()
      #if isMax==False:
      #  print(t,temporal,joint.sum(),res)
      if res<0:
        res = 1e-10
        #print(joint)
        #print(model['phi'][d//replications,kern_])
        #raise Exception("intensity negative, temporal %f, site comp: %f" %(temporal,joint.sum()))
      return res
    elif model['arrival_mode'] == "EXC":
      return joint.sum()+temporal
    else:
      raise Exception("unsupported excitation model %s" %  model['arrival_mode'])

def intensityMax(model,d,t,events,T,temporal_only=False,replications=1):
  history = events['nodes'][:,0] == d
  events_opt = {'nodes':events['nodes'][history,:],
                'times':events['times'][history,:],
                'sites':events['sites'][history,:]}
  objective = lambda x: -intensity(model,d,x,events_opt,temporal_only,True,True,replications)
  op_result = optimize.brute(objective,ranges=[(t,T)],Ns=1000)
  #op_result = optimize.basinhopping(objective,x0=t,minimizer_kwargs={"bounds":[(t,T)]})
  return -1*objective(op_result[0])

def intensityTemporal(model,events,d,step=0.1,mean=False,onlyBase=False,discard=0.25,only_hawkes=False,T=None):
  I = np.zeros(int(np.floor(np.max(events['times'])/step)))
  j = events['nodes'][:,0] ==d
  times = events['times'][j,:]
  sites = events['sites'][j,:]
  max_T = np.max(events['times'][:,1])+1.0 if T is None else T
  steps = np.arange(0,max_T,step)
  I = np.zeros(steps.shape[0])
  for ind,t in enumerate(steps):
    I[ind] = intensity(model,d,t,events,temporal_only=onlyBase,only_hawkes=only_hawkes)
  return I,steps,times
def intensityTemporalWikiBase(model,events,d,i,step=0.1):
  times = events['times'][events['nodes'][:,0]==d,:]
  if 'times_survived' in events:
    times_survived = events['times_survived'][events['nodes_survived'][:,0]==d,:]
    times_merged = np.concatenate([times,times_survived])
  else:
    times_merged = times
  max_T = np.max(times_merged)+1
  min_T = np.min(times_merged)
  steps = np.arange(min_T,max_T,step)
  I = np.zeros(steps.shape[0])
  for ind,t in enumerate(steps):
    #history = np.logical_and(events['nodes'][:,0] == d ,events['times'][:,1] > t-0.25)
    I[ind] = intensity(model,d,t,None,temporal_only=True,history=True)
  return I,steps
