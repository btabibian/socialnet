from __future__ import division
import cvxpy as cvx
import numpy as np
from scipy.stats import norm
from scipy import sparse
import math
import logging
import time
import pyximport; pyximport.install()
from model import  inference_utils
from model import outer

def inference(events,documents,items,sites, T, init_sites,fix_pi=None,fix_alpha=None,fix_beta=None):
  docs_count = documents
  items_count = items#model['items']
  sites_count = sites#smodel['sites']

  # Construct the problem.
  pi = cvx.Variable(sites_count) if fix_pi is None else fix_pi
  beta = cvx.Variable(docs_count) if fix_beta is None else fix_beta
  alpha = cvx.Variable(sites_count) if fix_alpha is None else fix_alpha

  items = []
  for d in range(docs_count):
    for i in range(items_count):
      j = np.logical_and(events['nodes'][:,0] ==d ,events['nodes'][:,1] == i)
      t = events['times'][j,:]
      if t.shape[0]>0:
        deltaT = t[1:] - t[:-1,:]
        tDiff = np.zeros((t.shape[0]+1,1))
        tDiff[0,:] = t[0,:]
        tDiff[1:-1,:] = deltaT
        tDiff[-1,:] = T - t[-1,:]
        siteChange = np.zeros((t.shape[0]+1,1))
        siteChange[1:] = events['sites'][j,:]
        siteChange[0] = init_sites[d,i]
        items.append((siteChange,tDiff,d))
      if t.shape[0]==0:
        siteChange = np.zeros((1,1))
        tDiff = np.zeros((0,1))
        items.append((siteChange,tDiff,d))


  loglikelihood = 0
  for item in items:
    sites = item[0].astype(int)
    tDiff = item[1]
    d = item[2]
    if tDiff.shape[0]>0:
      for j in range(tDiff.shape[0]-1):
        loglikelihood += cvx.log(pi[sites[j]])+cvx.log((alpha[sites[j]]+beta[d])*tDiff[j])-(alpha[sites[j]]+beta[d])*tDiff[j]
      loglikelihood += -(alpha[sites[j]]+beta[d])*tDiff[-1]
  objective = cvx.Minimize(-1*loglikelihood)

  constraints = []
  if fix_alpha is None:
    constraints.append(alpha>=0)
  if fix_beta is None:
    constraints.append(beta>=0)
  if fix_pi is None:
    constraints.append(cvx.sum_entries(pi)== 1)
    constraints.append(pi>=0)
  prob = cvx.Problem(objective, constraints)
  results = prob.solve()
  if fix_alpha is None:
    alpha = np.asarray(alpha.value).flatten()
  if fix_beta is None:
    beta = np.asarray(beta.value).flatten()
  if fix_pi is None:
    pi = np.asarray(pi.value).flatten()
  return results,pi,beta,alpha

def createDataSet(events,docs_count,items_count,T,kernels_mean,kernels_length,init_sites):
  computeGrid = lambda t: norm.pdf(t,loc = kernels_mean,scale = kernels_length)
  computeGrid_cdf = lambda t: norm.cdf(t,loc = kernels_mean,scale = kernels_length)
  items = []
  for d in range(docs_count):
    for i in range(items_count):
      j = np.logical_and(events['nodes'][:,0] ==d ,events['nodes'][:,1] == i)
      t = events['times'][j,:]
      if t.shape[0]>0:
        deltaT = t[1:] - t[:-1,:]
        tDiff = np.zeros((t.shape[0]+1,1))
        tDiff[0,:] = t[0,:]
        tDiff[1:-1,:] = deltaT
        tDiff[-1,:] = T - t[-1,:]
        tDiff_cdf = np.zeros((t.shape[0]+1,kernels_mean.shape[0]))
        tDiff_cdf[0,:] = computeGrid_cdf(t[0,:])-computeGrid_cdf(np.zeros(1))
        tDiff_cdf[1:-1,:] = computeGrid_cdf(t[1:,:])-computeGrid_cdf(t[:-1,:])
        tDiff_cdf[-1,:] = computeGrid_cdf(T) - computeGrid_cdf(t[-1,:])
        siteChange = np.zeros((t.shape[0]+1,1),dtype='int')
        siteChange[1:] = events['sites'][j,:]
        siteChange[0] = init_sites[d,i]
        t_ = np.append(t,np.array([[T]]),axis=0)
        survived = t.shape[0]
        items.append((siteChange.ravel(),tDiff,d,computeGrid(t_),tDiff_cdf,int(survived))) #making compatible with 0.3.0
  return items
def createDataSet_rawInh(events,docs_count,kernels_mean,kernels_length,one_removal,T):
  if one_removal:
    end_ = np.zeros(docs_count)+T
    ended_dox = events['nodes'][:,0].flatten()
    end_[ended_dox] = events['times'][:,1]
    events['times_survived'][:,1] = end_[events['nodes_survived'][:,0]]
  selected = events['times_survived'][:,0] < events['times_survived'][:,1]
  res = ((events['sites'],
        events['times'][:,1]-events['times'][:,0],events['nodes'][:,0],
        np.ones((events['times'].shape[0],1)),
        events['times'][:,1]-events['times'][:,0]),
       (events['sites_survived'][selected],
       events['times_survived'][selected,1]-events['times_survived'][selected,0],events['nodes_survived'][selected,0],
       np.ones((selected.sum(),1)),
       events['times_survived'][selected,1]-events['times_survived'][selected,0]))
  return res

def createDataSet_raw(events,docs_count,kernels_mean,kernels_length):
  computeGrid = lambda t: norm.pdf(np.reshape(t,(t.shape[0],1)),loc = kernels_mean,scale = kernels_length)
  computeGrid_cdf = lambda t: norm.cdf(np.reshape(t,(t.shape[0],1)),loc = kernels_mean,scale = kernels_length)

  res = ((events['sites'],
        events['times'][:,1]-events['times'][:,0],events['nodes'][:,0],
        computeGrid(events['times'][:,1]),
        computeGrid_cdf(events['times'][:,1])-computeGrid_cdf(events['times'][:,0])),
       (events['sites_survived'],
       events['times_survived'][:,1]-events['times_survived'][:,0],events['nodes_survived'][:,0],
       computeGrid(events['times_survived'][:,1]),
       computeGrid_cdf(events['times_survived'][:,1])-computeGrid_cdf(events['times_survived'][:,0])))
  return res

def inferenceKernelRaw(items,sites_count,documents_count,pi,beta_temp,alpha,lda=None,items_count=1,replications=1):
  none_survived, survived = items
  sites_none_survived,tDiff_none_srvived,d_none_survived,t_none_survived,t_diff_cdf_none_survived = none_survived

  sites_none_survived = sparse.csr_matrix((np.ones(sites_none_survived.shape[0]),(np.arange(sites_none_survived.shape[0]),sites_none_survived.flatten())),(sites_none_survived.shape[0],sites_count))
  rows =[]
  cols =[]
  for i in range(documents_count):
    rows.extend([i*replications+l for l in range(replications)])
    cols.extend([i for l in range(replications)])
  rep_matrix = sparse.csr_matrix((np.ones(len(rows)),(rows,cols)),(documents_count*replications,documents_count))

  documents_none_survived_ = sparse.csr_matrix((np.ones(d_none_survived.shape[0]),(np.arange(d_none_survived.shape[0]),d_none_survived)),(d_none_survived.shape[0],documents_count*replications))
  documents_none_survived = documents_none_survived_*rep_matrix
  sites_pi_none_survived = sites_none_survived*pi
  if lda is None:
    sites_alpha_none_survived = sites_none_survived*alpha
  else:
    sites_alpha_none_survived = cvx.mul_elemwise(documents_none_survived*lda,sites_none_survived*alpha)*np.ones(lda.shape[1])
  beta_d_none_survived = cvx.mul_elemwise(t_diff_cdf_none_survived,(documents_none_survived*beta_temp))*np.ones(t_none_survived.shape[1])
  loglikelihood=0
  loglikelihood = cvx.sum_entries(cvx.log(sites_pi_none_survived)+ \
                  cvx.log(cvx.mul_elemwise(t_none_survived,(documents_none_survived*beta_temp))*np.ones(t_none_survived.shape[1])+sites_alpha_none_survived) -\
                  cvx.mul_elemwise(tDiff_none_srvived,sites_alpha_none_survived) - beta_d_none_survived)

  sites_survived,tDiff_srvived,d_survived,t_survived,t_diff_cdf_survived = survived
  if sites_survived.shape[0]==0:
    return loglikelihood
  sites_survived = sparse.csr_matrix((np.ones(sites_survived.shape[0]),(np.arange(sites_survived.shape[0]),sites_survived.flatten())),(sites_survived.shape[0],sites_count))
  documents_survived_ = sparse.csr_matrix((np.ones(d_survived.shape[0]),(np.arange(d_survived.shape[0]),d_survived)),(d_survived.shape[0],documents_count*replications))
  documents_survived = documents_survived_*rep_matrix
  sites_pi_survived = sites_survived*pi
  if lda is None:
    sites_alpha_survived = sites_survived*alpha
  else:
    sites_alpha_survived = cvx.mul_elemwise(documents_survived*lda,sites_survived*alpha)*np.ones(lda.shape[1])
  beta_d_survived = cvx.mul_elemwise(t_diff_cdf_survived,(documents_survived*beta_temp))*np.ones(t_survived.shape[1])
  loglikelihood += cvx.sum_entries(cvx.log(sites_pi_survived)-beta_d_survived-cvx.mul_elemwise(tDiff_srvived,sites_alpha_survived))
  return loglikelihood


def inferenceKernelNew(items,sites_count,documents_count,pi,beta_temp,alpha,lda=None,items_count=1,replications=1,iterations=10,lamb=1.0,fix_alpha=None,fix_beta=None):
  none_survived, survived = items
  sites_none_survived,tDiff_none_srvived,d_none_survived,t_none_survived,t_diff_cdf_none_survived = none_survived
  if len(t_diff_cdf_none_survived.shape)==1:
    t_diff_cdf_none_survived = t_diff_cdf_none_survived[:,np.newaxis]
  t_diff_cdf_none_survived = t_diff_cdf_none_survived+1e-7

  sites_none_survived = sites_none_survived.astype('int')
  sites_survived,tDiff_srvived,d_survived,t_survived,t_diff_cdf_survived = survived
  if len(t_diff_cdf_survived.shape)==1:
    t_diff_cdf_survived = t_diff_cdf_survived[:,np.newaxis]
  t_diff_cdf_survived = t_diff_cdf_survived+1e-7

  sites_survived = sites_survived.astype('int')

  d_non_surv = sparse.csr_matrix((np.ones(d_none_survived.shape[0]),(np.arange(d_none_survived.shape[0]),d_none_survived)),(d_none_survived.shape[0],documents_count))
  print(sites_count)
  s_non_surv = sparse.csr_matrix((np.ones(sites_none_survived.shape[0]),(np.arange(sites_none_survived.shape[0]),sites_none_survived.flatten())),(sites_none_survived.shape[0],sites_count))
  d_surv = sparse.csr_matrix((np.ones(d_survived.shape[0]),(np.arange(d_survived.shape[0]),d_survived)),(d_survived.shape[0],documents_count))

  s_surv = sparse.csr_matrix((np.ones(sites_survived.shape[0]),(np.arange(sites_survived.shape[0]),sites_survived.flatten())),(sites_survived.shape[0],sites_count))
  if lda is None:
    lda = np.ones((documents_count,1))
  beta = np.random.uniform(0,1,(documents_count,t_diff_cdf_none_survived.shape[1]))
  alpha = np.random.uniform(0,1,(sites_count,lda.shape[1]))
  alpha[:] = 1e-6
  if fix_alpha is not None:
    print('alpha set')
    alpha=fix_alpha
  if fix_beta is not None:
    beta=fix_beta

  print(beta.dtype,alpha.dtype)
  etta_iij = np.zeros((sites_none_survived.shape[0],t_diff_cdf_none_survived.shape[1]))
  etta_iik = np.zeros((sites_none_survived.shape[0],lda.shape[1]))
  print(etta_iij.dtype,etta_iik.dtype)
  if type(lda) is not 'csr_matrix':
    lda = sparse.csr_matrix(lda) 
  lda_surv_site = np.asarray(lda[d_survived,:].todense())
  lda_none_surv_site = np.asarray(lda[d_none_survived,:].todense())
  t_none_survived = t_none_survived
  print('setting EM algorithm')
  print(d_none_survived.shape)
  for iteration in range(iterations):
    #expectation
    print('iteration %d of %d' % (iteration,iterations))
    beta_prod = np.multiply(beta[d_none_survived.flatten(),:],(t_none_survived))
    alpha_prod = np.multiply(alpha[sites_none_survived.flatten(),:],np.asarray(d_non_surv.dot(lda).todense()))
    norm_source = beta_prod.dot(np.ones(beta_prod.shape[1]))+alpha_prod.dot(np.ones(alpha_prod.shape[1]))
    #norm_source = norm_source.flatten()
    print('E step')
    for k in range(lda.shape[1]):
      etta_iik[:,k] = alpha_prod[:,k]/norm_source
    for k in range(t_diff_cdf_none_survived.shape[1]):
      etta_iij[:,k] = beta_prod[:,k]/norm_source
    print((etta_iik.sum(axis=1)+etta_iij.sum(axis=1)).min(),(etta_iik.sum(axis=1)+etta_iij.sum(axis=1)).max())
    etta_iij[etta_iij<1e-14] = 0

    print('M step')
    #MStep
    for k in range(beta.shape[1]):
      normalizer = (d_surv.T.dot(t_diff_cdf_survived[:,k])+d_non_surv.T.dot(t_diff_cdf_none_survived[:,k])+lamb)
      beta[:,k] = d_non_surv.T.dot(etta_iij[:,k])/(normalizer)
    if fix_alpha is not None:
      print('alpha set')
    else:
      for k in range(alpha.shape[1]):
        normalizer = s_surv.T.dot(tDiff_srvived*lda_surv_site[:,k])+s_non_surv.T.dot(tDiff_none_srvived*lda_none_surv_site[:,k])+1e-100
        alpha[:,k] = s_non_surv.T.dot(etta_iik[:,k])/\
          (normalizer)
    print('average beta %f, alpha %f' % (np.median(beta),np.median(alpha)))


  return beta,alpha
def inferenceKernel(events,documents,sites, T, kernels_mean = None, kernels_length = None,
                    fix_pi=None,fix_alpha=None,fix_beta=None,fix_beta_temp=None,verbose=False,lamb=0.0,lda=None,iterations=300,replications=1,kernel_type='RBF', one_removal=False,approximation=True):
  docs_count = documents
  sites_count = sites#smodel['sites']
  # Construct the problem.
  pi = cvx.Variable(sites_count) if fix_pi is None else fix_pi
  if lda is None:
    alpha = cvx.Variable(sites_count) if fix_alpha is None else fix_alpha
  else:
    alpha = cvx.Variable(sites_count,lda.shape[1]) if fix_alpha is None else fix_alpha
  beta_temp = cvx.Variable(docs_count,kernels_mean.shape[0]) if fix_beta_temp is None else fix_beta_temp
  loglikelihood = 0
  print(kernel_type,type(one_removal))
  if kernel_type=='RBF' and one_removal==False:
    items = createDataSet_raw(events,documents,kernels_mean,kernels_length)
  elif kernel_type=='CONST':
    items = createDataSet_rawInh(events,documents,kernels_mean,kernels_length,one_removal,T)
  if approximation:
    beta,alpha = inferenceKernelNew(items,sites_count,docs_count,pi,beta_temp,alpha,lda, replications=replications,iterations=iterations,lamb=lamb,fix_alpha=fix_alpha,fix_beta=fix_beta_temp)
    return 0,np.zeros(sites_count),alpha,beta
  else:
    loglikelihood = inferenceKernelRaw(items,sites_count,docs_count,pi,beta_temp,alpha,lda, replications=replications)
  objective = cvx.Minimize(-1*loglikelihood+(lamb*cvx.norm(beta_temp, 1) if lamb > 0 else 0))
  constraints = []
  if fix_alpha is None:
    constraints.append(alpha>=0)
  if fix_pi is None:
    constraints.append(cvx.sum_entries(pi)== 1)
    constraints.append(pi>=0)
  if kernels_mean is not None:
    if fix_beta_temp is None:
      constraints.append(beta_temp>=0)
  prob = cvx.Problem(objective, constraints)
  results = prob.solve(verbose=verbose,max_iters = iterations, solver = cvx.ECOS)
  if fix_alpha is None:
    if lda is None:
      alpha = np.asarray(alpha.value.T).flatten()
    else:
      alpha = np.asarray(alpha.value)
  if fix_pi is None:
    pi = np.asarray(pi.value.T).flatten()
  if fix_beta_temp is None:
    beta_temp = np.asarray(beta_temp.value)
  return results,pi,alpha,beta_temp

def inferenceKernelFrailtyRaw(items,sites_count,documents_count,pi,beta_temp,kappa,lda=None):
  none_survived, survived = items
  sites_none_survived,tDiff_none_srvived,d_none_survived,t_none_survived,t_diff_cdf_none_survived = none_survived

  sites_none_survived = sparse.csr_matrix((np.ones(sites_none_survived.shape[0]),(np.arange(sites_none_survived.shape[0]),sites_none_survived.flatten())),(sites_none_survived.shape[0],sites_count))
  documents_none_survived = sparse.csr_matrix((np.ones(d_none_survived.shape[0]),(np.arange(d_none_survived.shape[0]),d_none_survived)),(d_none_survived.shape[0],documents_count))
  sites_pi_none_survived = sites_none_survived*pi
  if lda is None:
    sites_kappa_none_survived = sites_none_survived*kappa
  else:
    print(lda.shape)
    sites_kappa_none_survived = cvx.mul_elemwise(documents_none_survived*lda[:documents_count],sites_none_survived*kappa)*np.ones(lda.shape[1])
    print(sites_kappa_none_survived.size)
  beta_d_none_survived = cvx.mul_elemwise(t_diff_cdf_none_survived,(documents_none_survived*beta_temp))*np.ones(t_none_survived.shape[1])
  loglikelihood = cvx.sum_entries(cvx.log(sites_pi_none_survived)+ \
                  cvx.log(cvx.mul_elemwise(tDiff_none_srvived+1,
                                           cvx.mul_elemwise(t_none_survived,(documents_none_survived*beta_temp))*np.ones(t_none_survived.shape[1]))+sites_kappa_none_survived) -\
                  cvx.mul_elemwise(np.log(1+tDiff_none_srvived),sites_kappa_none_survived+1) - beta_d_none_survived)

  sites_survived,tDiff_srvived,d_survived,t_survived,t_diff_cdf_survived = survived

  sites_survived = sparse.csr_matrix((np.ones(sites_survived.shape[0]),(np.arange(sites_survived.shape[0]),sites_survived.flatten())),(sites_survived.shape[0],sites_count))
  documents_survived = sparse.csr_matrix((np.ones(d_survived.shape[0]),(np.arange(d_survived.shape[0]),d_survived)),(d_survived.shape[0],documents_count))
  sites_pi_survived = sites_survived*pi
  if lda is None:
    sites_kappa_survived = sites_survived*kappa
  else:
    sites_kappa_survived = cvx.mul_elemwise(documents_survived*lda[:documents_count],sites_survived*kappa)*np.ones(lda.shape[1])
  beta_d_survived = cvx.mul_elemwise(t_diff_cdf_survived,(documents_survived*beta_temp))*np.ones(t_survived.shape[1])
  #print(loglikelihood.size)
  loglikelihood += cvx.sum_entries(cvx.log(sites_pi_survived)-beta_d_survived-cvx.mul_elemwise(np.log(tDiff_srvived+1),sites_kappa_survived))
  return loglikelihood

def inferenceKernelFrailty(events,documents,items,sites, T, init_sites,kernels_mean = None, kernels_length = None,
                    fix_pi=None,fix_kappa=None,fix_beta=None,fix_beta_temp=None,verbose=False,lamb=0.0,lda=None):
  docs_count = documents
  items_count = items#model['items']
  sites_count = sites#smodel['sites']
  # Construct the problem.
  pi = cvx.Variable(sites_count) if fix_pi is None else fix_pi
  if lda is None:
    kappa = cvx.Variable(sites_count) if fix_kappa is None else fix_kappa
  else:
    kappa = cvx.Variable(sites_count,lda.shape[1]) if fix_kappa is None else fix_kappa
  beta_temp = cvx.Variable(docs_count,kernels_mean.shape[0]) if fix_beta_temp is None else fix_beta_temp
  loglikelihood = 0
  items = createDataSet_raw(events,documents,items,kernels_mean,kernels_length)
  loglikelihood = inferenceKernelFrailtyRaw(items,sites_count,docs_count,pi,beta_temp,kappa,lda)
  objective = cvx.Minimize(-1*loglikelihood/docs_count + (lamb*cvx.norm(beta_temp, 1)/(beta_temp.size[0]*beta_temp.size[1]) if lamb > 0 else 0))
  constraints = []
  if fix_kappa is None:
    constraints.append(kappa>=0.0)

  if fix_pi is None:
    constraints.append(cvx.sum_entries(pi)== 1)
    constraints.append(pi>=0)
  if kernels_mean is not None:
    if fix_beta_temp is None:
      constraints.append(beta_temp>=0)
  prob = cvx.Problem(objective, constraints)
  start = time.clock()
  results = prob.solve(verbose=verbose,max_iters = 50, solver = cvx.SCS)
  print("optimization time: %f" % (time.clock()-start))
  if fix_kappa is None:
    if lda is None:
      kappa = np.asarray(kappa.value.T).flatten()
    else:
      kappa = np.asarray(kappa.value.T)
  if fix_pi is None:
    pi = np.asarray(pi.value.T).flatten()
  if fix_beta_temp is None:
    beta_temp = np.asarray(beta_temp.value)
  return results,pi,kappa,beta_temp

def inferenceArrivalInh(items,sites_count,documents_count,phi_temp,gamma,lda=None,replications=1):
  tDiff,d,t,t_diff_cdf,sp_influence,sp_influence_cdf,sp_influence_sites = items
  documents = sparse.csr_matrix((np.ones(d.shape[0]),(np.arange(d.shape[0]),d/replications)),(d.shape[0],documents_count))

  if lda is None:
    sites_gamma = sp_influence_sites*gamma
  else:
    sites_gamma = cvx.mul_elemwise(documents*lda,sp_influence_sites*gamma)*np.ones(lda.shape[1])

  phi_d = cvx.mul_elemwise(t_diff_cdf,phi_temp)

  print(phi_d.size)
  mat = sparse.diags(np.asarray((sp_influence_cdf.todense())).flatten())
  loglikelihood =  cvx.sum_entries(cvx.log(-sp_influence*sites_gamma + documents*phi_temp)) \
                  +cvx.sum_entries(cvx.min_elemwise(documents.T*mat*sites_gamma-phi_d,np.zeros(documents_count)))
  return loglikelihood

def inferenceArrivalRaw(items,sites_count,documents_count,phi_temp,gamma,lda=None,replications=1):

  tDiff,d,t,t_diff_cdf,sp_influence,sp_influence_cdf,sp_influence_sites = items
  documents = sparse.csr_matrix((np.ones(d.shape[0]),(np.arange(d.shape[0]),d/replications)),(d.shape[0],documents_count))
  if lda is None:
    sites_gamma = sp_influence_sites*gamma
  else:
    sites_gamma = cvx.mul_elemwise(documents*lda,sp_influence_sites*gamma)*np.ones(lda.shape[1])
  if replications>1:
    indices = [[d_]*replications for d_ in range(documents_count)]
    indices_flat = np.array([item for sublist in indices for item in sublist])
    documents_cdf = sparse.csr_matrix((np.ones(t_diff_cdf.shape[0]),(np.arange(t_diff_cdf.shape[0]),indices_flat)),(t_diff_cdf.shape[0],documents_count))
    phi_d = cvx.mul_elemwise(documents_cdf.T*t_diff_cdf,phi_temp)*np.ones(t.shape[1])
  else:
    phi_d = cvx.mul_elemwise(t_diff_cdf,phi_temp)*np.ones(t.shape[1])

  loglikelihood =  cvx.sum_entries(cvx.log(sp_influence*sites_gamma + cvx.mul_elemwise(t,documents*phi_temp)*np.ones(t.shape[1]))) \
                  -cvx.sum_entries(sp_influence_cdf*sites_gamma)-cvx.sum_entries(phi_d)
  return loglikelihood


global normalizer
global sp_influence_sites
global gamma
global documents
global lda
global sp_column_index
global sp_row_pointers
global sp_influence
global output_data_arr
global output_data_col
global output_data_row

def computeE_init(base_data,base_ind,base_indptr,size_data,size_ind,size_indptr,topics,ret=False):
   global output_data_arr,output_data_col,output_data_row
   output_data_arr = np.frombuffer(base_data.get_obj())
   output_data_row = np.frombuffer(base_ind.get_obj())
   output_data_col = np.frombuffer(base_indptr.get_obj())
   output_data_arr = output_data_arr.reshape(size_data, topics)
   output_data_row = output_data_row.reshape(size_ind, topics)
   output_data_col = output_data_col.reshape(size_indptr, topics)
   return output_data_arr,output_data_row,output_data_col

def computeE(k):
  global sp_influence_sites,gamma,documents,lda,sp_column_index,sp_row_pointers,sp_influence,normalizer
  outer_out = np.zeros(sp_influence.nnz,dtype='float32')
  part11 = sp_influence_sites*gamma[:,k]
  part12 = (documents*lda).T[k,:]
  outer_out[:] = np.multiply(part11[sp_column_index,0].T,part12[0,sp_row_pointers])/normalizer[sp_row_pointers].T+1e-17
  res = sparse.csr_matrix((outer_out,(sp_row_pointers,sp_column_index)),sp_influence.shape)
  v1_new = sp_influence.multiply(res)
  #print('##EM E step for etha_ijk topic %d' % k)
  output_data_arr[:,k] = v1_new.data
  output_data_row[:,k] = v1_new.indices
  output_data_col[:,k] = v1_new.indptr
  return

def inferenceArrivalNew(items,sites_count,documents_count,phi_temp,gamma_,lda_=None,replications=1,iterations=20):
  global sp_influence_sites,gamma,documents,lda,sp_column_index,sp_row_pointers,sp_influence,normalizer
  lda = lda_
  from multiprocessing import Pool,Array
  import ctypes
  print('starting inference using EM')
  tDiff,d,t,t_diff_cdf,sp_influence,sp_influence_cdf,sp_influence_sites = items
  print('doc counts',documents_count,'replications',replications)
  rows =[]
  cols =[]
  for i in range(documents_count):
    rows.extend([i*replications+l for l in range(replications)])
    cols.extend([i for l in range(replications)])
  rep_matrix = sparse.csr_matrix((np.ones(len(rows)),(rows,cols)),(documents_count*replications,documents_count))
  documents_ = sparse.csr_matrix((np.ones(d.shape[0]),(np.arange(d.shape[0]),d)),(d.shape[0],documents_count*replications))
  documents = documents_*rep_matrix
  indexing  = sp_influence.copy()
  indexing[indexing>0] = 1.0

  etha_iij = np.zeros(t.shape)
  etha_ijk = [indexing.copy() for k in range(lda.shape[1])]
  phi = np.asmatrix(np.random.uniform(0,1,(documents_count,t_diff_cdf.shape[1])))
  gamma = np.asmatrix(np.random.uniform(0,1,(sp_influence_sites.shape[1],lda.shape[1]))) + 1
  sp_influence_cdf = sp_influence_cdf.todense()
  if type(lda) is not np.ndarray:
    lda = np.asmatrix(lda.todense())
  else:
    lda = np.asmatrix(lda)
  item_topics = documents*lda
  sp_value = sp_influence.data
  sp_row_pointers ,sp_column_index = sp_influence.nonzero()
  normalizer= np.asmatrix(np.zeros((documents.shape[0],1)))

  output_data_arr_ = Array(ctypes.c_double,sp_influence.nnz*lda.shape[1])
  output_data_ind_ = Array(ctypes.c_int64,sp_influence.indices.shape[0]*lda.shape[1])
  output_data_indptr_ = Array(ctypes.c_int64,sp_influence.indptr.shape[0]*lda.shape[1])
  print(sp_influence.indices.shape[0]*lda.shape[1],np.frombuffer(output_data_ind_.get_obj()).shape,'buffer to outp')
  for iter in range(iterations):
    print('iteration %d of %d in EM' % (iter,iterations))
    logging.info('iteration %d of %d in EM' % (iter,iterations))
    # E step
    normalizer[:] = np.multiply(documents*phi,t).sum(axis=1)
    print('##EM computed normaliztion step 1')
    print('##EM updated Gamma topic')
    normalizer[:] += np.einsum('ij,ji->i',sp_influence*sp_influence_sites*gamma,item_topics.T)[:,np.newaxis]
    normalizer[:] += 0.0
    print('##EM updated normalization step 2')
    etha_iij= np.multiply(documents*phi,t)/normalizer
    print('##EM E step for etha_ii')
    # item-item interaction
    pool =  Pool(initializer=computeE_init,initargs=(output_data_arr_,output_data_ind_,output_data_indptr_,sp_influence.nnz,sp_influence.indices.shape[0],
                                                    sp_influence.indptr.shape[0],lda.shape[1],False))
    results = pool.map_async(computeE, range(lda.shape[1]))
    pool.close()
    pool.join()
    dat_,ind_,indptr_=computeE_init(output_data_arr_,output_data_ind_,output_data_indptr_,sp_influence.nnz,sp_influence.indices.shape[0],
                                                    sp_influence.indptr.shape[0],lda.shape[1])
    print('collected results')
    etha_ijk = [sparse.csr_matrix((dat_[:,l],ind_[:,l],indptr_[:,l]),shape=sp_influence.shape) for l in range(lda.shape[1])]
    print(etha_iij[0,:].sum()+ sum([etha_ijk[k][0,:].sum() for k in range(lda.shape[1])]))
    print('test')
    #M step
    phi = np.divide((documents.T.dot(etha_iij)),rep_matrix.T*t_diff_cdf)
    print('##EM M step updated parameter phi')
    for k in range(lda.shape[1]):
      cdf_lil = sparse.lil_matrix((sp_influence_cdf.shape[1],sp_influence_cdf.shape[1]))
      cdf_lil.setdiag(sp_influence_cdf.T )
      norm_site = (cdf_lil*sp_influence_sites).T*(documents*lda[:,k])+1e-7
      invalid_index = np.asarray(norm_site==0)[:,0]
      print(((etha_ijk[k]).sum(axis=0).T).sum(),'sites kappa update')
      gamma[:,k] = ((etha_ijk[k]*sp_influence_sites).sum(axis=0).T)/norm_site
      print('##EM M step updated gamma for topic %d' % k)
    print('------ summary stat ------')
    print('gamma mean: %f, phi mean %f' % (gamma.mean(),phi.mean()))
  return phi,gamma
def inferenceArrival(events,documents,items,sites,kernels_mean , kernels_length , arrivals_bandwidth, term,
                    fix_gamma=None,fix_phi=None,verbose=False,discard=None,pyx=False,lambd = 0,iterations=100,
                    store_items_call_back=None,restore_items_call_back=None,lda=None,replications=1,jensen=True,mode="EXC",kernel_type='RBF',hawkes_kernel_type='RBF'):
  docs_count = documents
  items_count = items
  sites_count = sites
  if lda is None:
    gamma = cvx.Variable(sites_count) if fix_gamma is None else fix_gamma
  else:
    gamma = cvx.Variable(sites_count,lda.shape[1]) if fix_gamma is None else fix_gamma
  phi = cvx.Variable(int(math.ceil(docs_count)),kernels_mean.shape[0]) if fix_phi is None else np.matrix(fix_phi)
  etha = None
  constraints = []
  if restore_items_call_back is not None:
    items = restore_items_call_back()
  else:
    if mode=="EXC":
      items = inference_utils.createDataSetArrival(events,int(documents),int(sites),int(items),kernels_mean,kernels_length,arrivals_bandwidth,np.asarray(term),-1 if discard is None else discard,max_influence=0)
    else:
      items = inference_utils.createDataSetArrivalInh(events,int(documents),int(sites),int(items),kernels_mean,kernels_length,arrivals_bandwidth,np.asarray(term),-1 if discard is None else discard,max_influence=0)
    if store_items_call_back is not None:
        store_items_call_back(items)
  if mode=="INH":
    loglikelihood = inferenceArrivalInh(items,sites_count,docs_count,phi,gamma,lda,replications)
  else:
    if jensen:
      print('ldaa shape',lda.shape)
      if mode == "INH":
        raise Exception ("EM algorithm does not support inhibition model")
      phi,gamma = inferenceArrivalNew(items,sites_count,docs_count,phi,gamma,lda,replications,iterations)
      fix_phi = phi
      fix_gamma = gamma
      return 0,gamma,phi
      #no support of regularization
      lamb =0
    loglikelihood = inferenceArrivalRaw(items,sites_count,docs_count,phi,gamma,lda,replications,mode)
  ##Adding l1 norm as prior
  if lambd > 0:
    loglikelihood = loglikelihood/docs_count -1*lambd*cvx.norm(phi, 1)/(phi.size[0]*phi.size[1])
  objective = cvx.Minimize(-1*loglikelihood)

  if fix_gamma is None:
    constraints.append(gamma>=0.0)
  if kernels_mean is not None:
    if fix_phi is None:
      constraints.append(phi>=0)
  prob = cvx.Problem(objective, constraints)
  results = prob.solve(verbose=verbose ,max_iters = iterations, solver = cvx.SCS, use_indirect=True)
  print(results)
  if fix_gamma is None:
    if lda is None:
      gamma = np.asarray(gamma.value.T).flatten()
    else:
      gamma = np.asarray(gamma.value)
  if fix_phi is None:
    phi = np.asarray(phi.value)
  return results,gamma,phi
