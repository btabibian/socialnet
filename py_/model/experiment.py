import numpy as np
from model import inference,reliability
import os
import datetime
import pickle
import logging
import math
from model import cross_validate
from sklearn.preprocessing import normalize
import time

def generateModel(d = 10, s= 4, items = 6,a=0.1,b=0.1):
  pi_ = np.random.dirichlet(np.ones(s),1)[0,:]
  alpha_ = np.random.uniform(0,a,size=s)#np.random.gamma(0.5,1.0,s)
  beta_ = np.random.uniform(0,b,size=d) #np.random.gamma(0.5,1.0,d)
  index = np.random.multinomial(1,pi_,(d,items))>0
  return {'beta':beta_,'alpha':alpha_,'index':index,'pi':pi_,'documents':d,'items':items,'sites':s}

def generateModelKernel(config,seed=123,seed_2=345):
  np.random.seed(seed_2)
  sites_count = int(config['General']['Sites'])
  topics_count = int(config['General']['Topics'])
  documents_count = int(config['General']['Documents'])
  pi_count = int(config['General']['Pi_count'])
  pi_drich = float(config['General']['Pi_drich'])

  one_removal = config.getboolean('Arrival', 'one_removal', fallback=False)
  mode = config.get('Arrival', 'mode', fallback="EXC")
  hawkes_kernel_type = config.get('Arrival', 'hawkes_kernel_type', fallback="RBF")
  # Kernel Positions
  kernels = np.linspace(0,float(config['General']['time'])-\
                          float(config['Survival']['kernels_gap_end']),
                          num=config['Survival']['kernels_count'])
  kernels_arrival = np.linspace(0,float(config['General']['time'])-\
                          float(config['Arrival']['kernels_gap_end']),
                          num=config['Arrival']['kernels_count'])
  # Kernel Bandwidth
  length = np.linspace(float(config['Survival']['kernels_std']),
                       float(config['Survival']['kernels_std']),num=kernels.shape[0])
  length_arrival = np.linspace(float(config['Arrival']['kernels_std']),
                       float(config['Arrival']['kernels_std']),num=kernels_arrival.shape[0])
  # site distribution
  pi_ = np.random.uniform(size=sites_count)
  pi_ = pi_/pi_.sum()
  pi_[pi_<0.01] = 0.01
  pi_ = pi_/pi_.sum()

  # survival term for sites
  alpha_ = np.random.beta(float(config['Survival']['alpha_beta_a']),
                          float(config['Survival']['alpha_beta_b']),
                          (sites_count,topics_count))
  if 'alpha_max' in config['Survival']:
    alpha_ = (alpha_/(alpha_.max()))*float(config['Survival']['alpha_max'])
  # arrival excitation for sites
  if mode=="EXC":
    gamma = np.random.uniform(float(config['Arrival']['gamma_uniform_min']),
                            float(config['Arrival']['gamma_uniform_max_ratio'])*alpha_)
  else:
    gamma = np.random.uniform(float(config['Arrival']['gamma_uniform_min']),
                            float(config['Arrival']['gamma_uniform_max_ratio']),  (sites_count,topics_count))
  np.random.seed(seed)
  membership = np.random.uniform(0.1,1,size=(documents_count,topics_count))
  membership = normalize(membership, norm='l1', axis=1)

  np.random.seed(seed)
  phi = np.random.lognormal(float(config['Arrival']['phi_lognormal_mean'])
        ,sigma=float(config['Arrival']['phi_lognormal_std']),size=(documents_count,kernels_arrival.shape[0]))
  sparse_pat = np.random.multinomial(1, [1/kernels_arrival.shape[0]]*kernels_arrival.shape[0], size=documents_count).astype('bool')
  phi[np.logical_not(sparse_pat)] = 0.0
  print('sparse_pat')
  w = np.zeros((documents_count,kernels.shape[0]))
  np.random.seed(seed)
  ratio = float(config['Survival']['beta_uniform_max_ratio'])
  if kernels.shape[0] == 1:
    w[:,0] =  np.random.uniform(float(config['Survival']['beta_uniform_min']),
                               float(config['Survival']['beta_uniform_max_ratio'])
                  ,(w.shape[0]))
  elif kernels.shape[0]==kernels_arrival.shape[0]:
    w[sparse_pat] = np.random.uniform(float(config['Survival']['beta_uniform_min']),
                  ratio*phi.sum(axis=1))
  else:
    raise Exception('Kernel shapes should be the same or survival kernels should have shape=1')
  #w = w.reshape(d,kernels.shape[0])
  pi = np.zeros((documents_count,sites_count))
  np.random.seed(seed)
  for d_ in np.arange(pi.shape[0]):
    pi[d_,np.random.choice(sites_count,pi_count,replace=False)]=np.random.dirichlet([pi_drich]*pi_count,1)
  return {'alpha':alpha_,'pi':pi,'kernels':kernels,'length':length,'w':w,
          'replications':int(config['General']['Replications']),'kernels_arrival':kernels_arrival,'length_arrival':length_arrival,
          'documents':documents_count,'sites':sites_count,'membership':membership,'phi':phi,'gamma':gamma,'arrival_bandwidth':float(config['Arrival']['arrival_bandwidth']),
          'kernel_type':config['Survival']['kernel_type'],'kernel_type_arrival':config['Arrival']['kernel_type'],'T':float(config['General']['Time']),'arrival_mode':mode,'one_removal':one_removal,'hawkes_kernel_type':hawkes_kernel_type}

def generateModelKernelGamma(d = 10, s= 4, items = 6,k=0.1,b=0.1,kernels = 5,L=30,length_min=0.3,length_max=0.4):
  if type(kernels) is int:
    kernels = np.logspace(0,np.log10(L+1),num=kernels)-1
  length = np.logspace(np.log10(length_min+1),np.log10(length_max+1),num=kernels.shape[0])-1

  pi_ = np.random.uniform(size=s)
  pi_ = pi_/pi_.sum()
  pi_[pi_<0.01] = 0.01
  pi_ = pi_/pi_.sum()
  kappa_ = np.random.uniform(0.1,k,size=s)#np.random.gamma(0.5,1.0,s)
  index = np.random.multinomial(1,pi_,(d,items))>0
  w = np.random.uniform(0,b,size=(d,kernels.shape[0]))
  return {'kappa':kappa_,'index':index,'pi':pi_,
          'kernels':kernels,'length':length,'w':w,
          'documents':d,'items':items,'sites':s}



def experimentKernelGamma(s,d,items,T,name="",fix_pi=False,fix_kappa=False,fix_beta_temp=False,
                     k=0.1,b=0.1,output_path = "./output/",L=20,kernels=2,length_min=0.3,length_max=0.4,
                         model = None, simulation = None, simOnly = False,verbose = False):
  if model is None:
    model = generateModelKernelGamma(d=d,s=s,items =items, kernels = kernels,L=L,k=k,b=b,length_min = length_min,
                             length_max=length_max)
    path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.model.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['items'],T,datetime.datetime.now()))
    fi = open(path,'wb')
    pickle.dump({'model':model},fi)
    fi.close()
  else:
    fi = open(model,'rb')
    model = pickle.load(fi)['model']

  if simulation is None:
    result = reliability.simulator(model,T)
    path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.simul.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,datetime.datetime.now()))
    fi = open(path,'wb')
    pickle.dump({'model':model,'data':result,'T':T},fi)
    fi.close()
    if simOnly:
      return
  else:
    fi = open(simulation,'rb')
    out = pickle.load(fi)
    result = out['data']
    T = out['T']
    model = out['model']

  results,pi,kappa,beta_temp = inference.inferenceKernelFrailty(result,model['documents'],model['items'],
                                              model['sites'], T, init_sites,model['kernels'],model['length'],
                                              fix_pi = model['pi'] if fix_pi else None,
                                              fix_kappa = model['kappa'] if fix_kappa else None,
                                              fix_beta_temp = model['w'] if fix_beta_temp else None,verbose = verbose)
  path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,datetime.datetime.now()))
  fi = open(path,'wb')
  pickle.dump({'model':model,'data':result,'negLoglike':results,'kappa':kappa, 'T':T,
               'pi':pi,'beta_temp':beta_temp,'fix_kappa': fix_kappa, 'fix_beta_temp': fix_beta_temp, 'fix_pi': fix_pi},fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path

def experimentKernelGammaReal(path,name,documents,output_path = "./output/",kernels=10,length_min=1500,length_max=1501,
                              log=True,verbose=True,min_count=None,lamb=0.0,lda_path=None,min_items=0,predict=0.0):
  from wikidatasets import wikidataRead
  data_,sites,titles,start,end = wikidataRead.loadFile(path,documents,min_count,min_items,predict)
  if 'train' in data_:
    data = data_['train']
  else:
    data = data_
  if type(kernels) is int:
    size = kernels
  else:
    size = len(kernels)

  if log:
    length = np.logspace(np.log10(length_min+1),np.log10(length_max+1),num=size)-1
  else:
    length = np.linspace(length_min,length_max,num=size)

  max_time = max(data['times'].max(),data['times_survived'].max())
  max_document = max(data['nodes'][:,0].max(),data['nodes_survived'][:,0].max())
  max_item = max(data['nodes'][:,1].max(),data['nodes_survived'][:,1].max())
  max_sites = len(sites)-1

  if type(kernels) is int:
    if log:
      kernels = np.logspace(np.log10(1),np.log(max_time+1),num=kernels)-1
    else:
      kernels = np.linspace(0,max_time,num=kernels)
  else:
    kernels = np.array(kernels)
  logging.info('starting inference')
  if lda_path is not None:
    lda = pickle.load(open(lda_path,'rb'))
    lda = normalize(lda, norm='l1', axis=1)
  else:
    lda=None
  results,pi,kappa,beta_temp = inference.inferenceKernelFrailty(data,
                                                       int(max_document)+1,
                                                       int(max_item)+1,
                                                       int(max_sites)+1,None,None,kernels,length,
                                                               verbose = verbose, lamb = lamb, lda = lda)
  path_out = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.pickle" % (name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
  fi = open(path_out,'wb')
  export_dict = {'model':{'kernels':kernels,'length':length},'data':data,'negLoglike':results,'kappa':kappa,
               'T':max_time,'pi':pi,'beta_temp':beta_temp,'sites':sites,'titles':titles,'start':start,'lamb': lamb}
  if 'train' in data_:
    export_dict['orig'] = data_['orig']
    export_dict['predict_arrival'] = data_['predict_arrival']
    export_dict['predict_reliable'] = data_['predict_reliable']

  pickle.dump(export_dict,fi)
  fi.close()
  fi = open(path_out,'rb')
  data = pickle.load(fi)
  return data,path

def mergeData(data_set):
  data_keys=list(data_set[0].keys())
  out = dict()
  for k in data_keys:
    out[k] = []
  for d in range(len(data_set)):
    for k in data_keys:
      out[k].append(data_set[d][k])
  for k in data_keys:
    out[k] = np.vstack(out[k])
  return out

def experimentKernel(config, fix_pi=False,fix_alpha=False,fix_beta_temp=False,output_path = "./output/", model = None, simulation = None, simOnly = False,verbose = False,seed=123,seed_2=987654,iterations=300,seed_3=321,lamb=0.0,predict=0):
  start_p_time = time.time()
  name = config['General']['Name']
  T = float(config['General']['Time'])
  if simulation is None:
    model = generateModelKernel(config,seed=seed,seed_2=seed_2)
    path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.model.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],
                                                                           T,datetime.datetime.now()))
    result = reliability.simulator(model,T,seed_3,split=False)
    path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.simul.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,datetime.datetime.now()))
    fi = open(path,'wb')
    pickle.dump({'model':model,'data':result,'T':T},fi)
    fi.close()
    if simOnly:
      return
  else:
    fi = open(simulation,'rb')
    out = pickle.load(fi)
    result = out['data']
    model = out['model']
    if type(result) is list:
      result = mergeData(result)
    T = out['T']
  predict_set = None
  if predict > 0:
    result, predict_set = cross_validate.crossSample(result,predict,predict)

  print(model['documents'],model['replications'])
  results,pi,alpha,beta_temp = inference.inferenceKernel(result,model['documents'],
                                              model['sites'], T, model['kernels'],model['length'],
                                              fix_pi = model['pi'] if fix_pi else None,
                                              fix_alpha = model['alpha'] if fix_alpha else None,lda=model['membership'],
                                              fix_beta_temp = model['w'] if fix_beta_temp else None,verbose = verbose,iterations=iterations,replications=model['replications'],lamb=lamb,kernel_type=model['kernel_type'],one_removal=model['one_removal'])
  path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.pickle" % (name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,
                                                                           datetime.datetime.now()))
  fi = open(path,'wb')
  end_p_time = time.time()
  pickle.dump({'model':model,'negLoglike':results,'alpha':alpha, 'T':T,'data':result,'sample_count':result['nodes'].shape[0],
               'pi':pi,'beta_temp':beta_temp,'fix_alpha': fix_alpha, 'fix_beta_temp': fix_beta_temp, 'fix_pi': fix_pi,'processed_time':end_p_time-start_p_time,'predict':predict_set},fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path

def experimentKernelReal(config,path,output_path = "./output/",
                         verbose=True,processed_path=None,store_processed=False,iterations=300):
  lda_topic_mean = None
  lda_titles = None
  lda = None
  documents = config.getint('General','limit_document',fallback=None)
  documents = None if documents == -1 else documents
  min_count = config.getint('General','min_count')
  min_items = config.getint('General','min_items')
  min_sites = config.getint('General','min_sites')

  kernels = config.getint('Survival','kernels_count')
  length = config.getfloat('Survival','kernels_std')
  kernel_type = config.get('Survival','kernel_type')
  one_removal =  config.getboolean('Arrival','one_removal')
  lamb =  config.getfloat('Survival','lamb')
  predict =  config.getfloat('Survival','predict',fallback=0.0)
  predict_doc = config.getfloat('Survival','predict_doc',fallback=0.0)
  lda_path =  config.get('Survival','topics_path',fallback=None)
  name = config.get('Survival','name')
  iterations = config.getint('Survival','iterations',fallback=iterations)
  termination = config.getfloat('General','termination',fallback=None)

  if lda_path is not None:
    lda_res = pickle.load(open(lda_path,'rb'))
    lda,lda_titles = lda_res[0],lda_res[1]
    #from sklearn.preprocessing import normalize
    #lda_topic_mean = np.array(lda.mean(axis=0)).flatten()
    #lda_selected_topics = lda[:,lda_topic_mean>lda_topic_threshold]
    #lda_selected_docs_index = np.array((lda_selected_topics.sum(axis=1)>lda_document_threshold)).ravel()
    #lda_selected = lda_selected_topics[lda_selected_docs_index,:]
    #print("number of documents in selected lda %d, number of topics %d" % (lda_selected.shape[0],lda_selected.shape[1] ))
    lda = normalize(lda, norm='l1', axis=1)
    #lda_titles = lda_titles[lda_selected_docs_index]
  else:
    lda=None

  if processed_path:
    data_, sites,titles,start,end,lda_out = pickle.load(open(processed_path,'rb'))
  else:
    from wikidatasets import wikidataRead
    data_,sites,titles,start,end,lda_out = wikidataRead.loadFile(path,documents,min_count,min_items,min_sites=min_sites,lda_matrix=lda,lda_titles=lda_titles)
  
  if lda_path is not None:
    lda = lda[lda_out,:]
    lda_titles = lda_titles[lda_out]
  #print("Max sites %d" % len(sites))
  #print(lda.shape, "lda shape")
  if predict > 0:
    data, predict_set = cross_validate.crossSample(data_,predict,predict)
  elif predict_doc > 0:
    data, predict_set = cross_validate.crossSampleDoc(data_,predict_doc)
    predict = predict_doc
  else:
    data = data_
  if type(kernels) is int:
    size = kernels
  else:
    size = len(kernels)
  max_time = max(data['times'].max(),data['times_survived'].max())
  max_document = max(data['nodes'][:,0].max(),data['nodes_survived'][:,0].max())
  max_item = max(data['nodes'][:,1].max(),data['nodes_survived'][:,1].max())
  max_sites = len(sites)-1
  print(max_document, "document")
  if store_processed and processed_path is None:
    print("store processed")
    path_out = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.post.pickle" % (name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
    fi = open(path_out,'wb')
    pickle.dump((data_,sites,titles,start,end,lda_out),fi)
    fi.close()
  length = np.linspace(length,length,num=size)

  if type(kernels) is int:
    kernels = np.linspace(0,max_time,num=kernels)
  else:
    kernels = np.array(kernels)
  logging.info('starting inference')
  results,pi,alpha,beta_temp = inference.inferenceKernel(data,
                                                       int(max_document)+1,
                                                       int(max_sites)+1,termination,kernels,length,
                                                         verbose = verbose,lamb=lamb,lda = lda,iterations=iterations,one_removal=one_removal,kernel_type=kernel_type)
  path_out = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.pickle" % (name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
  fi = open(path_out,'wb')
  export_dict = {'model':{'kernels':kernels,'length':length},'data':data,'negLoglike':results,'alpha':alpha,
               'T':max_time,'pi':pi,'beta_temp':beta_temp,'sites':sites,'titles':titles,'start':start,'lamb':lamb}
  if predict>0:
    export_dict['predict'] = predict_set
  if lda_path is not None:
    export_dict['lda_matrix'] = lda
    export_dict['lda_titles'] = np.asarray(lda_titles)
    #export_dict['lda_selected_topics'] = lda_topic_mean>lda_topic_threshold
  pickle.dump(export_dict,fi)
  fi.close()
  fi = open(path_out,'rb')
  data = pickle.load(fi)
  return data,path

def experiment(s,d,items,T,name="",fix_pi=False,fix_beta=False,fix_alpha=False,
               a=0.1,b=0.1,output_path = "./output/"):
  model = generateModel(d,s,items,a=a,b=b)
  result = reliability.simulator(model,T)
  init_sites = np.nonzero(model['index'])[2].reshape(model['index'].shape[:2])

  results,pi,beta,alpha = inference.inference(result,model['documents'],model['items'],
                                              model['sites'], T, init_sites,
                                              fix_pi = model['pi'] if fix_pi else None,
                                              fix_beta = model['beta'] if fix_beta else None,
                                              fix_alpha = model['alpha'] if fix_alpha else None)
  path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.pickle" % (name,s,d,items,T,datetime.datetime.now()))
  fi = open(path,'wb')
  pickle.dump({'model':model,'data':result,'negLoglike':results,'alpha':alpha, 'T':T,
               'beta':beta,'pi':pi,'fix_alpha': fix_alpha, 'fix_beta': fix_beta, 'fix_pi': fix_pi},fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path

def experimentArrival(config,fix_gamma = False,fix_phi=False,
                      output_path ="./outputs/",
                      verbose = False, discard=None,seed=123, iterations=10,seed_2=987654,simulation = None,seed_3=321):
  start_p_time = time.time()
  T = float(config['General']['Time'])
  if simulation is None:
    model = generateModelKernel(config,seed=seed,seed_2=seed_2)

    result = reliability.simulator(model,T,seed_3)
    path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.simul.pickle" % (config['General']['Name'],model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,datetime.datetime.now()))
    fi = open(path,'wb')
    pickle.dump({'model':model,'data':result,'T':T},fi)
    fi.close()
  else:
    fi = open(simulation,'rb')
    out = pickle.load(fi)
    result = out['data']
    T = out['T']
    model = out['model']
    d= model['documents']
  print(model['documents'])
  T_ = np.zeros(model['documents']*model['replications'])+T

  jensen = True
  if 'approximation' in config['Arrival']:
    jensen = config.getboolean('Arrival','approximation')

  results,gamma,phi = inference.inferenceArrival(result,
                                               model['documents'],0,
                                               model['sites'],model['kernels_arrival'],model['length_arrival'],
                                               model['arrival_bandwidth'],T_,verbose=verbose,
                                                 fix_gamma=model['gamma'] if fix_gamma else None
                                                 ,fix_phi=model['phi'] if fix_phi else None, lda=model['membership'],
                                                 discard=discard,replications=model['replications'],iterations=iterations,jensen=jensen,kernel_type=model['kernel_type_arrival'],mode=model['arrival_mode'],
                                                 hawkes_kernel_type=model['hawkes_kernel_type'])
  path = os.path.join(output_path,"%s-s-%d-d-%d-item-%d-T-%d-%s.pickle" % (config['General']['Name'],model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T,
                                                                           datetime.datetime.now()))
  logging.info("storing to %s" % path)
  fi = open(path,'wb')
  end_p_time = time.time()
  pickle.dump({'model':model,'negLoglike':results,'gamma':gamma, 'T':T,'data':result,'iterations':iterations,
               'phi':phi,'fix_gamma': fix_gamma, 'fix_phi': fix_phi,'processed_time':end_p_time-start_p_time},fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path

def experimentArrivalReal(config,path,output_path = "./outputs/",verbose = False,restore_path=None,
                          processed_path=None,store_processed=False):
  from wikidatasets import wikidataRead
  from dateutil import parser

  lda_topic_mean = None
  lda_titles = None
  lda=None


  documents = config.getint('General','limit_document',fallback=None)
  documents = None if documents == -1 else documents
  min_count = config.getint('General','min_count')
  min_items = config.getint('General','min_items')
  min_sites = config.getint('General','min_sites')

  kernels = config.getint('Arrival','kernels_count')
  kernel_type = config.get('Arrival','kernel_type')
  iterations = config.getint('Arrival','iterations')
  length = config.getfloat('Arrival','kernels_std')
  arrival_bandwidth =  config.getfloat('Arrival','arrival_bandwidth')
  hawkes_kernel_type =  config.get('Arrival','hawkes_kernel_type')
  jensen =  config.getboolean('Arrival','approximation')
  one_removal =  config.getboolean('Arrival','one_removal')
  discard =  config.getfloat('Arrival','discard')
  lamb =  config.getfloat('Arrival','lamb')
  predict =  config.getfloat('Arrival','predict')
  lda_path =  config.get('Arrival','topics_path',fallback=None)
  name = config.get('Arrival','name')
  mode = config.get('Arrival','mode')
  last = config.get('Arrival','last',fallback=None)

  print(name)

  if lda_path is not None:
    lda,lda_titles,_ = pickle.load(open(lda_path,'rb'))
    from sklearn.preprocessing import normalize
    #lda_topic_mean = np.array(lda.mean(axis=0)).flatten()
    #lda_selected_topics = lda[:,lda_topic_mean>lda_topic_threshold]
    #lda_selected_docs_index = np.array((lda_selected_topics.sum(axis=1)>lda_document_threshold)).ravel()
    #lda_selected = lda_selected_topics[lda_selected_docs_index,:]
    #print("number of documents in selected lda %d, number of topics %d" % (lda_selected.shape[0],lda_selected.shape[1] ))
    lda = normalize(lda, norm='l1', axis=1)
    #lda_titles = lda_titles[lda_selected_docs_index]
  else:
    lda=None

  if processed_path:
    data,sites,titles,start,end,lda_out,arrival_data= pickle.load(open(processed_path,'rb'))
  else:
    data,sites,titles,start,end,lda_out,arrival_data = wikidataRead.loadFile(path,documents,min_count,min_items,predict=0, arrival=True, jitter=False,lda_matrix=lda,lda_titles=lda_titles)
  if lda_path is not None:
    lda = lda[lda_out,:]
    lda_titles = lda_titles[lda_out]

  if type(kernels) is int:
    size = kernels
  else:
    size = len(kernels)

  length = np.linspace(length,length,num=size)
 
  max_time = max(data['times'].max(),data['times_survived'].max())
  max_document = max(data['nodes'][:,0].max(),data['nodes_survived'][:,0].max())
  max_item = max(data['nodes'][:,1].max(),data['nodes_survived'][:,1].max())
  max_sites = len(sites)-1
  if store_processed and processed_path is None:
    print("store processed")
    path_out = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.post.pickle" % (name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
    fi = open(path_out,'wb')
    pickle.dump((data,sites,titles,start,end,lda_out,arrival_data),fi)
    fi.close()

  if type(kernels) is int:
    kernels = np.linspace(0,max_time,num=kernels)
  else:
    kernels = np.array(kernels)
  #last = parser.parse(str("2014-07-08")) #parser.parse(str(end))
  if last is not None:
    last = parser.parse(last)
    term = [k for k in map(lambda d: (last-parser.parse(str(d))).total_seconds()/(60*60*24),start)]
  else:
    term = [k for k in map(lambda d: (parser.parse(str(d[0]))-parser.parse(str(d[1]))).total_seconds()/(60*60*24),zip(end,start))]
  def store_items_call_back(items):
    path = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.items.pickle" % (name,len(sites),
                                                                             int(max_document)+1,max_time,
                                                                             datetime.datetime.now()))
    logging.info("storing items to %s" % path)
    fi = open(path,'wb')
    pickle.dump({'model':{'kernels':kernels,'length':length,'arrival_bandwidth':arrival_bandwidth},'items':items,
                   'T':max_time,'sites':sites,'titles':titles,'start':start,'lda':lda},fi,protocol=3)
    fi.close()
  print(restore_path)
  def restore_items():
    path = restore_path
    logging.info("restoring items from %s" % path)
    fi = open(path,'rb')
    data = pickle.load(fi)
    fi.close()
    return data['items']
  results,gamma,phi = inference.inferenceArrival(arrival_data, int(max_document)+1,int(max_item)+1,
                                                 len(sites),kernels,length,arrival_bandwidth, term,verbose=verbose,
                                                 discard=discard,lambd=lamb,iterations=iterations,
                                                 store_items_call_back = store_items_call_back,
                                                 restore_items_call_back=restore_items if restore_path else None,lda=lda, jensen=jensen, kernel_type=kernel_type, mode=mode,
                                                 hawkes_kernel_type=hawkes_kernel_type)
  path = os.path.join(output_path,"%s-s-%d-d-%d-T-%d-%s.pickle" % (name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
  logging.info("storing to %s" % path)
  fi = open(path,'wb')
  export_dict = {'model':{'kernels':kernels,'length':length,'arrival_bandwidth':arrival_bandwidth},'data':data,'negLoglike':results,'gamma':gamma,
                 'T':max_time,'phi':phi,'sites':sites,'titles':titles,'start':start,'lambd':lamb}
  if lda_path is not None:
    export_dict['lda_matrix'] = lda
    export_dict['lda_titles'] = np.asarray(lda_titles)
    #export_dict['lda_selected_topics'] = lda_topic_mean>lda_topic_threshold
  pickle.dump(export_dict,fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path
