#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: profile=False
import numpy as np
cimport numpy as np
from scipy.stats import norm
from scipy import sparse
cimport cython
cimport cython
from cpython cimport array
#import array
import time
np.set_printoptions(threshold=np.nan)

cdef np.ndarray[np.float64_t, ndim=2] computeGrid(np.ndarray[np.float64_t, ndim=2] t, np.ndarray[np.float64_t, ndim=1] kernels_mean, np.ndarray[np.float64_t, ndim=1] kernels_length):
  return norm.pdf(t,loc = kernels_mean,scale = kernels_length)
cdef np.ndarray[np.float64_t, ndim=2] computeGrid_cdf(np.ndarray[np.float64_t, ndim=2] t, np.ndarray kernels_mean, np.ndarray kernels_length):
  return norm.cdf(t,loc = kernels_mean,scale = kernels_length)
cdef np.ndarray[np.float64_t, ndim=1] computeArrivalGrid(np.ndarray[np.float64_t, ndim=1] t,float arrival_bandwidth):
  return norm.pdf(t,loc = 0,scale = arrival_bandwidth)
cdef np.ndarray[np.float64_t, ndim=1] computeArrivalGrid_cdf(np.ndarray[np.float64_t, ndim=1] t,float arrival_bandwidth):
  return norm.cdf(t,loc = 0,scale = arrival_bandwidth)

cpdef tuple createDataSetArrival(list events_split,int docs_count,int sites_count,int items_count,
                        np.ndarray[np.float64_t, ndim=1] kernels_mean, np.ndarray[np.float64_t, ndim=1] kernels_length,
                        float arrival_bandwidth, np.ndarray[np.float64_t, ndim=1] T,float discard = -1,max_influence=0):
#def createDataSetArrival(events,docs_count,sites_count,items_count,kernels_mean,kernels_length,arrival_bandwidth,T,
#                        discard=-1):


  #cdef list items_item_inf_col,items_item_inf_row
  #cdef list items_item_inf_dat
  #cdef list items_item_inf_cdf
  #cdef list items_item_inf_col_sites
  #cdef list items_item_inf_sites
  cdef np.float64_t dist
  cdef int d
  #cdef np.float64_t T_,start_t
  #cdef np.int64_t survived
  #cdef np.ndarray[np.uint8_t, ndim=1, cast=True] j,k,
  #cdef np.ndarray[np.int64_t, ndim=1] ind
  #cdef np.ndarray t,tDiff_cdf,sites

  cdef tuple items
  #precomputing bandwidth
  #arrival_bandwidth = np.sqrt(arrival_bandwidth)
  kernels_length = kernels_length
  zero = np.array([0.0])
  items_item_inf_row= np.zeros(events_split[0]['times'].shape[0]+1)
  print(items_item_inf_row.shape)
  items_item_inf_col= np.zeros(events_split[0]['times'].shape[0]+1)
  items_item_inf_dat= np.zeros(events_split[0]['times'].shape[0]+1)
  end_all = np.zeros((0))
  end_list = []
  sites_all = np.zeros((0))
  times_all = np.zeros((0,2))
  nodes_all = np.zeros((0,2))
  total_records = 0
  c = 0
  for event_ in events_split:

    offset = total_records
    event = dict()
    event['times'] = np.concatenate([event_['times'],event_['times_survived']])
    event['nodes'] = np.concatenate([event_['nodes'],event_['nodes_survived']])
    event['sites'] = np.concatenate([event_['sites'],event_['sites_survived']])
    start = time.time()
    T_ = T[event['nodes'][0,0]]
    end = (np.zeros(event['nodes'].shape[0])+T_) - event['times'][:,1]
    end[event_['times'].shape[0]:] = 0
    end_all = np.concatenate([end_all,end])
    end_list.append(T_)
    sites_all = np.concatenate([sites_all,event['sites'].ravel()])
    times_all = np.concatenate([times_all,event['times']])
    nodes_all = np.concatenate([nodes_all,event['nodes']])
    if discard > -1:
      dist = discard#norm.ppf(discard,loc = 0,scale = arrival_bandwidth)
    count_selected = np.zeros(event['times'].shape[0])
    for start_t in event['times'][:,0]:
      # Don't consider items too far away.
      k = event['times'][:,1] <= start_t
      k = np.logical_and(k, event['times'][:,1] > 0)
      if discard > -1:
        k = np.logical_and(k, event['times'][:,1] > (start_t-dist))

      #if max_influence>0 and k.sum()>0:
      #  k = np.logical_and(k, count_selected < max_influence)
      k_lim = k
      while items_item_inf_col.shape[0] < c+k_lim.sum():
        print("extending")
        items_item_inf_col.resize(items_item_inf_col.shape[0]*2)
        items_item_inf_row.resize(items_item_inf_col.shape[0]*2)
        items_item_inf_dat.resize(items_item_inf_col.shape[0]*2)
      count_selected[k_lim] = count_selected[k_lim]+1
      items_item_inf_col[c:(c+k_lim.sum())] = np.arange(event['times'].shape[0])[k_lim]+offset
      items_item_inf_row[c:c+k_lim.sum()] = np.ones(k.sum(),dtype='int')*total_records
      items_item_inf_dat[c:c+k_lim.sum()] = norm.pdf(start_t-event['times'][k,1],loc = 0.0,scale = arrival_bandwidth)
      c+=k_lim.sum()
      total_records += 1

  items_item_inf_col.resize(c)
  items_item_inf_row.resize(c)
  items_item_inf_dat.resize(c)
  sp_influence = sparse.csr_matrix((items_item_inf_dat,(items_item_inf_row,items_item_inf_col)),shape=(times_all.shape[0],times_all.shape[0]))
  sp_influence_cdf = sparse.csr_matrix(computeArrivalGrid_cdf(end_all.ravel(),arrival_bandwidth) - computeArrivalGrid_cdf(zero,arrival_bandwidth))
  sp_influence_sites = sparse.csr_matrix((np.ones(sites_all.shape[0]),(np.arange(sites_all.shape[0]),sites_all)),(sites_all.shape[0],sites_count))
  t = times_all
  tDiff = t[:,0]
  end_list = np.array(end_list)

  tDiff_cdf = norm.cdf(end_list[:,np.newaxis],loc = kernels_mean,scale = kernels_length)
  zero = norm.cdf(np.zeros((end_list.shape[0],1)),loc = kernels_mean,scale = kernels_length)
  tDiff_cdf = tDiff_cdf - zero
  items = (tDiff.reshape((t.shape[0],1)) ,nodes_all[:,0],computeGrid( times_all[:,0].reshape(times_all.shape[0],1),kernels_mean,kernels_length),
          tDiff_cdf, sp_influence,sp_influence_cdf,sp_influence_sites)
  return items



cpdef tuple createDataSetArrivalInh(list events_split,int docs_count,int sites_count,int items_count,
                          np.ndarray[np.float64_t, ndim=1] kernels_mean, np.ndarray[np.float64_t, ndim=1] kernels_length,
                          float arrival_bandwidth, np.ndarray[np.float64_t, ndim=1] T,float discard = -1,max_influence=0):
  #def createDataSetArrival(events,docs_count,sites_count,items_count,kernels_mean,kernels_length,arrival_bandwidth,T,
  #                        discard=-1):


    #cdef list items_item_inf_col,items_item_inf_row
    #cdef list items_item_inf_dat
    #cdef list items_item_inf_cdf
    #cdef list items_item_inf_col_sites
    #cdef list items_item_inf_sites
    cdef np.float64_t dist
    cdef int d
    #cdef np.float64_t T_,start_t
    #cdef np.int64_t survived
    #cdef np.ndarray[np.uint8_t, ndim=1, cast=True] j,k,
    #cdef np.ndarray[np.int64_t, ndim=1] ind
    #cdef np.ndarray t,tDiff_cdf,sites

    cdef tuple items
    #precomputing bandwidth
    #arrival_bandwidth = np.sqrt(arrival_bandwidth)
    kernels_length = kernels_length
    zero = np.array([0.0])
    items_item_inf_row= np.zeros(events_split[0]['times'].shape[0]+1)
    print(items_item_inf_row.shape)
    items_item_inf_col= np.zeros(events_split[0]['times'].shape[0]+1)
    items_item_inf_dat= np.zeros(events_split[0]['times'].shape[0]+1)
    end_all = np.zeros((0))
    end_list = []
    sites_all = np.zeros((0))
    times_all = np.zeros((0,2))
    nodes_all = np.zeros((0,2))
    total_records = 0
    c = 0
    for event_ in events_split:

      offset = total_records
      event = dict()
      event['times'] = np.concatenate([event_['times'],event_['times_survived']])
      event['nodes'] = np.concatenate([event_['nodes'],event_['nodes_survived']])
      event['sites'] = np.concatenate([event_['sites'],event_['sites_survived']])
      start = time.time()
      T_ = T[event['nodes'][0,0]]
      end = (np.zeros(event['nodes'].shape[0])+T_) - event['times'][:,1]
      end[event_['times'].shape[0]:] = 0
      end_all = np.concatenate([end_all,end])
      end_list.append(T_)
      sites_all = np.concatenate([sites_all,event['sites'].ravel()])
      times_all = np.concatenate([times_all,event['times']])
      nodes_all = np.concatenate([nodes_all,event['nodes']])
      if discard > -1:
        dist = discard#norm.ppf(discard,loc = 0,scale = arrival_bandwidth)
      count_selected = np.zeros(event['times'].shape[0])
      for start_t in event['times'][:,0]:
        # Don't consider items too far away.
        k = event['times'][:,1] <= start_t
        k = np.logical_and(k, event['times'][:,1] > 0)

        #if max_influence>0 and k.sum()>0:
        #  k = np.logical_and(k, count_selected < max_influence)
        k_lim = k
        while items_item_inf_col.shape[0] < c+k_lim.sum():
          print("extending")
          items_item_inf_col.resize(items_item_inf_col.shape[0]*2)
          items_item_inf_row.resize(items_item_inf_col.shape[0]*2)
          items_item_inf_dat.resize(items_item_inf_col.shape[0]*2)
        count_selected[k_lim] = count_selected[k_lim]+1
        items_item_inf_col[c:(c+k_lim.sum())] = np.arange(event['times'].shape[0])[k_lim]+offset
        items_item_inf_row[c:c+k_lim.sum()] = np.ones(k.sum(),dtype='int')*total_records
        items_item_inf_dat[c:c+k_lim.sum()] = 1
        c+=k_lim.sum()
        total_records += 1

    items_item_inf_col.resize(c)
    items_item_inf_row.resize(c)
    items_item_inf_dat.resize(c)
    sp_influence = sparse.csr_matrix((items_item_inf_dat,(items_item_inf_row,items_item_inf_col)),shape=(times_all.shape[0],times_all.shape[0]))
    sp_influence_cdf =  sparse.csr_matrix(end_all.ravel())
    sp_influence_sites = sparse.csr_matrix((np.ones(sites_all.shape[0]),(np.arange(sites_all.shape[0]),sites_all)),(sites_all.shape[0],sites_count))
    t = times_all
    tDiff = t[:,0]
    end_list = np.array(end_list)

    tDiff_cdf = end_list[:,np.newaxis]

    tDiff_cdf = tDiff_cdf
    items = (tDiff.reshape((t.shape[0],1)) ,nodes_all[:,0],None,
            tDiff_cdf, sp_influence,sp_influence_cdf,sp_influence_sites)
    return items
