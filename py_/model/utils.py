import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
from model import reliability
import pandas as pd
import datetime
import dateutil

def computeAggReliability(datas,no_melting=False):
  total_param = []
  total_labels = []
  w=1.0/(len(datas)+0.3)
  frailty = False
  kernel = False
  for data in datas:
    if 'kappa' in data['model']:
      frailty = True
    else:
      kernel = True
  total_params = []
  items = []
  sites_count = []
  desc = []
  times = []
  processed_time = []
  sample_ratio = []
  sample_count = []
  replications=[]
  for ind,data in enumerate(datas):
    labels = ['$\\alpha$','$\\kappa$','$\\beta$',]
    params = []
    if 'kappa' in data['model']:
      max_int = np.max(data['model']['kappa'])#+np.max(data['model']['w'])
      min_int = np.min(data['model']['kappa'])#+np.max(data['model']['w'])
      params.append(0)
      params.append(np.sqrt((np.abs(data['kappa'].flatten() - data['model']['kappa'])**2).mean()))
    else:
      np.max(data['model']['alpha'])#+np.max(data['model']['w'])
      min_int = np.min(data['model']['alpha'])#+np.max(data['model']['w'])
      params.append(np.sqrt((np.abs(data['alpha']- data['model']['alpha'])**2).mean()))
      params.append(0)
    #data['pi'] = data['pi']/data['pi'].sum()
    #pi_diff = (np.abs(np.asarray(data['pi']).flatten() - data['model']['pi'])**2).mean()
    #params.append(pi_diff)

    if 'kernels' in data['model']:
      beta_temp_diff = (np.abs(data['beta_temp'] - data['model']['w'])**2).mean()
      params.append(beta_temp_diff)
    max_p = 0
    min_p = 0
    if frailty and not kernel:
      del params[0]
      del labels[0]
      max_p = data['model']['kappa'].max()
      min_p = data['model']['alpha'].max()
    if kernel and not frailty:
      del params[1]
      del labels[1]
      max_p = data['model']['alpha'].max()
      min_p = data['model']['alpha'].min()
    if 'samples' in data:
      sample_ratio.append(data['samples'])
      sample_count.append(data['count'])
    else:
      #backward compatibility
      sample_ratio.append(1.0)
      sample_count.append(0)
    total_params.append(params)
    items.append(data['model']['documents']*data['model']['replications'])
    times.append(data['T'])
    processed_time.append(data['processed_time'])
    replications.append(data['model']['replications'])
    sites_count.append(data['model']['sites'])
    desc.append({'docs':data['model']['documents'],
                 'items':data['model']['replications'],
                 'sites':data['model']['sites'],
                'max $\\beta$':data['model']['w'].max(),'min $\\beta$':data['model']['w'].min(), 'max '+labels[0]:max_p,'min '+labels[0]:min_p,'T':data['T']})
  items_ = np.array(items)
  times_ = np.array(times)
  sites_ = np.array(sites_count)
  total_params_ = np.array(total_params)
  replications_ = np.array(replications)
  df = pd.DataFrame(data={'count':items_,'sites':sites_,'T':times_,labels[0]:total_params_[:,0],'replications':replications_,
                           labels[1]:total_params_[:,1],'sample_ratio':sample_ratio, 'sample_count':sample_count,'processed_time':processed_time})
  if no_melting:
    return df
  xlabel = "count"
  if len(df['count'].unique())>1:
    label = "count"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['count','sites','replications','sample_ratio','sample_count'] ,value_name='rmse').sort(['kind','count'])
  elif len(df['sample_ratio'].unique())>1:
    label = "sample_ratio"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['T','count','sites','replications','sample_ratio','sample_count'],value_name='rmse').sort(['kind','sample_ratio'])
  else:
    label = "T"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['T','count','sites','replications','sample_ratio','sample_count'],value_name='rmse').sort(['kind','T'])
  return df, label, desc

def plotCompare(datas):
  df,label,desc = computeAggReliability(datas)
  if df['sites'].unique().shape[0]>1:
    g = sns.factorplot(x=label, y="mse", data=df,hue="kind",col='sites',
                   palette="YlGnBu_d",size=6)
  else:
    g = sns.factorplot(x=label, y="mse", data=df,hue="kind",
                   palette="YlGnBu_d",size=6)
  g.set(yscale="log")
  #plt.title('parameters error(MSE)')
  return [plt.gcf()],pd.DataFrame(desc).groupby(['docs','replications','sites','T'], sort=True).agg(['mean', 'count']).T

def plotKernelModelSiteParam(data):
  g = (sns.jointplot(data['model']['alpha'],data['model']['pi'], stat_func=None)
         .set_axis_labels("$\\alpha$","$\pi$"))

  sns.despine(left=True,bottom=True)
  return g

def plotKernelModelSiteArrivalParam(data):
  g = (sns.jointplot(data['model']['gamma'],data['model']['pi'], stat_func=None)
         .set_axis_labels("$\\gamma$","$\pi$"))

  sns.despine(left=True,bottom=True)
  return g

def plotKernelModelDocParam(data):
  g = (sns.jointplot(np.tile(data['model']['kernels'],(data['model']['w'].shape[0],1)).ravel(),
                     data['model']['w'].ravel(), stat_func=None)
         .set_axis_labels("$r$","$\\beta$"))
  sns.despine(left=True,bottom=True)
  return g

def plotKernelModelDocArrivalParam(data):
  g = (sns.jointplot(np.tile(data['model']['kernels'],(data['model']['phi'].shape[0],1)).ravel(),
                     data['model']['phi'].ravel(), stat_func=None)
         .set_axis_labels("$r$","$\\phi$"))
  sns.despine(left=True,bottom=True)
  return g

def plotArrival2(data,d,i,steps_ = 0.1):
  from model import arrival
  step = 0.1
  I_base,steps_base,times_base = arrival.intensityTemporal(data['model'],data['data'],d,step =steps_,
                                                              onlyBase=True)
  I,steps,times = arrival.intensityTemporal(data['model'],data['data'],d,step =steps_)
  fig2 = plt.figure()

  with sns.color_palette(n_colors=2):
    if 'alpha' in data['model']:
      plt.plot(steps,I,label='true model')
      plt.plot(steps_base,I_base,label='document($\\beta$) param')
    #plt.plot(steps_new,I_new,'--',label='estimated parameters')
    #plt.plot(steps_base_new,I_base_new,'--',label='estimateddocument($\\beta$) param')
  #plt.vlines(times_new,ymin=0.45*I_new.max(),ymax=0.55*I_new.max(),label='event')
  sns.rugplot(times_base[:,0], color="k", ax=plt.gca())
  #sns.kdeplot(times_new,color='g',ax=plt.gca(),label='events histogram',clip=(0,times_new.max()))
  plt.xlabel('time')
  plt.ylabel('intensity')
  plt.legend()
  return [fig2]

def plot(data,d,plot_sites,steps_ = 0.1,time=None,color_grid='grey'):
  step = 0.1
  if 'alpha' in data['model']:
    max_int = np.max(data['model']['alpha'])#+np.max(data['model']['w'])
    min_int = np.min(data['model']['alpha'])#+np.max(data['model']['w'])

  new_data = {'alpha':np.asarray(data['alpha']).flatten(),'pi':np.asarray(data['pi']).flatten()
              }
  if 'index' in data['model']:
    new_data['index'] =  data['model']['index']
  if 'kernels' in data['model']:
    new_data['kernels'] = data['model']['kernels']
    new_data['length'] = data['model']['length']
    new_data['w']= np.asarray(data['beta_temp'])
  else:
    new_data['beta'] = np.asarray(data['beta']).flatten()
  if type(data['data']) is list:
     data_ = data['data'][d]
  else:
     data_ = data['data']
  doc_items = (data_['nodes'][:,0]==d).sum()
  sites = data_['sites'][data_['nodes'][:,0]==d]

  #plot base
  steps_base = []
  I_base = []
  for t in np.arange(0,data['T'],steps_):
    I_base.append(reliability.intensity(data['model'],d,0,0,t,onlyBase=True))
    steps_base.append(t)

  map_sites_color=dict(zip(plot_sites,np.arange(plot_sites.shape[0])))
  pallet = sns.color_palette('hls',n_colors=plot_sites.shape[0])
  ld_filter=list(filter(lambda x:sites[x] in plot_sites,range(0,doc_items,1)))
  #ld = list(map(lambda x:[pallet[sites[x][0]],pallet[sites[x][0]]],ld_filter))
  ld = list(map(lambda x:[pallet[map_sites_color[sites[x][0]]],pallet[map_sites_color[sites[x][0]]]],ld_filter))
  ld_comp = [item for sublist in ld for item in sublist]
  model=False
  objects = []
  visited_sources = []
  with sns.color_palette(ld_comp):
    for k in ld_filter:
      I,steps,times = reliability.intensityTemporal(data['model'],data_,d,k,step = steps_)
      #I = I-data['model']['alpha'][]
      site_ind = data_['sites'][k][0]
      #I = I-data['model']['alpha'][site_ind]
      #I_new,steps_new,times_new = reliability.intensityTemporal(new_data,data_,d,k,step =steps_)
      if 'alpha' in data['model']:
        if sites[k] not in visited_sources:
          visited_sources.append(sites[k])
        objects.append(plt.plot(steps,I+sites[k]*0,'-'))
        plt.plot((steps.min(),steps.max()),(2.3+0.5*visited_sources.index(sites[k]),2.3+0.5*visited_sources.index(sites[k])),'-x')
  plt.hlines(2.3+0.5*np.arange(len(visited_sources)),0,10,linestyles='-.',lw=0.7)
  plt.fill_between(steps_base,np.array(I_base),0,label='survival grid'
                   ,color=color_grid,alpha=0.7)

  plt.xlabel('time')
  plt.yticks(2.3+0.5*np.arange(len(visited_sources)),map(lambda x:"source %d" % (x+1), range(len(visited_sources))))
  plt.gca().yaxis.tick_right()
  return objects

def plotFrailtyModelSiteParam(data):
  g = (sns.jointplot(data['model']['kappa'],data['model']['pi'], stat_func=None)
         .set_axis_labels("$E[\\alpha]$","$\pi$"))
  sns.despine(left=True,bottom=True)
  return g

def plotFrailty(data,d,i,steps_ = 0.1):
  step = 0.1
  if 'kappa' in data['model']:
    max_int = np.max(data['model']['kappa'])#+np.max(data['model']['w'])
    min_int = np.min(data['model']['kappa'])
    I,steps,times = reliability.intensityTemporal(data['model'],data['data'],d,i,step =0.1,mean=True)
    I_base,steps_base,times_base = reliability.intensityTemporal(data['model'],data['data'],d,i,step =0.1,
                                                              onlyBase=True)

  new_data = {'kappa':np.asarray(data['kappa']).flatten(),'pi':np.asarray(data['pi']).flatten(),
              'w':np.asarray(data['beta_temp']),
             'kernels':data['model']['kernels'],'length':data['model']['length']}
  if 'index' in data['model']:
    new_data['index'] =  data['model']['index']

  I_new,steps_new,times_new = reliability.intensityTemporal(new_data,data['data'],d,i,step =0.1,mean=True)
  I_base_new,steps_base_new,times_base_new = reliability.intensityTemporal(new_data,data['data'],d,i,step =0.1,
                                                              onlyBase=True)

  #plt.colorbar()
  fig2 = plt.figure()
  with sns.color_palette(n_colors=2):
    if 'kappa' in data['model']:
      plt.plot(steps,I,label='true model')
      plt.plot(steps_base,I_base,label='document($\\beta$) param')
    plt.plot(steps_new,I_new,'--',label='estimated parameters')
    plt.plot(steps_base_new,I_base_new,'--',label='estimateddocument($\\beta$) param')
  #plt.vlines(times_new,ymin=0.45*I_new.max(),ymax=0.55*I_new.max(),label='event')
  #sns.rugplot(times_new, color="k", ax=plt.gca())
  #sns.kdeplot(times_new,color='g',ax=plt.gca(),label='events histogram',clip=(0,times_new.max()))
  plt.xlabel('time')
  plt.ylabel('intensity')
  plt.legend()

  fig3 = plt.figure()
  if 'kappa' in data['model']:
    kappa_diff = (np.abs(data['kappa'] - data['model']['kappa'])**2).mean()
    pi_diff = (np.abs(data['pi'] - data['model']['pi'])**2).mean()
    params = [kappa_diff,pi_diff]
    labels = ['$\\kappa$','$\\pi$']
    if 'kernels' in data['model']:
      beta_temp_diff = ((data['beta_temp'] - data['model']['w'])**2).mean()
      labels.append('$\\beta$')
      params.append(beta_temp_diff)
    sns.barplot(np.array(labels),np.array(params),palette="Set3")
    plt.title('parameters error(MSE)')

  return [fig2,fig3]
def convertDataFrame(data,min_count=0,max_count=-1,step=100,titles=None):
  if 'kappa' in data:
    new_data = {'kappa':np.asarray(data['kappa']).flatten()}
  else:
    new_data = {'alpha':np.asarray(data['alpha']).flatten()}

  new_data['pi']=np.asarray(data['pi']).flatten()
  new_data['w']=np.asarray(data['beta_temp'])
  new_data['kernels']=data['model']['kernels']
  new_data['length']=data['model']['length']
  new_data['kernel_type']=data['model']['kernel_type']

  res_document = None
  res_times = None
  res_steps = None
  res_intensity = None
  res_title = None
  df = pd.DataFrame(columns=['document','intensity','steps','times'])
  #print(data['titles'])
  if titles is None:
    selected = np.unique(data['titles'][min_count:max_count])
  else:
    selected = titles
  for ti in selected:
    v = np.arange(data['titles'].shape[0])[data['titles']==ti][0]
    if (data['data']['nodes'][:,0]==v).sum()==0:
      continue
    i,steps = reliability.intensityTemporalWikiBase(new_data,data['data'],v,0,step=step)
    start_d = dateutil.parser.parse(str(data['start'][v]))
    helper_start = np.vectorize(lambda x:str(datetime.timedelta(days=x)+start_d))

    title = np.zeros(i.shape[0], dtype=data['titles'].dtype)
    title[:] = ti
    if res_document is None:
      res_document = np.zeros(i.shape[0])+v
      res_intensity = i
      res_steps = helper_start(steps)
      res_title = title
    else:
      res_document = np.append(res_document,np.zeros(i.shape[0])+v)
      res_intensity = np.append(res_intensity,i)
      res_steps = np.append(res_steps,helper_start(steps))
      res_title = np.append(res_title,title)
  df = pd.DataFrame({'document':res_document,'titles':res_title,
                    'intensity':res_intensity,
                    'steps':res_steps
                    })
  df['steps'] = pd.to_datetime(df['steps'])
  #df = df.set_index('steps')
  return df

def convertDataFrameArrival(data,min_count=0,max_count=-1,step=100,titles=None):
  from model import arrival
  new_data = {'gamma':np.asarray(data['gamma']).flatten(),
              'phi':np.asarray(data['phi']),'arrival_bandwidth':data['model']['arrival_bandwidth'],
              'kernels_arrival':data['model']['kernels_arrival'],'length_arrival':data['model']['length_arrival'],'kernel_type_arrival':data['model']['kernel_type_arrival']}
  res_document = None
  res_times = None
  res_steps = None
  res_intensity = None
  res_title = None
  df = pd.DataFrame(columns=['document','intensity','steps','times'])
  #print(data['titles'])
  if titles is None:
    titles = np.unique(data['titles'][min_count:max_count])
  for ti in titles:
    v = np.arange(data['titles'].shape[0])[data['titles']==ti][0]
    if (data['data']['nodes'][:,0]==v).sum()==0:
      continue
    i,steps = arrival.intensityTemporalWikiBase(new_data,data['data'],v,0,step=step)
    start_d = dateutil.parser.parse(str(data['start'][v]))
    helper_start = np.vectorize(lambda x:str(datetime.timedelta(days=x)+start_d))

    title = np.zeros(i.shape[0], dtype=data['titles'].dtype)
    title[:] = ti
    if res_document is None:
      res_document = np.zeros(i.shape[0])+v
      res_intensity = i
      res_steps = helper_start(steps)
      res_title = title
    else:
      res_document = np.append(res_document,np.zeros(i.shape[0])+v)
      res_intensity = np.append(res_intensity,i)
      res_steps = np.append(res_steps,helper_start(steps))
      res_title = np.append(res_title,title)
  df = pd.DataFrame({'document':res_document,'titles':res_title,
                    'intensity':res_intensity,
                    'steps':res_steps
                    })
  df['steps'] = pd.to_datetime(df['steps'])
  #df = df.set_index('steps')
  return df

def plotRealIntensities(df,titles,g=None):
  if g is None:
    g = sns.FacetGrid(df[df['titles'].isin(titles)], col="titles",aspect=3.0,col_wrap=int(np.sqrt(len(titles)))+1,
                      sharey=False,sharex=False)
  def plot(x, y, data=None, label=None, **kwargs):
    data.plot(x, y, label=label, grid=True, **kwargs)
  g.map_dataframe(plot,"steps", "intensity")
  return g
def plotSites(data,min_count):
  index = ''
  if 'kappa' in data:
    index = 'kappa'
  if 'alpha' in data:
    index = 'alpha'

  df = pd.DataFrame(data['sites'],columns = ['site','count'])
  label_index = "$\\"+index+"$"
  df[label_index] = data[index].flatten()
  df = df.sort(label_index)
  df['labels'] = df['site']+df['count'].map(lambda x:"{:5.1f}K".format(x/1000.0))
  g = sns.factorplot(x='labels',y= label_index,data=df[df['count']>min_count],kind='bar',aspect=3.0,color="grey")
  g.set_xticklabels(rotation=90)
  g.set(yscale="log")
  return g

def plotSitesArrival(data,min_count,max_count=None):
    index = 'gamma'
    index_label = '$\\gamma$'
    df = pd.DataFrame(data['sites'],columns = ['site','count'])
    df[index_label] = data[index]
    df = df.sort(index_label)
    df['labels'] = df['site']+df['count'].map(lambda x:"{:5.1f}K".format(x/1000.0))
    max_count = df['count'].max()+1 if max_count is None else max_count
    selected_df = df[(df['count']>min_count) & (df['count']<max_count)]
    sorted_selected = selected_df['count'].argsort().tolist()
    g = sns.factorplot(x='labels',y= index_label,data=selected_df,
        kind='bar',aspect=3.0,legend=False,color="grey")
    g.set_xticklabels(rotation=90)
    #g.set(yscale="log")
    return g

def plotArrival(data,d,i,steps_ = 0.1):
  from model import arrival
  step = 0.1
  max_int = np.max(data['model']['gamma'])#+np.max(data['model']['w'])
  min_int = np.min(data['model']['gamma'])#+np.max(data['model']['w'])
  I,steps,times = arrival.intensityTemporal(data['model'],data['data'],d,step = steps_)
  I_base,steps_base,times_base = arrival.intensityTemporal(data['model'],data['data'],d,step =steps_,
                                                              onlyBase=True)
  new_data = {'gamma':np.asarray(data['gamma']).flatten()}
  new_data['kernels'] = data['model']['kernels']
  new_data['length'] = data['model']['length']
  new_data['arrival_bandwidth']= np.asarray(data['model']['arrival_bandwidth'])
  new_data['phi']= np.asarray(data['phi'])


  I_new,steps_new,times_new = arrival.intensityTemporal(new_data,data['data'],d,step =steps_)
  I_base_new,steps_base_new,times_base_new = arrival.intensityTemporal(new_data,data['data'],d,step =steps_,
                                                              onlyBase=True)
  fig2 = plt.figure()

  with sns.color_palette(n_colors=2):
    if 'gamma' in data['model']:
      plt.plot(steps,I,label='true model')
      plt.plot(steps_base,I_base,label='document($\\phi$) param')
    plt.plot(steps_new,I_new,'--',label='estimated parameters')
    plt.plot(steps_base_new,I_base_new,'--',label='estimateddocument($\\beta$) param')
  #plt.vlines(times_new,ymin=0.45*I_new.max(),ymax=0.55*I_new.max(),label='event')
  sns.rugplot(times_base[:,0], color="k", ax=plt.gca())
  #sns.kdeplot(times_new,color='g',ax=plt.gca(),label='events histogram',clip=(0,times_new.max()))
  plt.xlabel('time')
  plt.ylabel('intensity')
  plt.legend()

  fig3 = plt.figure()
  if 'gamma' in data['model']:
    gamma_diff = ((np.asarray(data['gamma']).ravel() - data['model']['gamma'])**2).mean()
    params = [gamma_diff]
    labels = ['$\\gamma$']
    beta_temp_diff = ((data['phi'] - data['model']['phi'])**2).mean()
    labels.append('$\\phi$')
    params.append(beta_temp_diff)
    g = sns.barplot(np.array(labels),np.array(params),palette="Set3")
    g.set_yscale('log')
    plt.title('parameters error(MSE)')
  return [fig2,fig3]

def errorCompute(column,column_est):
  return np.asarray((column - column_est)).flatten()
def errorStd(column,column_est):
  diff = (column - column_est)
  return np.power(diff - np.mean(diff),2)

def computeAggArrival(datas,no_melting=False):
  total_param = []
  total_labels = []
  w=1.0/(len(datas)+0.3)
  frailty = False
  kernel = False
  total_params = []
  total_stds = []
  items = []
  sites_count = []
  desc = []
  times = []
  replications = []
  sample_ratio = []
  sample_count = []
  iterations = []
  likelihood = []
  processed_time = []
  for ind,data in enumerate(datas):
    labels = []
    params = []
    stds = []
    param_count = 0
    #arrival
    if 'phi' in data:
      phi_diff = errorCompute(data['model']['phi'],data['phi'])
      labels.extend(['$\\phi$']*phi_diff.shape[0])
      params.extend(phi_diff.tolist())
      #stds.append(errorStd(data['model']['phi'],data['phi']))
      param_count+=phi_diff.shape[0]
      gamma_diff = errorCompute(data['model']['gamma'],data['gamma'])
      #stds.append(errorStd(data['model']['gamma'],data['gamma']))
      labels.extend(['$\\gamma$']*gamma_diff.shape[0])
      params.extend(gamma_diff.tolist())
      param_count+=gamma_diff.shape[0]
      print(param_count,len(labels),phi_diff.shape,gamma_diff.shape)
    #removal
    if 'beta_temp' in data:
      beta_diff = errorCompute(data['model']['w'],data['beta_temp'])
      labels.extend(['$\\beta$']*beta_diff.shape[0])
      params.extend(beta_diff.tolist())
      param_count+=beta_diff.shape[0]
      alpha_diff = errorCompute(data['model']['alpha'],data['alpha'])
      labels.extend(['$\\alpha$']*alpha_diff.shape[0])
      params.extend(alpha_diff.tolist())
      param_count+=alpha_diff.shape[0]
      print(param_count,len(labels),beta_diff.shape,alpha_diff.shape)
    if 'processed_time' in data:
      processed_time.extend([data['processed_time']]*param_count)
    items.extend([data['model']['documents']*data['model']['replications']]*param_count)
    times.extend([data['T']]*param_count)
    total_params.extend(params)
    total_stds.extend(stds)
    total_labels.extend(labels)
    likelihood.extend([data['negLoglike']]*param_count)
    if 'samples' in data:
      sample_ratio.extend([data['samples']]*param_count)
      sample_count.extend([data['count']]*param_count)
    else:
      #backward compatibility
      sample_ratio.extend([np.nan]*param_count)
      sample_count.extend([np.nan]*param_count)
    replications.extend([data['model']['replications']]*param_count)
    sites_count.extend([data['model']['sites']]*param_count)
    desc.append({'docs':data['model']['documents']/data['model']['replications'],'likelihood':likelihood,
                 'sites':data['model']['sites'],'replications':data['model']['replications'],
                'T':data['T']})
  #print(times[:2])
  items_ = np.asarray(items).flatten()
  times_ = np.asarray(times).flatten()
  iterations_ = np.array(iterations).flatten()
  sites_ = np.array(sites_count).flatten()
  total_params_ = np.array(total_params).flatten()
  total_stds_ = np.array(total_stds).flatten()
  replications_ = np.array(replications).flatten()
  sample_ratio_ = np.array(sample_ratio).flatten()
  sample_count_ = np.array(sample_count).flatten()
  processed_time_ = np.array(processed_time).flatten()
  likelihood_ = np.array(likelihood).flatten()
  total_labels_ = np.array(total_labels).flatten()
  print(total_params_.shape,processed_time_.shape)
  df = pd.DataFrame(data={'count':items_,'sites':sites_,'T':times_,'error':total_params_,
                          'replications':replications_,
                          'likelihood':likelihood_,'replications':replications_,
                          'sample_ratio':sample_ratio_ ,'sample_count':sample_count_,
                          'processed_time':processed_time_,'kind':total_labels_})
  if no_melting:
    return df,desc
  xlabel = "count"

  if len(df['replications'].unique())>1:
    label = "replications"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['count','sites','replications','sample_ratio','sample_count','iterations' ],value_name='rmse').sort(['kind','replications'])
  elif len(df['sample_ratio'].unique())>1:
    label = "sample_ratio"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['count','sites','replications','sample_ratio','sample_count','iterations'],value_name='rmse').sort(['kind','sample_ratio'])
  else:
    label = "count"
    df = pd.melt(df, value_vars=labels,var_name='kind',id_vars=['count','sites','replications','sample_ratio','sample_count','iterations' ],value_name='rmse').sort(['kind','count'])
  return df,desc

def plotCompareArrival(datas):
  fig = plt.figure()

  df,label,desc = computeAggArrival(datas)
  if df['sites'].unique().shape[0]>1:
    g = sns.factorplot(x=label, y="mse", data=df,hue="kind",col='sites',
                   palette="YlGnBu_d",size=6,)
  else:
    g = sns.factorplot(x=label, y="mse", data=df,hue="kind",
                   palette="YlGnBu_d",size=6)
  g.set(yscale="log")
  #plt.title('parameters error(MSE)')
  return [plt.gcf()],pd.DataFrame(desc).groupby(['docs','sites','T','replications'], sort=True).agg(['mean', 'count']).T

def plotSitesArrivalReliability(data_arrival,data_reliability,min_count,max_count=None):
  index = ''
  if 'kappa' in data_reliability:
    index = 'kappa'
  if 'alpha' in data_reliability:
    index = 'alpha'

  df = pd.DataFrame(data_reliability['sites'],columns = ['site','count'])
  df[index] = data_reliability[index]
  df = df.set_index('site')
  df_arr = pd.DataFrame(data_arrival['sites'],columns = ['site','count'])
  df_arr['gamma'] = data_arrival['gamma']
  df_arr= df_arr.set_index('site')

  #df = df.sort(index)
  df = df.join(df_arr[['gamma']])
  df = df.reset_index()
  df['labels'] = df['site']+df['count'].map(lambda x:"{:5.1f}K".format(x/1000.0))
  max_count = df['count'].max()+1 if max_count is None else max_count
  dat = df[(df['count']>min_count) & (df['count']<max_count)]
  #gsns.pairplot(df, hue="species")
  with sns.color_palette("PuBuGn_d", dat.shape[0]):
    g = sns.jointplot("gamma", "kappa", data=dat, kind="reg", stat_func=None,
                    color='k', size=10,xlim=(-3,np.max(dat['gamma'])))
    g.ax_joint.cla()
    markers = ['o','^','<','*','d']*100
    dat = dat.sort('gamma')
    #Plot each individual point separately
    for i,row in enumerate(dat[["gamma", "kappa","labels"]].values):
      g.ax_joint.plot(row[0], row[1], marker=markers[i],label=row[2],markersize=13)
    g.legend_out = True

    #g.despine(left=True)
    plt.legend(loc='upper left')
    g.set_axis_labels('$\\gamma$', '$\\kappa$', fontsize=16)
#plot and utils for prediction tasks
def extractPredictions(data):
  train_time = data['data']['times'][data['data']['original_order']]
  train_time_set = train_time[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  train_time_set_not_survived = train_time_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]
  train_doc = data['data']['nodes'][data['data']['original_order']]
  train_doc_set = train_doc[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  train_doc_set_not_survived = train_doc_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]
  train_sites = data['data']['sites'][data['data']['original_order']]
  train_sites_set = train_sites[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  train_sites_set_not_survived = train_sites_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]

  true_set = data['orig']['times'][data['predict_reliable']]
  true_survived = data['orig']['survived'][data['predict_reliable']]
  true_set_not_survived =  data['orig']['times'][np.logical_and(np.logical_not(data['orig']['survived']),data['predict_reliable'])]
  return true_set_not_survived, train_sites_set_not_survived, train_doc_set_not_survived, train_time_set_not_survived
def plotPrediction(length_file,label,topic=None):
  import pickle
  time_error = []
  for time in length_file:
    file = length_file[time]
    data = pickle.load(open(file,
                         'rb'))
    if topic is not None:
      model = pickle.load(open(topic,
                         'rb'))
      from sklearn.preprocessing import normalize
      model = normalize(model, norm='l1', axis=1)
    true_set_not_survived, train_sites_set_not_survived, train_doc_set_not_survived, train_time_set_not_survived = extractPredictions(data)
    if 'kappa' in data:
      data['model']['kappa'] = data['kappa']
      data['model']['kappa'][data['model']['kappa']<0] = 0
    else:
      data['model']['alpha'] = data['alpha']
      data['model']['alpha'][data['model']['alpha']<0] = 0
    data['model']['w'] = data['beta_temp']
    def predictRemoval(i):
      if topic is not None:
        return reliability.intensityLDA(data['model'],train_doc_set_not_survived[i,0],train_doc_set_not_survived[i,1],train_sites_set_not_survived[i],
                        train_time_set_not_survived[i,1],topic=model,mean=True)
      else:
        return reliability.intensity(data['model'],train_doc_set_not_survived[i,0],train_doc_set_not_survived[i,1],train_sites_set_not_survived[i],
                        train_time_set_not_survived[i,1],mean=True)
    predictRemoval_vec = np.vectorize(predictRemoval)
    predictions_interval = predictRemoval_vec(np.arange(train_doc_set_not_survived.shape[0]))
    predictions = 1.0/predictions_interval
    plot_ed =(1.0/predictions_interval)<1500
    time_error.append((time,((np.median(np.abs(true_set_not_survived[plot_ed,1]-train_time_set_not_survived[plot_ed,1]-predictions[plot_ed]))))))
  data = np.array(time_error)
  sorted_data = data[np.argsort(data[:,0]),:]
  plt.plot(sorted_data[:,0],sorted_data[:,1],'-x',label=label)
  plt.xlabel('length of prediction')
  plt.ylabel('error median')
  plt.legend()
def extractSurvived(data):
  train_time = data['data']['times'][data['data']['original_order']]
  train_time_set = train_time[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  #train_time_set_not_survived = train_time_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]
  train_doc = data['data']['nodes'][data['data']['original_order']]
  train_doc_set = train_doc[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  #train_doc_set_not_survived = train_doc_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]
  train_sites = data['data']['sites'][data['data']['original_order']]
  train_sites_set = train_sites[data['predict_reliable'][ np.logical_not(data['predict_arrival'])]]
  #train_sites_set_not_survived = train_sites_set[np.logical_not(data['orig']['survived'][data['predict_reliable']])]

  true_set = data['orig']['times'][data['predict_reliable']]
  true_survived = data['orig']['survived'][data['predict_reliable']]

  return true_survived, train_sites_set, train_doc_set, train_time_set
def plotROC(files,model,time, title):
  import pickle
  from sklearn.metrics import f1_score
  from sklearn import metrics
  plt.hold(True)
  for k in files:
    data = pickle.load(open(files[k],'rb'))
    true_survived, train_sites_set, train_doc_set, train_time_set = extractSurvived(data)
    from sklearn.preprocessing import normalize
    model = normalize(model, norm='l1', axis=1)
    lda=False
    if 'kappa' in data:
      data['model']['kappa'] = data['kappa']
      data['model']['kappa'][data['model']['kappa']<0] = 0
      if len(data['model']['kappa'].shape)>1:
        lda = True
    else:
      data['model']['alpha'] = data['alpha']
      data['model']['alpha'][data['model']['alpha']<0] = 0
      if len(data['model']['alpha'].shape)>1:
        lda = True
    data['model']['w'] = data['beta_temp']
    data['model']['w'][:] = 0
    train_time = data['data']['times'][data['data']['original_order']]
    def predictRemoval(i):
      rel = reliability.computeLambLDA(data['model'],train_doc_set[i,0],train_doc_set[i,1],train_sites_set[i],
                        train_time_set[i,1],topic=model,mean=True)
      return rel
    predictRemoval_vec = np.vectorize(predictRemoval)
    predictions_interval = predictRemoval_vec(np.arange(train_doc_set.shape[0]))
    predictions = np.exp(-(predictions_interval))

    predicted_survived = predictions>time
    fpr, tpr, thresholds = metrics.roc_curve(true_survived, predicted_survived, pos_label=1)
    plt.plot(fpr, tpr, label='%s-ROC curve (area = %0.2f)' % (k,metrics.auc(fpr, tpr)))
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="lower right")

def plotRealIntensities(df,titles,g=None,cols=1,skip=2,show_title=False,yticks_arr=7,
                        yticks_rem=6):
  if g is None:
    g = sns.FacetGrid(df[df['titles_rem'].isin(titles)], col="titles_rem",aspect=3.0,col_wrap=cols,
                      sharey=False,sharex=False,legend_out=False)
  labels = []
  ticksx=[]
  ticksy=[]
  rem_axis=[]
  #colors = sns.color_palette("RdBu_r", n_colors=2)
  colors = sns.diverging_palette(10, 220, sep=80, n=2)[::-1]
  def plot(x, y, data=None, label=None, **kwargs):
    if 'titles_rem' in data:
      ti = data['titles_rem'].iloc[0]
    labels.append(ti)
    #ax2 = None
    if label == 'removal':
      ax2 = plt.gca().twinx()
      k = data.plot(x, y,ax = ax2,label=label,**kwargs)
      rem_axis.append(ax2)
      ax2.legend(loc='upper right',bbox_to_anchor=(-0.57,1.2, 1.6, .14))

    else:
      ax2 = plt.gca()
      k = data.plot(x, y, label=label, title = ti,ax=ax2, **kwargs)
    ticksy.append(ax2.axes.get_yticks())
    #last_ticks = np.linspace(data[y].min(),data[y].max(),3)
    ticksx.append(ax2.axes.get_xticks())

    #ax2.set_yticklabels(map(lambda x:"%.2f" % x,ticks))
    #ax2.set_ylabel(label)


  k = g.map_dataframe(plot,"steps_arr", "intensity_arr",color=colors[0],label='arrival')
  shape_x = ticksx[0].shape[0]
  shape_y = ticksy[0].shape[0]
  k.set(yticks=ticksy[0][::yticks_arr])
  #g.set(xticks=ticksx[0][::1])


  k_ = g.map_dataframe(plot,"steps_rem", "intensity_rem",color=colors[1],label='removal')
  shape_x = ticksx[1].shape[0]
  shape_y = ticksy[1].shape[0]
  rem_axis[0].set(yticks=ticksy[1][::yticks_rem])
  rem_axis[0].set(xticks=ticksx[1][::skip])
  #rem_axis[0].set_yticklabels(map(lambda x:("%01.3f") % (x),rem_axis[0].get_yticks()))
  #g.axes[0].set_yticklabels(map(lambda x:("%01.2f") % (x),g.axes[0].get_yticks()))
  #g.set(yticks=[])
  g.set(xlabel='')
  g.set(ylabel='')
  g.despine(right=False)
  if not show_title:
    g.set_titles("")
  g.axes[0].legend(loc=2,bbox_to_anchor=(-0.02, 1.2, 1.6, .14))
  return g

def intensityTemporalWikiBase(model,events,d,i,step=0.1):
  times = events['times'][events['nodes'][:,0]==d,:]
  if 'times_survived' in events:
    times_survived = events['times_survived'][events['nodes_survived'][:,0]==d,:]
    times_merged = np.concatenate([times,times_survived])
  else:
    times_merged = times
  max_T = np.max(times_merged)+1
  min_T = np.min(times_merged)
  steps = np.arange(min_T,max_T,step)
  I = np.zeros(steps.shape[0])
  for ind,t in enumerate(steps):
    I[ind] = intensity(model,d,i,False,t,False,True)
  return I,steps

def convertDataFrameCluster(data,centeroids,step=10,part1=True):
  new_data = {'alpha':np.asarray(data['alpha']).flatten(),'pi':np.asarray(data['pi']).flatten(),
              'w':centeroids[:,:centeroids.shape[1]/2] if part1 else centeroids[:,centeroids.shape[1]/2:],
             'kernels':data['model']['kernels'],'length':data['model']['length'],'kernel_type':data['model']['kernel_type']}
  res_document = None
  res_times = None
  res_steps = None
  res_intensity = None
  res_title = None
  df = pd.DataFrame(columns=['document','intensity','steps','times'])
  #print(data['titles'])
  for ti in range(centeroids.shape[0]):
    i,steps = reliability.intensityTemporalWikiBase(new_data,data['data'],ti,0,step=step)

    if res_document is None:
      res_document = np.zeros(i.shape[0])+ti
      res_intensity = i
      res_steps = steps
      res_title = [["cluster %d" % ti]*i.shape[0]]
    else:
      res_document = np.append(res_document,np.zeros(i.shape[0])+ti)
      res_intensity = np.append(res_intensity,i)
      res_steps = np.append(res_steps,steps)
      res_title = np.append(res_title,np.array([["cluster %d" % ti]*i.shape[0]]))
  df = pd.DataFrame({'document':res_document,'titles':res_title,
                    'intensity':res_intensity,
                    'steps':res_steps
                    })
  return df

def site_rel_ext(selected_arr,selected_rem,label,sites,legend=True,save=False,
share_y=False,xmax=0.1,ymax=1.0,ymin=-0.003):
  sns.set_style("white")
  with sns.plotting_context("paper",rc={"lines.linewidth": 4,
                          'xtick.labelsize':35,
                          'ytick.labelsize':35,
                          'legend.fontsize': 40.0,
                         'axes.labelsize': 40}):
    ax = plt.gca()
    plt.scatter(selected_arr[label],selected_rem[label],
                color=sns.color_palette("Blues", 10)[3]
               ,label='')
    #plt.xlim(0,xmax)
    #plt.ylim(0,ymax)

    plt.xlim(0,xmax)
    plt.ylim(ymin,ymax)
    xmin, _ = plt.gca().get_xlim()
    ymin, _ = plt.gca().get_ylim()
    xmin =0
    #ymin = -0.003
    dps = plt.gcf().dpi_scale_trans.inverted()
    bbox = ax.get_window_extent().transformed(dps)
    width, height = bbox.width, bbox.height
    hw = 1./20.*(ymax-ymin)
    hl = 1./20.*(xmax-xmin)
    lw = 5. # axis line width
    ohg = 0.3 # arrow overhang


    ax.arrow(xmin, ymin, xmax-xmin, xmin, fc='k', ec='k', lw = lw/3,
             head_width=hw, head_length=hl, overhang = ohg,
             length_includes_head= True, clip_on = False)
    yhw = hw/(ymax-ymin)*(xmax-xmin)* height/width
    yhl = hl/(xmax-xmin)*(ymax-ymin)* width/height
    ax.arrow(0, ymin, 0., ymax-ymin, fc='k', ec='k', lw = lw,
             head_width=yhw, head_length=yhl, overhang = ohg,
             length_includes_head= True, clip_on = False)




    #plt.ylabel("exciation")
    #plt.yticks((0,1),("low excitation","high excitation"))

    #grid.plot_marginals(sns.rugplot,height=1,color='lightgreen')
    def appPoint(domain,**kwarg):
      ind_y = selected_rem[label][selected_rem[label].index==domain]
      ind_x = selected_arr[label][selected_arr[label].index==domain]
      plt.scatter(ind_x,ind_y,label=domain,**kwarg)

    colors = sns.color_palette("Set1", len(sites))
    for ind,si in enumerate(sites):
      appPoint(domain=si,color=colors[ind],marker='x',lw=23)
    if legend:
      plt.legend(loc='upper left',ncol=1,bbox_to_anchor=(-0.07, 1.80, 1., .102))




    plt.xticks(np.linspace(0,xmax,3))
    plt.yticks(np.linspace(0,ymax,3))
    #plt.xticks([])
    #plt.yticks([])
    plt.xlim(0,xmax)
    plt.ylim(ymin,ymax)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    plt.xlabel("$\\gamma$  (links/day)",labelpad=12)
    if share_y:
      plt.ylabel("")
    else:
      plt.ylabel("$\\alpha$ (1/day)",rotation=90,horizontalalignment='center',labelpad=12)
    if save:
      plt.savefig("./paper_plots/UnrExc-%s.png" % label,bbox_inches = 'tight')
