import numpy as np
from scipy.stats import norm
def intensity(model,d,i,next_site,t,mean=False,onlyBase=False,replications=1):
  alpha_i = 0
  if onlyBase is False:
    if 'kappa' in model:
      kappa_i =  model['kappa'][next_site]
      if mean is False:
        alpha_i = np.random.gamma(kappa_i,1.0,1)
      else:
        alpha_i = kappa_i
    else:
      alpha_i = model['alpha'][next_site]
    if 'membership' in model:
      alpha_i = alpha_i.dot(model['membership'][d//replications,:].T)


  temporal = 0
  if model['kernel_type'] == 'RBF' :
    temporal = (norm.pdf(t,loc = model['kernels'],scale = model['length'])*model['w'][d//replications,:]).sum()
  elif model['kernel_type'] == 'CONST':
    temporal = model['w'][d//replications,0]

  if onlyBase:
    return temporal
  else:
    return alpha_i+temporal

def intensityMax(model,d,i,next_site,t,T,mean=False,onlyBase=False,replications=1):
  import scipy as sp
  from scipy import optimize
  import functools
  objective = lambda x:-intensity(model,d,i,next_site,x,mean,onlyBase,replications)
  op_result = optimize.brute(objective,ranges=[(t,T)],Ns=100)
  return -1*objective(op_result[0])

def intensityGrid(model,events,t,onlyBase=False):
  I = np.zeros((model['documents'],model['items']))
  time_set = events['times'] < t
  nodes = events['nodes'][time_set[:,0],:]
  times = events['times'][time_set[:,0]]
  sites = events['sites'][time_set[:,0]]
  for d in range(model['documents']):
    for i in range(model['items']):
      j = np.logical_and(nodes[:,0] ==d ,nodes[:,1] == i)
      if (sites[j,:].shape[0]>0):
        site = np.zeros((model['sites']),dtype='bool')
        site[sites[j,:][-1,0]] = 1
        I[d,i] = intensity(model,d,i,site,t,onlyBase=onlyBase)
      else:
        I[d,i] = intensity(model,d,i,None,t,onlyBase=onlyBase)
  return I

def intensityTemporalWikiBase(model,events,d,i,step=0.1):
  times = events['times'][events['nodes'][:,0]==d,:]
  if 'times_survived' in events:
    times_survived = events['times_survived'][events['nodes_survived'][:,0]==d,:]
    times_merged = np.concatenate([times,times_survived])
  else:
    times_merged = times
  max_T = np.max(times_merged)+1
  min_T = np.min(times_merged)
  steps = np.arange(min_T,max_T,step)
  I = np.zeros(steps.shape[0])
  for ind,t in enumerate(steps):
    I[ind] = intensity(model,d,i,False,t,False,True)
  return I,steps

def intensityTemporal(model,events,d,i,step=0.1,mean=False,onlyBase=False):
  I = np.zeros(int(np.floor(np.max(events['times'])/step)))
  j = events['nodes'][:,0] ==d
  times = events['times'][j,:]
  sites = events['sites'][j,:]
  max_T = np.max(times) if times.shape[0] != 0 else 1.0

  steps = np.arange(times[i,0],times[i,1],step)
  #print(steps.shape[0])
  I = np.zeros(steps.shape[0])
  for k,t in enumerate(steps):
    #site = sites[times<t]
    I[k] = intensity(model,d,0,sites[i],t,mean,onlyBase)
  return I,steps,times[i,1]

def sample(Is):
  u = np.random.rand()*Is.sum()
  sumIs = 0
  for d in range(Is.shape[0]):
    sumIs = sumIs + Is[d]
    if sumIs >= u:
      return d

def simulatorArrival(model,T,seed, spliting=True):
  np.random.seed(seed)
  from model import arrival
  init_size = 10
  times = np.zeros((init_size,2))
  nodes = np.zeros((init_size,2),dtype='int')-1
  sites = np.zeros((init_size,1),dtype='int')
  n = -1
  print('---------model parameters---------')
  print('kernels',model['kernels'])
  print('kernels arrival',model['kernels_arrival'])
  print('kernels length',model['length'])
  print('arrival kernels length',model['length_arrival'])
  print('replacement length',model['arrival_bandwidth'])
  print('reliability wights',model['w'].shape)
  print('arrival kernel weights',model['phi'].shape)
  print('membership',model['membership'].shape)
  print('gamma of arrival', model['gamma'].shape)
  print('alpha of removal', model['alpha'].shape)
  print('pi',model['pi'].shape)
  print('only one removed item?',model['one_removal'])
  print('------------------------------------')
  events = {"times":times,"nodes":nodes,"sites":sites}
  for d in range(0,model['documents']*model['replications']):
    t = 0
    next_site = None
    j = 0
    surv =0
    earliest_remove = -1
    print('document', d)
    while (True):
      I_max = arrival.intensityMax(model,d,t,{"times":events['times'][:n+1,:],
                                                                    "nodes":events['nodes'][:n+1,:],
                                                                    "sites":events['sites'][:n+1,:]}
                                                                 ,T,temporal_only=False,
                                                                  replications=model['replications'])
      #print('found maximum for arrival')
      deltat=np.random.exponential(1.0/I_max,1)
      t_candidate = t+ deltat
      if t_candidate > T:
        ## Discard empty documents
        if j ==0:
          t = 0
          surv = 0
          j = 0
          continue
        break
      I_candidate = arrival.intensity(model,d,t_candidate,{"times":events['times'][:n+1,:],
                                     "nodes":events['nodes'][:n+1,:],
                                    "sites":events['sites'][:n+1,:]},temporal_only=False,replications=model['replications'])
      if I_candidate>I_max:
        print("Max smaller than candidate in Arrival")
      ## rejection sampling
      t_temp = t
      t=t_candidate
      accept_ratio = np.random.uniform(0,1,1)
      if accept_ratio>I_candidate/I_max:
        #print('rejected Ican %f, Imax %f, t %f, samples %d' % (I_candidate,I_max,t,j))
        continue
      n=n+1
      if n==events['nodes'].shape[0]:
        temp = events['nodes']
        events['nodes'] = np.zeros((events['nodes'].shape[0]*2,events['nodes'].shape[1]),dtype='int')
        events['nodes'][:n,:] = temp
        temp = events['times']
        events['times'] = np.zeros((events['times'].shape[0]*2,events['times'].shape[1]))
        events['times'][:n,:] = temp
        temp = events['sites']
        events['sites'] = np.zeros((events['sites'].shape[0]*2,events['sites'].shape[1]),dtype='int')
        events['sites'][:n,:] = temp
      j = j+1
      next_site = (np.random.multinomial(1,model['pi'][d//model['replications'],:])>0).nonzero()
      events['times'][n,0] = t
      events['sites'][n] = next_site
      end_candidate_t = t
      while(True):
        I_end_max = intensityMax(model,d,-1,next_site,end_candidate_t,T,replications=model['replications'])
        end_candidate_t = end_candidate_t+ np.random.exponential(1.0/I_end_max,1)
        I_candidate_end =  intensity(model,d,-1,next_site,end_candidate_t,replications=model['replications'])
        if end_candidate_t>T:
          surv += 1
          break

        if np.random.uniform(0,1.0,1)<I_candidate_end/I_end_max:
          break
  
      if end_candidate_t > T:
        events['times'][n,1] = T
        events['nodes'][n,:] = [d,0]
      else:
        events['times'][n,1] = end_candidate_t
        events['nodes'][n,:] = [d,j]
        if model['one_removal']:
          if earliest_remove == -1:
            earliest_remove = n
          elif end_candidate_t < events['times'][earliest_remove,1]:
            events['times'][earliest_remove,1] = T
            surv += 1
            earliest_remove = n
          else:
            surv += 1
            events['times'][n,1] = T
    ind = events['nodes'][:,0] == d
    #sorted = np.argsort(events['nodes'][ind,1])[::-1]
    #events['nodes'][ind,:] = events['nodes'][ind,:][sorted,:]
    #events['times'][ind,:] = events['times'][ind,:][sorted,:]
    #events['sites'][ind,:] = events['sites'][ind,:][sorted,:]
    print('document %d, generated %d samples, time %f, %d survived' % (d,j,t,surv))

  events['times'] = events['times'][:n+1]
  events['nodes'] = events['nodes'][:n+1,:]
  events['sites'] = events['sites'][:n+1,:]
  survived = events['times'][:,1] == T
  def split(doc_index):
    selected = events['nodes'][:,0]==doc_index
    non_surv = np.logical_and(selected,np.logical_not(survived))
    surv = np.logical_and(selected,survived)
    return {"times":events['times'][non_surv,:],
            "nodes":events['nodes'][non_surv,:],
            "sites":events['sites'][non_surv,:],
            "times_survived":events['times'][surv,:],
            "nodes_survived":events['nodes'][surv,:],
            "sites_survived":events['sites'][surv,:]}
  if spliting:
    return list(map(split,range(int(model['documents']*model['replications']))))
  else:
    print("not splitling")
    events['times_all'] = events['times']
    events['nodes_all'] = events['nodes']
    events['sites_all'] = events['sites']

    events['times_survived'] = events['times_all'][survived]
    events['nodes_survived'] = events['nodes_all'][survived]
    events['sites_survived'] = events['sites_all'][survived]

    events['times'] = events['times_all'][np.logical_not(survived)]
    events['nodes'] = events['nodes_all'][np.logical_not(survived)]
    events['sites'] = events['sites_all'][np.logical_not(survived)]
    print(events['times_survived'].shape)
    events.pop('nodes_all', 0)
    events.pop('sites_all', 0)
    return events

def simulator(model,T,seed,split=True):
    return simulatorArrival(model,T,seed,split)

def computeLamb(kernels, kernels_length, beta_temp, alpha, time,nodes, sites):
  from scipy import sparse
  from scipy.stats import norm
  computeGrid_cdf = lambda t: norm.cdf(t,loc = kernels,scale = np.sqrt(kernels_length))
  cdf_beta_end = computeGrid_cdf((time[:,1])[:,np.newaxis])
  cdf_beta_start = computeGrid_cdf((time[:,0])[:,np.newaxis])
  cdf_beta_diff = cdf_beta_end - cdf_beta_start
  cdf_site = time[:,1] - time[:,0]
  item_document = sparse.csr_matrix((np.ones(nodes.shape[0]),(np.arange(nodes.shape[0]),nodes[:,0].flatten())),(nodes.shape[0],beta_temp.shape[0]))

  item_site = sparse.csr_matrix((np.ones(nodes.shape[0]),(np.arange(nodes.shape[0]),sites.flatten())),(nodes.shape[0],alpha.shape[0]))

  print(( (cdf_beta_diff*(item_document.dot(beta_temp))).sum(axis=1)).shape)
  beta_temp[beta_temp<0] = 0
  alpha[alpha<0] = 0
  lamb = (cdf_beta_diff*(item_document.dot(beta_temp))).sum(axis=1)[:,np.newaxis] + cdf_site[:,np.newaxis]*item_site.dot(alpha)
  return lamb

def computeLambLDA(kernels, kernels_length, beta_temp, alpha, time,nodes, sites, topics):
  from scipy import sparse
  from scipy.stats import norm
  computeGrid_cdf = lambda t: norm.cdf(t,loc = kernels,scale = np.sqrt(kernels_length))
  cdf_beta_end = computeGrid_cdf((time[:,1])[:,np.newaxis])
  cdf_beta_start = computeGrid_cdf((time[:,0])[:,np.newaxis])
  cdf_beta_diff = cdf_beta_end - cdf_beta_start
  cdf_site = time[:,1] - time[:,0]
  item_document = sparse.csr_matrix((np.ones(nodes.shape[0]),(np.arange(nodes.shape[0]),nodes[:,0].flatten())),(nodes.shape[0],beta_temp.shape[0]))
  item_topic = item_document.dot(topics)
  item_site = sparse.csr_matrix((np.ones(nodes.shape[0]),(np.arange(nodes.shape[0]),sites.flatten())),(nodes.shape[0],alpha.shape[0]))
  
  beta_temp[beta_temp<0] = 0
  alpha[alpha<0] = 0
  weights = item_topic.multiply(item_site.dot(alpha)).sum(1)
  lamb = (cdf_beta_diff*(item_document.dot(beta_temp))).sum(axis=1)[:,np.newaxis] + np.multiply(cdf_site[:,np.newaxis],weights)
  return lamb
