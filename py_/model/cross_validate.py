import numpy as np
import random
#{'nodes_survived':final[0],'times_survived':final[1],'sites_survived':final[2], 'nodes':final[4],'times':final[5],'sites':final[6]}
def crossSample(data,ratio_survived, ratio_removed):
  #survived
  sample_prediction = dict()
  sample_train = dict()
  survived_index_ = random.sample(range(data['nodes_survived'].shape[0]),int(data['nodes_survived'].shape[0]*ratio_survived))
  print("number of survived items",data['nodes_survived'].shape[0])
  survived_index = np.array(survived_index_)
  sample_prediction['nodes_survived'] = data['nodes_survived'][survived_index]
  sample_prediction['sites_survived'] = data['sites_survived'][survived_index]
  sample_prediction['times_survived'] = data['times_survived'][survived_index]

  survived_index_train_ = [item for item in range(data['nodes_survived'].shape[0]) if item not in survived_index_]
  survived_index_train = np.array(survived_index_train_)
  sample_train['nodes_survived'] = data['nodes_survived'][survived_index_train]
  sample_train['sites_survived'] = data['sites_survived'][survived_index_train]
  sample_train['times_survived'] = data['times_survived'][survived_index_train]

  #removed
  removed_index_ = random.sample(range(data['nodes'].shape[0]),int(data['nodes'].shape[0]*ratio_removed))
  print("number of removed items",data['nodes'].shape[0])
  removed_index = np.array(removed_index_)
  sample_prediction['nodes'] = data['nodes'][removed_index]
  sample_prediction['sites'] = data['sites'][removed_index]
  sample_prediction['times'] = data['times'][removed_index]

  removed_index_train_ = [item for item in range(data['nodes'].shape[0]) if item not in removed_index_]
  removed_index_train = np.array(removed_index_train_)
  sample_train['nodes'] = data['nodes'][removed_index_train]
  sample_train['sites'] = data['sites'][removed_index_train]
  sample_train['times'] = data['times'][removed_index_train]

  return (sample_train,sample_prediction)


def crossSampleDoc(data,ratio):
  #survived  i
  np.random.seed(1)
  perm_titles = np.random.permutation(np.unique(data['nodes'][:,0]))
  pred_titles = perm_titles[:int(ratio*perm_titles.shape[0])]
  train_titles = perm_titles[int(ratio*perm_titles.shape[0]):]

  #print(titles_train)
  sample_prediction = dict()
  sample_train = dict()
  survived_index = np.in1d(data['nodes_survived'][:,0],pred_titles)
  print("number of records in predict",survived_index.sum())
  sample_prediction['nodes_survived'] = data['nodes_survived'][survived_index]
  sample_prediction['sites_survived'] = data['sites_survived'][survived_index]
  sample_prediction['times_survived'] = data['times_survived'][survived_index]
  
  survived_index_train = np.in1d(data['nodes_survived'][:,0],train_titles)
  sample_train['nodes_survived'] = data['nodes_survived'][survived_index_train]
  sample_train['sites_survived'] = data['sites_survived'][survived_index_train]
  sample_train['times_survived'] = data['times_survived'][survived_index_train]

  #removed
  removed_index = np.in1d(data['nodes'][:,0],pred_titles)
  print("number of removed items",removed_index.sum())
  sample_prediction['nodes'] = data['nodes'][removed_index]
  sample_prediction['sites'] = data['sites'][removed_index]
  sample_prediction['times'] = data['times'][removed_index]

  removed_index_train = np.in1d(data['nodes'][:,0],train_titles)
  sample_train['nodes'] = data['nodes'][removed_index_train]
  sample_train['sites'] = data['sites'][removed_index_train]
  sample_train['times'] = data['times'][removed_index_train]

  return (sample_train,sample_prediction)
