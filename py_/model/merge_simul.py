import pickle
from collections import OrderedDict
import argparse
import logging
import datetime
import numpy as np

parser = argparse.ArgumentParser(description='merge vector files into one.')
parser.add_argument('-i','--input', type=str, required=True,
                   help = 'Input directory.')
parser.add_argument('-o','--output', type=str, required = True,
                   help = 'Output file')
parser.add_argument('--logging',default='I',type=str,help = 'logging level, can be [W]arning,[E]rror,[D]ebug,[I]nfo,[C]ritical')
parser.add_argument('--logging_dir',default='./experiments/',help='path for storing log files')
parser.add_argument('--count',default=None,type=int)
parser.add_argument('--step',default=None,type=int,help='number of documents per file')

def processDocument(inp):
  ind,(split,data,step) = inp
  if split:
    for l in range(len(data['data'])):
      data['data'][l]['nodes'][:,0] += step*ind
      data['data'][l]['nodes_survived'][:,0] += step*ind
  else:
    data['data']['nodes'][:,0] += step*ind
    data['data']['nodes_survived'][:,0] += step*ind
  return data,split  
def conc_array(sets):
  output = []
  for set_ in zip(*sets):
    output.append(np.concatenate(set_))
  return tuple(output)

def merge(data_set):
  model = None
  w = []
  phi = []
  membership = []
  data = []
  data_keys = None
  T = None
  output = []
  for d_set,split in data_set:
    if model is None:
      model = {'pi': d_set['model']['pi'],
               'arrival_bandwidth': d_set['model']['arrival_bandwidth'],
               'alpha': d_set['model']['alpha'],
               'gamma': d_set['model']['gamma'],
               'kernels':d_set['model']['kernels'],
               'length':d_set['model']['length'],
               'kernels_arrival':d_set['model']['kernels_arrival'],
               'length_arrival':d_set['model']['length_arrival'],
               'kernel_type':d_set['model']['kernel_type'],
               'replications':d_set['model']['replications'],
               'sites':d_set['model']['sites'],'kernel_type_arrival':d_set['model']['kernel_type_arrival'],
               'documents':0,'arrival_mode':d_set['model']['arrival_mode'],'hawkes_kernel_type':d_set['model']['hawkes_kernel_type'],
               'one_removal':d_set['model']['one_removal']}
    else:
      ##Sanity check, if parameters are not shared throw excepetion
      assert np.allclose(model['alpha'],d_set['model']['alpha']),"parameter alpha is not the samei, %f %f" % (model['alpha'][0],d_set['model']['alpha'][0])
      assert np.allclose(model['gamma'],d_set['model']['gamma']),"parameter gamma is not the same, %f %f" % (model['gamma'][0],d_set['model']['gamma'][0]) 
    if data_keys is None:
      if split is False:
        data_keys=list(d_set['data'][0].keys())
        print(data_keys)
    if T is None:
      T = d_set['T']
    w.append(d_set['model']['w'])
    phi.append(d_set['model']['phi'])
    model['documents']+=d_set['model']['documents']
    membership.append(d_set['model']['membership'])
    if split:
      data.extend(d_set['data'])
    else:
      elements= []
      for k in data_keys:
        elements.append(d_set['data'][k])
      output.append(elements)
  if len(output)>0:
    final = conc_array(output)
    data = dict(zip(data_keys,final))
  model['w'] = np.vstack(w)
  model['phi'] = np.vstack(phi)
  model['membership'] = np.vstack(membership)
  return {'model':model,'data':data,'T':T}
  
    
def loadFile(fi_list,count=None,step=1):
  ind = 0
  split = False
  for fi in fi_list:
    if ind==count:
      break 
    if ".simul.pickle" not in fi:
      continue
    data_complete = pickle.load(open(fi,'rb'))
    if type(data_complete['data']) is list:
      split = True
    else:
      split = False
    ind += 1

    yield (split,data_complete,step)
  assert ind==count, "not enough samples in file"
if __name__ == "__main__":
  import os
  args = parser.parse_args()
  level = logging.WARNING
  if args.logging == 'D':
    level = logging.DEBUG
  if args.logging == 'I':
    level = logging.INFO
  if args.logging == 'W':
    level = logging.WARNING
  if args.logging == 'E':
    level = logging.ERROR
  if args.logging == 'C':
    level = logging.CRITICAL
  logging.basicConfig(filename=os.path.join(args.logging_dir,"wiki_merge_"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+".log"),level=level)
  fi_list = [k for k in sorted(list(map(lambda x: os.path.join(args.input,x),os.listdir(args.input))))]
  # Shuffle list in a determinsitic fashion
  import random
  random.seed(123)
  random.shuffle(fi_list)
  data_set = [l for l in loadFile(fi_list,count=args.count,step = args.step)]
  processed = [l for l in map(processDocument,enumerate(data_set))]
  out = merge(processed)
  output = open(args.output, 'wb')
  print(args.output+" stored")
  pickle.dump(out, output)
  output.close()
