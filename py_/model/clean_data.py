import pickle
from collections import OrderedDict
import argparse
import logging
import datetime
import numpy as np

parser = argparse.ArgumentParser(description='clean vector files from empty documents.')
parser.add_argument('-i','--input', type=str, required=True,
                   help = 'Input directory.')
parser.add_argument('-o','--output', type=str, required = True,
                   help = 'Output file')
parser.add_argument('--logging',default='I',type=str,help = 'logging level, can be [W]arning,[E]rror,[D]ebug,[I]nfo,[C]ritical')
parser.add_argument('--logging_dir',default='./experiments/',help='path for storing log files')

def conc_array(sets):
  output = []
  for set_ in zip(*sets):
    output.append(np.concatenate(set_))
  return tuple(output)

def clean(d_set):
  model = None
  w = []
  phi = []
  membership = []
  data = []
  data_keys = None
  T = None
  output = []
  model = {'pi': d_set['model']['pi'],
               'arrival_bandwidth': d_set['model']['arrival_bandwidth'],
               'alpha': d_set['model']['alpha'],
               'gamma': d_set['model']['gamma'],
               'kernels':d_set['model']['kernels'],
               'length':d_set['model']['length'],
               'replications':d_set['model']['replications'],
               'sites':d_set['model']['sites'],
               'documents':0}

  data_keys=list(d_set['data'][0].keys())
  for d in range(d_set['model']['documents']):
    if d_set['data'][d]['nodes'].shape[0]+d_set['data'][d]['nodes_survived'].shape[0]==0:
      print('empty document found')
      continue
    
    if T is None:
      T = d_set['T']
    w.append(d_set['model']['w'][d,:])
    phi.append(d_set['model']['phi'][d,:])
    d_set['data'][d]['nodes'][:,0] = model['documents']
    model['documents']+=1
    membership.append(d_set['model']['membership'][d,:])
    data.append(d_set['data'][d])
  model['w'] = np.vstack(w)
  model['phi'] = np.vstack(phi)
  model['membership'] = np.vstack(membership)
  print("%d docs in file" % model['documents'])
  return {'model':model,'data':data,'T':T}

def loadFile(fi):
  data_complete = pickle.load(open(fi,'rb'))
  return data_complete

if __name__ == "__main__":
  import os
  args = parser.parse_args()
  level = logging.WARNING
  if args.logging == 'D':
    level = logging.DEBUG
  if args.logging == 'I':
    level = logging.INFO
  if args.logging == 'W':
    level = logging.WARNING
  if args.logging == 'E':
    level = logging.ERROR
  if args.logging == 'C':
    level = logging.CRITICAL
  logging.basicConfig(filename=os.path.join(args.logging_dir,"wiki_merge_"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+".log"),level=level)
  fi = args.input
  data_set = loadFile(fi)
  out = clean(data_set)
  output = open(args.output, 'wb')
  print(args.output+" stored")
  pickle.dump(out, output)
  output.close()
