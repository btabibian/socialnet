from collections import OrderedDict
import pandas as pd
import argparse
from scipy import sparse
import pickle
import numpy as np

parser = argparse.ArgumentParser(description='Extract tags from stackoverflow csv export')
parser.add_argument('-i','--input', required=True, type=str, help = 'stackoverflow csv dataset')
parser.add_argument('-o','--output', required=True, type=str, help = 'output file')
parser.add_argument('-t','--threshold', type=int, default=1000, help = 'minimum number of documents for each tag')

tags_set = OrderedDict()
doc_tags = dict()
def tag_extract(group):
  id_ = group['question_id'].iloc[0]
  doc_tags[id_] = []
  for tag in map(lambda a: a.strip('><'),  group['tags'].iloc[0].split('><')):
    if tag in tags_set:
      tags_set[tag]+=1
    else:
      tags_set[tag]=1
    doc_tags[id_].append(tag)

if __name__ == "__main__":
  args = parser.parse_args()
  #answer_id, user_id, time_posted, time_accepted, question_id, time_question_posted, tags
  data = pd.read_csv(args.input,parse_dates=[2,3,5],na_values='[NULL]')
  data = data.iloc[:-2] #ignore last two lines
  print('loaded file, %d records available' % data.shape[0])
  tags_set = OrderedDict()
  doc_tags = OrderedDict()
  print('processing each question...')
  tags =  data.groupby('question_id')\
    .apply(tag_extract)
  print('number of tags found: %d' % len(tags_set))
  THRESH = args.threshold
  for tag in tags_set:
    if tags_set[tag]<THRESH:
      del tags_set[tag]
  print('number of tags after discarding insufficient samples: %d' % len(tags_set))
  keys_ = list(tags_set.keys())
  row = []
  col = []
  docs = []
  current_ind = 0
  print('number of questions in dataset: %d' % len(doc_tags))
  for ind,doc in enumerate(doc_tags):
    current_tags = []
    for tag in doc_tags[doc]:
      if tag not in keys_:
        continue
      index_ = keys_.index(tag)
      current_tags.append(index_)
    if len(current_tags)>0:
      col.extend(current_tags)
      row.extend([current_ind]*len(current_tags))
      current_ind+=1
      docs.append(doc)
  print('number of documents after discarding those without tags: %d' % len(docs))
  membership = sparse.csr_matrix(([1.0]*len(row),(row,col)),shape=(len(doc_tags),len(keys_)))
  print('dumping data')
  pickle.dump((membership,np.array(list(map(str,doc_tags.keys()))),keys_),open(args.output,'wb'))
