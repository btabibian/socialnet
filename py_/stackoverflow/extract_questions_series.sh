doc_size=(1000 10000 100000 1000000 5000000)
source ./venv_par/bin/activate
for doc_index in ${doc_size[@]}; do
  python ./stackoverflow/extract_questions.py -i ./stackoverflow.csv -o ./stackoverflow/ -t 4 -c $doc_index &
done
deactivate
