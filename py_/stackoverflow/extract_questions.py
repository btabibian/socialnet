from collections import OrderedDict
import pandas as pd
import argparse
from scipy import sparse
import pickle
import os
import datetime

parser = argparse.ArgumentParser(description='Extract tags from stackoverflow csv export')
parser.add_argument('-i','--input', required=True, type=str, help = 'stackoverflow csv dataset')
parser.add_argument('-c','--cut',type=int,default=None,help='number of questions to process')
parser.add_argument('-o','--output', required=True, type=str, help = 'output directory')
parser.add_argument('-t','--threshold', type=int, default=4, help = 'minimum number of samples per source')
parser.add_argument('-d','--duration', type=float, default=4, help = 'duration of study')

if __name__ == "__main__":
  args = parser.parse_args()
  #answer_id, user_id, time_posted, time_accepted, question_id, time_question_posted, tags
  data = pd.read_csv(args.input,parse_dates=[2,3,5],na_values='[NULL]')
  data = data.iloc[:-2] #ignore last two lines
  convert_date = lambda x: int(datetime.datetime.strftime(x, '%Y%m%d%H%M%S'))
  print('read file, %d number of records' % data.shape[0])


  users = data['user_id'].unique().astype('int')
  sources = OrderedDict(zip(users,data.groupby('user_id').count()['answer_id'][users]))
  source_keys = list(sources.keys())
  
  grouped = data.groupby('question_id')
  data_set = []
  end_times = []
  start_times = []
  duration = args.duration
  
  id_ = 0
  accepted_after = 0
  for ind,(name, group_) in enumerate(grouped):
    start = group_['time_question_posted'].iloc[0]
    end = start+datetime.timedelta(days=duration)

    group = group_[group_['time_posted']<end]
    if group.shape[0]==0:
      continue
    accepted_time = group['time_accepted'][group['time_accepted'].notnull()]
    if accepted_time.shape[0]>1:
      print('multiple accepted answers! %d' % accepted_time.shape[0])
      continue

    if (group['time_question_posted']>group['time_posted']).sum()>0:
      print('traveling back in time!')
      continue

    if accepted_time.shape[0]>0:
      if accepted_time.iloc[0] > (group['time_question_posted'].iloc[0]+datetime.timedelta(days=duration)):
        continue
  
  #if accepted_time.shape[0]>0:
  #  acc = accepted_time.iloc[0]+datetime.timedelta(days=1)
  #  if (acc < group['time_posted']).sum() > (acc > group['time_posted']).sum():
  #    continue
  
  #if accepted_time.shape[0]>0:
  #  if (group['time_posted']>(accepted_time.iloc[0]+datetime.timedelta(days=1))).sum()==0:
  #    continue
  #else:
  #  accepted = None
  
    if accepted_time.shape[0]>0:
      if (group['time_posted'] < (accepted_time.iloc[0]+datetime.timedelta(days=1))).sum()>0:
        accepted_after += 1
        #print("Has more response after answered %d" % ind)
    added = False
    start_times.append(convert_date(start))
    end_times.append(convert_date(end))
    for row in group.iterrows():
      accepted = -1 if pd.isnull(row[1]['time_accepted']) else convert_date(row[1]['time_accepted']+datetime.timedelta(days=1)) 
    
      data_set.append((id_,
          int(row[1]['user_id']),
          convert_date(row[1]['time_posted']),
          accepted,row[1]['answer_id'],
          str(row[1]['question_id'])))
    if args.cut is not None and ind>args.cut:
      break
    id_ += 1
    if id_ % 10000 == 0:
      print('read %d questions' % id_)

  active_sources = OrderedDict(zip(source_keys+[-1],[0]*(len(source_keys)+1)))
  for rec in data_set:
    if rec[3] != -1:
      active_sources[rec[1]]+=1
    else:
      active_sources[-1]+=1

  print('number of sources: %d' % len(active_sources))
  min_count = args.threshold
  for a in active_sources:
    #print(a, active_sources[a])
    if active_sources[a]<min_count:
      del active_sources[a]
  print('number of active sources after prunning: %d' % len(active_sources))
  active_sources_keys = list(active_sources.keys())
  active_sources_keys_index = dict(zip(list(active_sources.keys()),range(len(active_sources))))
  
  refined = []
  refined_end = []
  refined_start = []
  temp=[]
  current_id = -1
  new_id = 0
  discard = False
  start = None
  end = None
  for row in data_set:
    if row[0]!=current_id and len(temp)>0 and discard==False: 
      refined+=temp
      current_id = row[0]
      refined_end.append(end)
      refined_start.append(start)
      temp=[]
      new_id += 1
      discard = False
      start = start_times[row[0]]
      end = end_times[row[0]]
      if new_id % 10000 == 0:
        print('%d questions in final set so far' % new_id)

    elif row[0]!=current_id and discard:
      temp=[]
      discard = False
      current_id = row[0]
      start = start_times[row[0]]
      end = end_times[row[0]]
    else:
      current_id = row[0]
      start = start_times[row[0]]
      end = end_times[row[0]]
    
    if discard:
      continue
    if row[1] not in active_sources_keys and row[3]!=-1:
      temp = []
      discard = True
      continue

    temp.append((new_id,
               active_sources_keys_index[row[1]] if row[1] in active_sources_keys_index else active_sources_keys_index[-1],
               row[2],row[3],row[4],row[5]))
  fi_ = os.path.join(args.output,"so_d-%d-s-%s" % (new_id,len(active_sources)))
  print('total number of questions in dataset %d' % new_id)
  with open(fi_,'wb') as file:
    pickle.dump({'data':refined,'sites':active_sources,'end_time':refined_end,'start_time':refined_start},file)
