import os
import pickle
from IPython.html import widgets
from IPython.display import display, clear_output
from model import utils
import numpy as np
from collections import OrderedDict
import pandas as pd
def viewer(output_dir,real=False,show_param=True):
  textInput = widgets.Text(description="Input directory",value =output_dir)
  if real:
    textPopularity = widgets.Text(description="Count",value ="2000")
  else:
    text = widgets.Text(description="item",value ="0")
    textDoc = widgets.Text(description="Document",value ="0")
  textOutput = widgets.Text(description="Output",value ="./notebooks/outputs/")
  dirs = sorted(os.listdir(textInput.value))
  li = widgets.SelectMultiple(
      options=dirs,
      #value = (dirs[-1],),
      description='experiment')
  saveBut = widgets.Button(description="Save",value ="0")
  if real:
    container = widgets.VBox(children=[textInput,textPopularity,widgets.HBox(children=[textOutput,saveBut]),li])
  else:
    container = widgets.VBox(children=[textInput,textDoc,text,widgets.HBox(children=[textOutput,saveBut]),li])
  display(container)
  def display_real(value):
    clear_output()
    fi = open(os.path.join(textInput.value,li.value[0]),'rb')
    data = pickle.load(fi)
    return utils.plotSites(data,int(textPopularity.value))
  def on_value_change(value):
    global data
    clear_output()
    datas=[]
    if len(li.value)>1:
      for path in li.value:
        if not path.endswith('.pickle'):
          continue
        fi = open(os.path.join(textInput.value,path),'rb')
        data = pickle.load(fi)
        datas.append(data)
        ### compare

      res = utils.plotCompare(datas)
      if show_param:
        display(res[1])
      return res[0]

    else:
      clear_output()
      fi = open(os.path.join(textInput.value,li.value[0]),'rb')

      data = pickle.load(fi)
      if 'kappa' in data['model']:
        return utils.plotFrailty(data,int(textDoc.value),int(text.value))
      else:
        return utils.plot(data,int(textDoc.value),int(text.value))

  def on_save(value):
    figs = on_value_change(value)
    if len(figs)>1:
      #figs[0].savefig(textOutput.value+"-sites.png",dpi=300)
      #figs[1].savefig(textOutput.value+"-doc.png",dpi=300)
      figs[0].savefig(textOutput.value+"-sample.png",dpi=300)
      figs[1].savefig(textOutput.value+"-error.png",dpi=300)
    else:
      figs[0].savefig(textOutput.value+".png",dpi=300)

  def update_path(value):
    li.options = sorted(os.listdir(textInput.value))
    #li.value = (os.listdir(output_dir)[-1],)
    import subprocess
    #val = subprocess.Popen(["rsync", "-r", "btabibian@sched.cluster.is.localnet:/home/btabibian/socialnet/py_/"+textInput.value,"./"]
    #,stderr=subprocess.PIPE)


  if real:
    textPopularity.on_submit(display_real)
  else:
    text.on_submit(on_value_change)
    textDoc.on_submit(on_value_change)
  saveBut.on_click(on_save)
  textInput.on_submit(update_path)
  #li.on_trait_change(on_value_change)
  #on_value_c4
  #hange("910")
def viewer_real(output,min_count=0,max_count=-1):
  textPopularity = widgets.Text(description="Count",value ="2000")
  textOutput = widgets.Text(description="Output",value ="./notebooks/outputs/")
  saveBut = widgets.Button(description="Save",value ="0")
  textTitle = widgets.Text(description="Title", value="")
  fi = open(output,'rb')
  data = pickle.load(fi)
  #data['titles'] = data['titles']
  data['data']['sites'] = data['data']['sites'].astype(int)
  titles = data['titles'].tolist()
  df = utils.convertDataFrame(data,min_count,max_count)
  li = widgets.SelectMultiple(
      options=titles[min_count:max_count],
      #value = (dirs[-1],),
      description='experiment')
  container = widgets.VBox(children=[textPopularity,textTitle,widgets.HBox(children=[textOutput,saveBut]),li])
  display(container)
  def display_real(value):
    clear_output()
    return utils.plotSites(data,int(textPopularity.value)),utils.plotRealIntensities(df,li.value)
  textPopularity.on_submit(display_real)
  def display_title(value):
    clear_output()
    d = np.nonzero(data['titles']==textTitle.value)
    df_temp = utils.convertDataFrame(data,d[0]-1,d[0]+1)
    return utils.plotRealIntensities(df_temp,[textTitle.value])

  textTitle.on_submit(display_title)

def viewer_real_arrival(output,min_count=0,max_count=-1):
    textPopularity = widgets.Text(description="Count",value ="2000")
    textMaxPopularity = widgets.Text(description="Max Count",value ="-1")
    textOutput = widgets.Text(description="Output",value ="./notebooks/outputs/")
    saveBut = widgets.Button(description="Save",value ="0")
    fi = open(output,'rb')
    data = pickle.load(fi)
    data['model']['length_arrival'] = data['model']['length_arrival'] if 'length_arrival' in data['model'] else data['model']['length']
    data['model']['kernels_arrival'] = data['model']['kernels_arrival'] if 'kernels_arrival' in data['model'] else data['model']['kernels']
    titles = data['titles'].tolist()
    df = utils.convertDataFrameArrival(data,min_count,max_count)
    li = widgets.SelectMultiple(
        options=titles[min_count:max_count],
        #value = (dirs[-1],),
        description='experiment')
    container = widgets.VBox(children=[textPopularity,textMaxPopularity,widgets.HBox(children=[textOutput,saveBut]),li])
    display(container)
    def display_real(value):
      clear_output()
      return utils.plotSitesArrival(data,int(textPopularity.value),None if textMaxPopularity.value=="-1" else int(textMaxPopularity.value)), \
                                    utils.plotRealIntensities(df,li.value)
    textPopularity.on_submit(display_real)
def viewer_joint(output_arr,output_rel):
  textPopularity = widgets.Text(description="Count",value ="2000")
  textMaxPopularity = widgets.Text(description="Max Count",value ="-1")
  textOutput = widgets.Text(description="Output",value ="./notebooks/outputs/")
  saveBut = widgets.Button(description="Save",value ="0")
  fi_arr = open(output_arr,'rb')
  fi_rel = open(output_rel,'rb')
  data_arr = pickle.load(fi_arr)
  data_rel = pickle.load(fi_rel)

  container = widgets.VBox(children=[textPopularity,textMaxPopularity,widgets.HBox(children=[textOutput,saveBut])])
  display(container)
  def display_real(value):
    clear_output()
    return utils.plotSitesArrivalReliability(data_arr,data_rel,int(textPopularity.value),None if textMaxPopularity.value=="-1" else int(textMaxPopularity.value))
  textPopularity.on_submit(display_real)

def getTopicsKappaDF(data_arr,kappa,topics_,min_rel_upper = 0.1,min_count=8000,max_count=-1):
  sites_count = OrderedDict(data_arr['sites'])
  sites = list(sites_count.keys())
  counts = list(sites_count.values())
  from sklearn.preprocessing import normalize
  df = pd.DataFrame(data = kappa,index =sites, columns = topics_)
  df['counts'] = counts
  plot_df = df[df['counts']>min_count]
  if max_count != -1:
    plot_df = plot_df[plot_df['counts']<max_count]
  columns = plot_df.max(axis=0)>min_rel_upper
  selected_cols = plot_df.columns[columns]
  selected_cols = selected_cols.drop("counts")
  return plot_df[selected_cols]

def topicSiteviewer(gensim_model_path,data,popularity_threshold=100000,topic_min_threshold=0.1,post=None,list_site=None):
  import gensim
  model = gensim.models.LdaModel.load(gensim_model_path)
  topics_ = []
  for t_i in range(model.num_topics):
    words =  model.show_topic(t_i, topn=5)
    #print(words)
    topics_.append(words[0][1]+"_"+words[1][1]+"_"+words[2][1]+"_"+words[3][1]+"_"+words[4][1])
  fi_arr = open(data,'rb')
  fi_arr = pickle.load(fi_arr)
  if 'lda_selected_topics' in fi_arr:
     subTopics=np.arange(len(topics_))[fi_arr['lda_selected_topics']]
     topics_ = [topics_[ind] for ind in subTopics]
  if 'alpha' in fi_arr:
    param = fi_arr['alpha']#.reshape(len(topics_),len(fi_arr['sites'])).T
  elif 'gamma' in fi_arr:
    param = fi_arr['gamma']#.reshape(len(topics_),len(fi_arr['sites'])).T
  #param[param < 0]=0
  df = getTopicsKappaDF(fi_arr,param,topics_,topic_min_threshold,min_count=popularity_threshold)
  print(df.shape)
  if post is not None:
    index = list(filter(lambda x:x.endswith(post),df.index))
    return df.loc[index]
  elif list_site is not None:
    index = list(filter(lambda x:x in list_site,df.index))
    return df.loc[index]
  else:
    return df
