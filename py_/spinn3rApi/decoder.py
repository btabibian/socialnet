from spinn3rApi import spinn3rApi_pb2,protoStream_pb2, contentExtension_pb2
import fnmatch
import os

import sys
def _VarintDecoder(mask):
    '''Like _VarintDecoder() but decodes signed values.'''

    local_ord = ord
    def DecodeVarint(buffer, pos):
        result = 0
        shift = 0
        while 1:
          if sys.version_info >= (3, 0):
            b = buffer[pos]
          else:
            b = local_ord(buffer[pos])
          result |= ((b & 0x7f) << shift)
          pos += 1
          if not (b & 0x80):
              if result > 0x7fffffffffffffff:
                  result -= (1 << 64)
                  result |= ~mask
              else:
                  result &= mask
                  return (result, pos)
          shift += 7
          if shift >= 64:
              ## need to create (and also catch) this exception class...
              raise _DecodeError('Too many bytes when decoding varint.')
    return DecodeVarint

def _VarintEncoder():
  """Return an encoder for a basic varint value."""

  local_chr = chr
  def EncodeVarint(write, value):
      bits = value & 0x7f
      value >>= 7
      while value:
        if sys.version_info >= (3, 0):
          enc = bytes([0x80|bits])
        else:
          enc = local_chr(0x80|bits)
        write(enc)
        bits = value & 0x7f
        value >>= 7
      if sys.version_info >= (3, 0):
        return write(bytes([bits]))
      else:
        return write(local_chr(bits))


  return EncodeVarint

def encodeEntry(write,dat):
  pos = 0
  encodeVarint = _VarintEncoder()
  
  header    = protoStream_pb2.ProtoStreamHeader()
  delimiter = protoStream_pb2.ProtoStreamDelimiter()
  delimiter.delimiter_type = protoStream_pb2.ProtoStreamDelimiter.ENTRY
  
  deli = delimiter.SerializeToString()
  size_deli = len(deli)
  encodeVarint(write,size_deli)
  write(deli)
  for data in dat:
    deli = delimiter.SerializeToString()
    size_deli = len(deli)
    encodeVarint(write,size_deli)
    write(deli)
    data_str = data.SerializeToString()
    encodeVarint(write,len(data_str))
    write(data_str)
  delimiter = protoStream_pb2.ProtoStreamDelimiter()
  delimiter.delimiter_type = protoStream_pb2.ProtoStreamDelimiter.END
  deli = delimiter.SerializeToString()
  size_deli = len(deli)
  encodeVarint(write,size_deli)
  write(deli)

def decodeEntry(content,type_):
  ## get a 64bit varint decoder
  decoder = _VarintDecoder((1<<64) - 1)

  ## get the three types of protobuf messages we expect to see
  #header    = protoStream_pb2.ProtoStreamHeader()
  delimiter = protoStream_pb2.ProtoStreamDelimiter()

  ## get the header
  pos= 0
  next_pos, pos = decoder(content, pos)
  delimiter.ParseFromString(content[pos:pos + next_pos])
  entry = type_()
  while 1:
    try:
      entry = type_()
      pos += next_pos
      next_pos, pos = decoder(content, pos)
      delimiter.ParseFromString(content[pos:pos + next_pos])    
      if delimiter.delimiter_type == delimiter.END:
        break

      pos += next_pos
      next_pos, pos = decoder(content, pos)

      entry.ParseFromString(content[pos:pos + next_pos])
      
      yield entry
    except Exception as inst:
      print("error: "+ str(inst))
      yield None
      break

def extractor(dir_path,match=None,per_day_sample=1.0,return_date = False, return_filename = False, match_after = None,match_before = None,ignore_if_exception=[],append_error=None):
  ''' extracts entries in a given directory structure such as dir_path/match. 
      return_date returns a tuple with element,date.
      return_filename return a tuple with element,file_name.
      if return_date is True return_filename is ignored. '''

  count = 0
  current_date = None
  files = sorted(filter(lambda f:True if match == None else fnmatch.fnmatch(f,match),os.listdir(dir_path)))
  match_after_index = 0
  if match_after is not None:
    for fi in files:
      match_after_index +=1
      if fnmatch.fnmatch(fi,match_after):
        break
  match_before_index = len(files)
  counter = 0
  if match_before is not None:
    for fi in files:
      if fnmatch.fnmatch(fi,match_before):
        match_before_index = counter
        break
      counter+=1
        

  print("First file: %s \n Last file: %s" % (files[match_after_index],files[match_before_index-1]))
  for f_set in files[match_after_index:match_before_index]:
    paths = os.listdir(dir_path+f_set)
    print(f_set, len(paths))
    step = int(1.0/per_day_sample)
    if current_date == None or f_set.split('-')[0:3]!=current_date:
      j = -1
      current_date = f_set.split('-')[0:3]
    for path in paths:
      j += 1
      if j%step != 0:
        continue 
      print(path,count)
      if fnmatch.fnmatch(dir_path+f_set+"/"+path, '*.protostream'):
        f_ = open(dir_path+f_set+"/"+path, "r")
        content = f_.read()
        if len(content) == 0:
          continue
        items=[]
        for item in decodeEntry(content,spinn3rApi_pb2.Entry):
          if item is None:
            # There was an error
            if os.path.join(dir_path,f_set,path) in ignore_if_exception:
              break
            else:
              if append_error:
                fi = open(append_error,'a') 
                fi.write(os.path.join(dir_path,f_set,path)+"\n")
                fi.close()
                break
              else:
                raise Exception("%s contains malformed content" % os.path.join(dir_path,f_set,path))
          if return_date == True:
            yield (item, '-'.join(current_date))
          elif return_filename:
            yield (item,f_set+"/"+path)
          else:
            yield item
          count += 1
    print("item counts:", count)


if __name__ == "__main__":
  #import os
  #os.chdir("../")
  from spinn3rApi import decoder,spinn3rApi_pb2
  import itertools
  import random
  import sys
  print(sys.getdefaultencoding())
  sample_rate = random.random()
  collection_14_01 = decoder.extractor("/agbs/spinn3r1/data/",match="14-01-*")
  samples = list(itertools.islice(collection_14_01,800))
  res = []
  decoder.encodeEntry(res.append,samples[0:800])
  print("done encoding")
  its = list(decoder.decodeEntry(("".join(res)),spinn3rApi_pb2.Entry))
  its_ls = its
  for i,j in zip(samples, its):
    #print("done!")
    assert i.SerializeToString() == j.SerializeToString(), "Incorrect serialization"
  
