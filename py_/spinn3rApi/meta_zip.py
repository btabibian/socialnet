import os
from spinn3rApi import decoder,spinn3rApi_pb2,mpi_spinn3rApi_pb2
import itertools
import random
from utils.extractors import extractLink
import argparse
import subprocess

parser = argparse.ArgumentParser(description='Process Protobuffer items and store them in 7zip format.')
parser.add_argument('-i','--input', type=str, required = True,
                   help = 'Input directory.')
parser.add_argument('-o','--output', type=str, required = 'True',
                   help = 'Output directory')
parser.add_argument('-m','--match', type=str, default  = None,
                   help = 'Regular expression to match only some paths')
parser.add_argument('-r','--rate', type=str, default = 1.0,
                   help = 'Sampling rate of elements.')
parser.add_argument('--social', dest = 'social', action ='store_true',
                   help = 'Include social network')
parser.add_argument('--no-social',dest = 'social', action ='store_false',
                   help = 'do not include social network data')
parser.add_argument('--content', dest = 'store_content',action = 'store_true', default = None,
                   help = 'store_contnets')
parser.add_argument('--content-path', type = str, default = "",
                   help = 'path to store contents')
parser.add_argument('--after', type=str,default = None,
                   help = 'starts parsing directories with names after given pattern')
parser.add_argument('--before', type=str,default = None,
                   help = 'ends before the matching directory')

parser.set_defaults(social=False)
args = parser.parse_args()

collection_14_01 = decoder.extractor(args.input,match=args.match,per_day_sample=args.rate,return_filename=True,match_after = args.after, match_before = args.before)
collection_14_01_meta = args.output

buff_meta = []
size_meta = 0
current_f = ""
buff_content = []
size_content = 0
ind_content = 0
def consumer_metadata(val):
  global current_f,buff_meta,buff_content
  global size_content, size_meta, ind_content
  item_ = val[0]
  file_name = val[1]
  if current_f == "":
    current_f = file_name
  if size_meta > 240000000:
    print 'writing'
    if not os.path.isdir(collection_14_01_meta+"/".join(file_name.split('/')[:-1])):
      os.mkdir(collection_14_01_meta+"/".join(file_name.split('/')[:-1]))
    writer = open(collection_14_01_meta+file_name+".tmp",'w')
    decoder.encodeEntry(writer.write,buff_meta)
    writer.close()
    subprocess.call(["7z",'a','-mx9',collection_14_01_meta+file_name+".7z",collection_14_01_meta+file_name+".tmp"])
    os.remove(collection_14_01_meta+file_name+".tmp")
    buff_meta = []
    size_meta = 0
  item_.content.data = args.content_path+current_f+"/"+str(ind_content)
  item_.content.mime_type = "Link"
  size_meta += len(item_.SerializeToString())
  buff_meta.append(item_)
  #print size_meta,size_content,item_.source.publisher_type,len(content.data)

samples = collection_14_01
samples_final = samples
res=[]
fi = itertools.imap(consumer_metadata, samples_final)
for i in fi:
  pass
if len(buff_meta) > 0:
  if not os.path.isdir(collection_14_01_meta+"/".join(file_name.split('/')[:-1])):
    os.mkdir(collection_14_01_meta+"/".join(file_name.split('/')[:-1]))
  writer = open(collection_14_01_meta+current_fi+".tmp",'w')
  decoder.encodeEntry(writer.write,buff_meta)
  writer.close()
  subprocess.call(["7z",'a','-mx9',collection_14_01_meta+current_fi+".7z",collection_14_01_meta+current_fi+".tmp"])
  os.remove(collection_14_01_meta+file_name+".tmp")
