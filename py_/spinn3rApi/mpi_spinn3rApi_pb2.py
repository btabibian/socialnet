# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: mpi_spinn3rApi.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import spinn3rApi_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='mpi_spinn3rApi.proto',
  package='contentApi',
  serialized_pb=_b('\n\x14mpi_spinn3rApi.proto\x12\ncontentApi\x1a\x10spinn3rApi.proto:<\n\rcontent_links\x12\x13.contentApi.Content\x18\x65 \x03(\x0b\x32\x10.contentApi.Link')
  ,
  dependencies=[spinn3rApi_pb2.DESCRIPTOR,])
_sym_db.RegisterFileDescriptor(DESCRIPTOR)


CONTENT_LINKS_FIELD_NUMBER = 101
content_links = _descriptor.FieldDescriptor(
  name='content_links', full_name='contentApi.content_links', index=0,
  number=101, type=11, cpp_type=10, label=3,
  has_default_value=False, default_value=[],
  message_type=None, enum_type=None, containing_type=None,
  is_extension=True, extension_scope=None,
  options=None)

DESCRIPTOR.extensions_by_name['content_links'] = content_links

content_links.message_type = spinn3rApi_pb2._LINK
spinn3rApi_pb2.Content.RegisterExtension(content_links)

# @@protoc_insertion_point(module_scope)
