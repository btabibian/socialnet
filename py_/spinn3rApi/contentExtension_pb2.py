# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: protoMessage/contentExtension.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='protoMessage/contentExtension.proto',
  package='contentExtension',
  serialized_pb=_b('\n#protoMessage/contentExtension.proto\x12\x10\x63ontentExtension\"\xc0\x01\n\x07Wrapper\x12*\n\x07twitter\x18\x01 \x01(\x0b\x32\x19.contentExtension.Twitter\x12\x34\n\x0clinkRankNode\x18\x02 \x01(\x0b\x32\x1e.contentExtension.LinkRankNode\x12.\n\ttimestamp\x18\x03 \x01(\x0b\x32\x1b.contentExtension.Timestamp\x12\x10\n\x08redirect\x18\x04 \x01(\t\x12\x11\n\tcanonical\x18\x05 \x01(\t\"\x92\x01\n\x07Twitter\x12\x11\n\tfollowers\x18\x01 \x01(\r\x12\x0f\n\x07\x66riends\x18\x02 \x01(\r\x12\x10\n\x08location\x18\x03 \x01(\t\x12\x19\n\x11profile_image_url\x18\x04 \x01(\t\x12\x0c\n\x04lang\x18\x05 \x01(\t\x12\x16\n\x0estatuses_count\x18\x06 \x01(\r\x12\x10\n\x08verified\x18\x07 \x01(\x08\"a\n\x0cLinkRankNode\x12\x0c\n\x04rank\x18\x01 \x02(\x01\x12\x10\n\x08indegree\x18\x02 \x02(\r\x12\x11\n\toutdegree\x18\x03 \x02(\r\x12\x10\n\x08resource\x18\x04 \x02(\t\x12\x0c\n\x04name\x18\x05 \x01(\t\"\x1d\n\tTimestamp\x12\x10\n\x08original\x18\x01 \x02(\tB,\n\x18\x63om.spinn3r.api.protobufB\x10\x43ontentExtension')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_WRAPPER = _descriptor.Descriptor(
  name='Wrapper',
  full_name='contentExtension.Wrapper',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='twitter', full_name='contentExtension.Wrapper.twitter', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='linkRankNode', full_name='contentExtension.Wrapper.linkRankNode', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='timestamp', full_name='contentExtension.Wrapper.timestamp', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='redirect', full_name='contentExtension.Wrapper.redirect', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='canonical', full_name='contentExtension.Wrapper.canonical', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=58,
  serialized_end=250,
)


_TWITTER = _descriptor.Descriptor(
  name='Twitter',
  full_name='contentExtension.Twitter',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='followers', full_name='contentExtension.Twitter.followers', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='friends', full_name='contentExtension.Twitter.friends', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='location', full_name='contentExtension.Twitter.location', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='profile_image_url', full_name='contentExtension.Twitter.profile_image_url', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='lang', full_name='contentExtension.Twitter.lang', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='statuses_count', full_name='contentExtension.Twitter.statuses_count', index=5,
      number=6, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='verified', full_name='contentExtension.Twitter.verified', index=6,
      number=7, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=253,
  serialized_end=399,
)


_LINKRANKNODE = _descriptor.Descriptor(
  name='LinkRankNode',
  full_name='contentExtension.LinkRankNode',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='rank', full_name='contentExtension.LinkRankNode.rank', index=0,
      number=1, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='indegree', full_name='contentExtension.LinkRankNode.indegree', index=1,
      number=2, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='outdegree', full_name='contentExtension.LinkRankNode.outdegree', index=2,
      number=3, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='resource', full_name='contentExtension.LinkRankNode.resource', index=3,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='contentExtension.LinkRankNode.name', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=401,
  serialized_end=498,
)


_TIMESTAMP = _descriptor.Descriptor(
  name='Timestamp',
  full_name='contentExtension.Timestamp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='original', full_name='contentExtension.Timestamp.original', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=500,
  serialized_end=529,
)

_WRAPPER.fields_by_name['twitter'].message_type = _TWITTER
_WRAPPER.fields_by_name['linkRankNode'].message_type = _LINKRANKNODE
_WRAPPER.fields_by_name['timestamp'].message_type = _TIMESTAMP
DESCRIPTOR.message_types_by_name['Wrapper'] = _WRAPPER
DESCRIPTOR.message_types_by_name['Twitter'] = _TWITTER
DESCRIPTOR.message_types_by_name['LinkRankNode'] = _LINKRANKNODE
DESCRIPTOR.message_types_by_name['Timestamp'] = _TIMESTAMP

Wrapper = _reflection.GeneratedProtocolMessageType('Wrapper', (_message.Message,), dict(
  DESCRIPTOR = _WRAPPER,
  __module__ = 'protoMessage.contentExtension_pb2'
  # @@protoc_insertion_point(class_scope:contentExtension.Wrapper)
  ))
_sym_db.RegisterMessage(Wrapper)

Twitter = _reflection.GeneratedProtocolMessageType('Twitter', (_message.Message,), dict(
  DESCRIPTOR = _TWITTER,
  __module__ = 'protoMessage.contentExtension_pb2'
  # @@protoc_insertion_point(class_scope:contentExtension.Twitter)
  ))
_sym_db.RegisterMessage(Twitter)

LinkRankNode = _reflection.GeneratedProtocolMessageType('LinkRankNode', (_message.Message,), dict(
  DESCRIPTOR = _LINKRANKNODE,
  __module__ = 'protoMessage.contentExtension_pb2'
  # @@protoc_insertion_point(class_scope:contentExtension.LinkRankNode)
  ))
_sym_db.RegisterMessage(LinkRankNode)

Timestamp = _reflection.GeneratedProtocolMessageType('Timestamp', (_message.Message,), dict(
  DESCRIPTOR = _TIMESTAMP,
  __module__ = 'protoMessage.contentExtension_pb2'
  # @@protoc_insertion_point(class_scope:contentExtension.Timestamp)
  ))
_sym_db.RegisterMessage(Timestamp)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n\030com.spinn3r.api.protobufB\020ContentExtension'))
# @@protoc_insertion_point(module_scope)
