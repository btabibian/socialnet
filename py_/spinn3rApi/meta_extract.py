import os
from spinn3rApi import decoder,spinn3rApi_pb2,mpi_spinn3rApi_pb2
import itertools
import random
from utils.extractors import extractLink
import argparse
import subprocess
parser = argparse.ArgumentParser(description='Process Protobuffer items and store meta data information.')
parser.add_argument('-i','--input', type=str, required = True,
                   help = 'Input directory.')
parser.add_argument('-o','--output', type=str, required = 'True',
                   help = 'Output directory')
parser.add_argument('-m','--match', type=str, default  = None,
                   help = 'Regular expression to match only some paths')
parser.add_argument('-r','--rate', type=str, default = 1.0,
                   help = 'Sampling rate of elements.')
parser.add_argument('--social', dest = 'social', action ='store_true',
                   help = 'Include social network')
parser.add_argument('--no-social',dest = 'social', action ='store_false',
                   help = 'do not include social network data')
parser.add_argument('--content', dest = 'store_content',action = 'store_true', default = None,
                   help = 'store_contnets')
parser.add_argument('--content-path', type = str, default = "", 
                   help = 'path to store contents')
parser.add_argument('--after', type=str,default = None,
                   help = 'starts parsing directories with names after given pattern') 
parser.add_argument('--before', type=str,default = None,
                   help = 'ends before the matching directory')
parser.add_argument('--czip', dest = 'czip', action = 'store_true', default = False,
                   help = 'zip content')
parser.add_argument('--mzip', dest = 'mzip', action = 'store_true', default = False,
                   help = 'zip meta')
parser.add_argument('--mspace',type=int, default = 128, 
                   help = 'meta data size per file(before zip)')
parser.add_argument('--cspace',type=int, default = 128,
                   help = 'content data size per file(before zip)')
parser.add_argument('--exceptions', type=str, default=None,
                   help = 'a list of files to ignore if an exception occured otherwise program \
                           will terminate if an exception is raised.')
parser.add_argument('--appendError',type=str, default=None,
                   help = 'pass a file name to append all malformed files and skip afterwards')
parser.set_defaults(social=False)
args = parser.parse_args()

ignore_paths = open(args.exceptions,'r').read().split('\n') if args.exceptions is not None else []
collection_14_01 = decoder.extractor(args.input,match=args.match,per_day_sample=args.rate,return_filename=True,match_after = args.after, match_before = args.before,ignore_if_exception=ignore_paths,append_error=args.appendError)
collection_14_01_meta = args.output

def producer_metadata((item_,file_name)):
  item = spinn3rApi_pb2.Entry()
  links = extractLink(item_)
  content = spinn3rApi_pb2.Content()
  content.CopyFrom(item_.permalink_entry.content)
  content.mime_type = "H" if content.mime_type == "" else content.mime_type
  item.CopyFrom(item_)
  link_set = []
  for l in links:
    new_link = spinn3rApi_pb2.Link()
    new_link.href = l
    link_set.append(new_link)
  item.permalink_entry.content.Extensions[mpi_spinn3rApi_pb2.content_links].extend(link_set)
  item.permalink_entry.content.data = ""
  item.permalink_entry.content.encoding = ""
  item.permalink_entry.content.mime_type = "" if item_.permalink_entry.content.mime_type == "" else item_.permalink_entry.content.mime_type
  if args.store_content:
    return (item,file_name,content)
  else:
    return (item,file_name)

buff_meta = []
size_meta = 0
buff_content = []
size_content = 0
ind_content = 0
curr_file_index = ""
next_content_fi = ""
def consumer_metadata(val):
  global buff_meta,buff_content,next_content_fi
  global size_content, size_meta, ind_content
  global current_file_buf, curr_file_index
  item_ = val[0]
  file_name = val[1]
  if args.store_content:
    content = val[2]
    if size_content > args.cspace*1000000 or ("/".join(curr_file_index.split('/')[0:-1]) != "/".join(file_name.split('/')[0:-1]) and curr_file_index != "") :  
      print 'writing content'
      
      if not os.path.isdir(args.content_path+"/".join(next_content_fi.split('/')[:-1])):
        os.mkdir(args.content_path+"/".join(next_content_fi.split('/')[:-1]))
      writer = open(args.content_path+next_content_fi,'w')
      decoder.encodeEntry(writer.write,buff_content)
      writer.close()
      if args.czip:
        subprocess.call(["7z",'a','-mx9',args.content_path+next_content_fi+".7z",args.content_path+next_content_fi])
        os.remove(args.content_path+next_content_fi)
      buff_content = []
      size_content = 0
      ind_content = 0
      next_content_fi = file_name
    if next_content_fi == "":
      next_content_fi = file_name
    size_content += len(content.SerializeToString())
    buff_content.append(content)
    ind_content += 1 
  

  if size_meta > args.mspace*1000000 or ("/".join(curr_file_index.split('/')[0:-1]) != "/".join(file_name.split('/')[0:-1]) and curr_file_index != ""):
    print 'writing meta' 
    if not os.path.isdir(collection_14_01_meta+"/".join(curr_file_index.split('/')[:-1])):
      os.mkdir(collection_14_01_meta+"/".join(curr_file_index.split('/')[:-1]))
    writer = open(collection_14_01_meta+curr_file_index,'w')
    decoder.encodeEntry(writer.write,buff_meta)
    writer.close()
    if args.mzip:
      subprocess.call(["7z",'a','-mx9',collection_14_01_meta+curr_file_index+".7z",collection_14_01_meta+curr_file_index])
      os.remove(collection_14_01_meta+curr_file_index)
    buff_meta = []
    size_meta = 0
  if curr_file_index != file_name:
    curr_file_index = file_name
  item_.permalink_entry.content.data = args.content_path+next_content_fi+"/"+str(ind_content)
  item_.permalink_entry.content.mime_type = "Link"
  size_meta += len(item_.SerializeToString())
  buff_meta.append(item_)
  #print size_meta,size_content,item_.source.publisher_type,len(content.data)

samples_ = collection_14_01
samples = itertools.imap(producer_metadata, samples_)
if not args.social:
  samples_final = itertools.ifilter(lambda x:not ((x[0].source.publisher_type=="SOCIAL_MEDIA") or (x[0].source.publisher_type == "MICROBLOG")), samples)
else:
  samples_final = samples
res=[]
fi = itertools.imap(consumer_metadata, samples_final)
for i in fi:
  pass

if len(buff_meta) > 0: 
  print 'writing last set of meta'
  if not os.path.isdir(collection_14_01_meta+"/".join(curr_file_index.split('/')[:-1])):
    os.mkdir(collection_14_01_meta+"/".join(curr_file_index.split('/')[:-1]))
  writer = open(collection_14_01_meta+curr_file_index,'w')
  decoder.encodeEntry(writer.write,buff_meta)
  writer.close()
  if args.mzip:
    subprocess.call(["7z",'a','-mx9',collection_14_01_meta+curr_file_index+".7z",collection_14_01_meta+curr_file_index])
    os.remove(collection_14_01_meta+curr_file_index)
  if args.store_content:
    print 'writing last set of content'
    if not os.path.isdir(args.content_path+"/".join(next_content_fi.split('/')[:-1])):
        os.mkdir(args.content_path+"/".join(next_content_fi.split('/')[:-1]))
    writer = open(args.content_path+next_content_fi,'w')
    decoder.encodeEntry(writer.write,buff_content)
    writer.close()
    if args.czip:
      subprocess.call(["7z",'a','-mx9',args.content_path+next_content_fi+".7z",args.content_path+next_content_fi])
      os.remove(args.content_path+next_content_fi)
