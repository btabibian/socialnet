PyMySQL==0.6.6
argparse==1.3.0
distribute>=0.6.24
enum34==1.0.4
mediawiki-utilities==0.4.13
memory-profiler==0.32
objgraph==1.8.1
numpy==1.9.0 
#protobuf==3.0.0a3.dev0
protobuf3==0.2.0
psutil==2.2.1
requests==2.6.0
wsgiref>=0.1.2
