import spinn3rApi
from spinn3rApi import wikipedia_pb2
from spinn3rApi import decoder
from wikidatasets import utils,extractLink
import os
import pickle
import re
from collections import OrderedDict
import numpy as np
from dateutil import parser,relativedelta
import datetime
import logging
import time

def checkSanity(start,end):
  if (end-start>0) or (end == -1):
    #print(end,start)
    return True
  else:
    logging.warning("sanity check failed")
    return False

def conc_array(sets):
  output = []
  for set_ in zip(*sets):
    output.append(np.concatenate(set_))
  return tuple(output)

def convert_dict(set_tuple):
  return {'nodes_survived':set_tuple[0],'times_survived':set_tuple[1],'sites_survived':set_tuple[2], 'nodes':set_tuple[4],'times':set_tuple[5],'sites':set_tuple[6]}

def process_document(items,items_survived, title, start, index, end_study,jitter):
  # if empty return null
  if len(items) == 0 and len(items_survived)==0:
    raise Exception("Empty document")
  # definitions
  total_length = len(items)+len(items_survived)
  nodes = np.zeros((len(items),2),dtype = 'int32')
  starts = np.zeros(len(items),dtype = 'int64')
  nodes[:,0] = index
  nodes[:,1] = 0
  starts[:] = start
  times = np.zeros((len(items),2))
  sites = np.zeros((len(items),1),dtype = 'int64')

  nodes_survived = np.zeros((len(items_survived),2),dtype = 'int32')
  starts_survived = np.zeros(len(items_survived),dtype = 'int64')
  #print(index)
  nodes_survived[:,0] = index
  nodes_survived[:,1] = 0
  starts_survived[:] = start
  times_survived = np.zeros((len(items_survived),2))
  sites_survived = np.zeros((len(items_survived),1),dtype = 'int64')

  # converting times
  start = parser.parse(str(start))
  def convert_to_int(val):
   diff = (parser.parse(str(val))-start).total_seconds()/(60*60*24)
   if diff <0:
     raise Exception("Invalid diff %s , %s, diff: %f " % (val,start,diff))
   return diff
  items_array = np.array(items) if len(items) > 0 else np.zeros((0,4))
  items_survived_array = np.array(items_survived) if len(items_survived) > 0 else np.zeros((0,4))
  #print(index,title,len(items),len(items_survived))
  items_survived_array[:,3] = end_study
  convert = np.vectorize(convert_to_int)
  if len(items):
    times[:,0] = convert(items_array[:,2])
    times[:,1] = convert(items_array[:,3])
    sites = (items_array[:,1])[:,np.newaxis]
  if len(items_survived):
    times_survived[:,0] = convert(items_survived_array[:,2])
    times_survived[:,1] = convert(items_survived_array[:,3])
    sites_survived = (items_survived_array[:,1])[:,np.newaxis]
  if jitter:
    times_survived[:,1] = times_survived[:,0]+np.random.uniform(0,0.0001,times_survived.shape[0])
    times[:,1] = times[:,0]+np.random.uniform(0,0.0001,times.shape[0])
  return (nodes_survived,times_survived,sites_survived,starts_survived,nodes,times,sites,starts)

def loadFile(fi,count=None,min_count=None,min_items=1,predict=0, end_study=None, arrival=False,jitter=False, min_sites=0,lda_matrix=None,lda_titles=None):
  if predict == 1:
    raise Exception("prediction is not yet supported")

  data = pickle.load(open(fi,'rb'))
  sites = data['sites']
  min_count = 0 if min_count is None else min_count
  errors = 0
  current_list = []
  current_list_survived = []
  current_title = None
  titles = []
  current_start = None
  output = []

  lda_out = []
  sites_items = list(sites.items())
  sorted_index_sites = list(reversed(sorted(range(len(sites)), key=lambda x:int(sites_items[x][1]))))
  map_old_new = dict(zip(sorted_index_sites,range(len(sites))))
  print(min_sites)
  if lda_matrix is not None:
    lda_title_dict = dict(zip(lda_titles.tolist(),np.arange(lda_titles.shape[0])))
  #sites_ = [(key,sites[key]) for key in sites.keys()]
  new_sites = [sites_items[ind] for ind in sorted_index_sites]
  new_sites_filtered = list(filter(lambda x: x[1]>=min_sites,new_sites))
  print("new len %d" % len(new_sites_filtered))
  starts = []
  # if no study_end is provided iterate over data once and find latest date
  if end_study is None:
    if 'end_time' in data:
      end_study = data['end_time']
    else:
      end_study = -1
      for li in data['data']:
        if li[2] > end_study:
          end_study = li[2]
        if li[3] > end_study:
          end_study = li[3]
      print("end study %d" % end_study)
  starts_list = None
  if 'start_time' in data:
    starts_list = data['start_time']
  active = -1
  for ind,li in enumerate(data['data']):
    dat_ = li[:-1]
    dat = (dat_[0],map_old_new[dat_[1]],dat_[2],dat_[3])
    title = li[-1]
    if checkSanity(dat[2],dat[3]) is False:
      errors += 1
      #print("errors")
      continue
    if current_title == title:
      if new_sites[dat[1]][1]<min_sites:
          continue
      if starts_list is None:
        if current_start > dat[2]:
          current_start = dat[2]
      if dat[3] == -1:
        current_list_survived.append(dat)
      else:
        current_list.append(dat)
    else:
      #print("changing %s, %s"% (current_title,title))
      if (title not in titles):
        if current_title is not None:
          discard = False
          if len(current_list)+len(current_list_survived) < min_items:
            print("discarding %s" % current_title)
            discard = True
          lda_ind = -1
          if lda_matrix is not None:
            if current_title in lda_title_dict:
              lda_ind = lda_title_dict[current_title]
            else:
              discard=True
            #if lda_ind.shape[0]==0:
            #  discard = True
            #else:
            #  lda_ind = lda_ind[0]
          if discard is False:
            lda_out.append(lda_ind)
            titles.append(current_title)
            starts.append(current_start)
            result = process_document(current_list,current_list_survived,current_title,current_start,active,end_s,jitter)
            output.append(result)
            if ind % 100000 == 0:
              print("processed %s" % active)
          else:
            active-=1
        #print("Going from %s to %s" %(current_title,title))
        current_list = []
        current_list_survived = []
        current_title = title
        current_start = starts_list[dat[0]] if starts_list is not None else dat[2]
        end_s = end_study[dat[0]] if type(end_study) is list else end_study
        active += 1
        ## clear everything and start again
        #print(li[0],count,errors)
        if li[0] < min_count:
          continue
        #if len(current_list)+len(current_list_survived)>3:
        #  continue
        #print(li[0],count)
        if new_sites[dat[1]][1]<min_sites:
          continue
        if (count is not None) and (active > count):
          break
        if dat[3] == -1:
          current_list_survived.append(dat)
        else:
          current_list.append(dat)
      else:
        print("% already in our dataset" % title)
  #last batch
  discard = False
  if len(current_list)+len(current_list_survived) < min_items:
    discard = True
  lda_ind = -1
  if lda_matrix is not None:
    if current_title in lda_title_dict:
      lda_ind = lda_title_dict[current_title]
    else:
      discard=True
  if not discard:
    lda_out.append(lda_ind)
    starts.append(current_start)
    end_s = end_study[dat[0]] if type(end_study) is list else end_study
    result = process_document(current_list,current_list_survived,current_title,current_start,active,end_s,jitter)
    if result is not None:
      output.append(result)
      titles.append(current_title)
  else:
    active-=1
  #merge and return
  final = conc_array(output)
  final = {'nodes_survived':final[0],'times_survived':final[1],'sites_survived':final[2], 'nodes':final[4],'times':final[5],'sites':final[6]}

  if arrival:
    final_arrival = list(map(convert_dict,output))
    return final,new_sites_filtered,np.asarray(titles),starts,end_study,lda_out,final_arrival
  else:
    return final,new_sites_filtered,np.asarray(titles),starts,end_study,lda_out
