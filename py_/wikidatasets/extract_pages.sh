source venv_new/bin/activate
mkdir ./full_set/dataset.$1
python -m wikidatasets.wiki_extract_list -i /lustre/shared/wikipedia_20140707/ -l wikidatasets/news_articles.csv -s 150 -o ./full_set/dataset.$1 --sep '\t' --logging I --column 1 --input_list ./wikidatasets/files_list --step 100 --index $1 --logging I
deactivate
