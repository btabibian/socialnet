import spinn3rApi
from spinn3rApi import wikipedia_pb2
from spinn3rApi import decoder
from wikidatasets import utils,extractLink
import os
import pickle
import re
from collections import OrderedDict
import numpy as np
from dateutil import parser,relativedelta
import datetime
import logging

def loadFile(fi,count=None,min_count=None,min_items=0,predict=1):
  data = pickle.load(open(fi,'rb'))
  sites = data['sites']
  dat = np.array([li[:-1] for li in data['data']])
  titles = np.array([li[-1] for li in data['data']])
  #Check if data is consistent
  if (dat.shape[0] - np.logical_or((dat[:,3]-dat[:,2])>0,dat[:,3] == -1).sum()) !=0:
    logging.warning("Invalid data: %d" % (dat.shape[0] - np.logical_or((dat[:,3]-dat[:,2])>0,dat[:,3] == -1).sum()))
    titles = titles[np.logical_or((dat[:,3]-dat[:,2])>0,dat[:,3] == -1)]
    dat = dat[np.logical_or((dat[:,3]-dat[:,2])>0,dat[:,3] == -1),:]
  if count is not None:
    print(count)
    logging.info('end document %d' % (count-1))
    titles = titles[dat[:,0]<count]
    dat = dat[dat[:,0]<count,:]
  if min_count is not None:
    logging.info('start document %d' % (min_count+1))
    titles = titles[dat[:,0]>min_count]
    dat = dat[dat[:,0]>min_count,:]
  k = 0
  ## Removing duplicate
  dat[:,0] = -dat[:,0] -1
  for ti in np.unique(titles):
    d = dat[titles == ti,0][0]
    # check if there should be some minimum number of items for each document
    if min_items > 0:
      if (dat[:,0] == d).sum()<min_items:
        continue
    dat[dat[:,0] == d,0] = k
    k = k+1
  titles = titles[dat[:,0]>-1]
  dat = dat[dat[:,0] > -1,:]
  print(dat.shape)
  _sites =  list(sites.keys())[:dat[:,1].max()+1]
  sites_ = [(key,sites[key]) for key in _sites]
  nodes = np.zeros((dat.shape[0],2),dtype = 'int32')
  starts = np.zeros(np.max(dat[:,0])+1,dtype = 'int64')
  survived = np.zeros((dat.shape[0]),dtype = 'bool')
  nodes[:,0] = dat[:,0]
  ind = dat[:,3] == -1
  nodes[:,1] = 0
  times = np.zeros((dat.shape[0],2))
  ends_item = np.zeros(dat.shape[0],dtype = 'int64')
  starts_item = np.zeros(dat.shape[0],dtype = 'int64')
  #storing start of document per item
  starts_doc_item = np.zeros(dat.shape[0],dtype = 'int64')-1
  T = np.zeros((dat.shape[0],1))
  T_ = np.max(dat[:,2:3])
  titles_d = np.zeros(np.max(dat[:,0])+1,dtype = titles.dtype)
  for d in range(np.max(dat[:,0])+1):
    logging.info('reading document %d' % d)
    dat_d = dat[dat[:,0] == d,:]
    times_d = times[dat[:,0] == d,:]
    T_d = T[dat[:,0] == d,:]
    survived[dat[:,0] == d] = dat_d[:,3] == -1
    if dat_d.shape[0] == 0:
     continue
    start_int = np.min(dat_d[:,2])
    titles_d[d] = titles[dat[:,0] == d][0]
    starts[d] = start_int
    start = parser.parse(str(start_int))
    def convert_to_int(val):
      diff = (parser.parse(str(val))-start).total_seconds()/(60*60*24)
      if diff <0:
        raise Exception("Invalid diff %s , %s " % (val,start))
      return diff

    convert = np.vectorize(convert_to_int)
    dat_d[dat_d[:,3]==-1,3] = T_+ 1
    times[dat[:,0] == d,0] = convert(dat_d[:,2])
    times[dat[:,0] == d,1] = convert(dat_d[:,3])

    #Keeping track of dates for future use in training/predict split
    ends_item[dat[:,0] == d] = dat_d[:,3]
    starts_item[dat[:,0] == d] = dat_d[:,2]
    if start_int==0:
      raise Exception("Start time is zero!")
    starts_doc_item[dat[:,0] == d] = start_int

    convert = np.vectorize(convert_to_int)
    dat_d[dat_d[:,3]==-1,3] = T_+ 1
    times[dat[:,0] == d,0] = convert(dat_d[:,2])
    times[dat[:,0] == d,1] = convert(dat_d[:,3])

  if predict > 0:
    #convert last training day into iso format
    train_end = int((parser.parse(str(T_))-relativedelta.relativedelta(days=predict)).isoformat().replace("-","").replace("T","").replace(":",""))
    training_index = starts_item<train_end
    train_times = np.copy(times[training_index,:])
    train_survived = np.copy(survived[training_index])
    train_nodes = nodes[training_index]


    train_starts_item = starts_item[training_index]
    train_ends_item = ends_item[training_index]
    train_starts_doc_item = starts_doc_item[training_index]
    def lengthRecompute(end, start):
      diff = (parser.parse(str(end))-parser.parse(str(start))).total_seconds()/(60*60*24)
      if diff <0:
        raise Exception("Invalid diff %s , %s " % (end,start))
      return diff
    lengthRec = np.vectorize(lengthRecompute)
    train_times[train_ends_item>train_end,1] = lengthRec(train_end,train_starts_doc_item[train_ends_item>train_end])
    train_survived[train_ends_item>train_end] = 1
    train_sites = dat[training_index,1]
    sorted = np.argsort(train_ends_item)
    arr = (np.arange(nodes.shape[0])[training_index])[train_ends_item>train_end]
    predict_reliable = np.zeros(nodes.shape[0],dtype='bool')
    predict_reliable[arr] = True
    return {'train': {'nodes':train_nodes[sorted,:],'times':train_times[sorted,:],'sites':train_sites[sorted,np.newaxis],'survived':train_survived[sorted],'original_order':np.argsort(sorted)},
            'orig': {'nodes':nodes,'times':times,'sites':np.reshape(dat[:,1],(dat[:,1].shape[0],1)),'survived':survived},
            'predict_arrival': np.logical_not(training_index),
            'predict_reliable': predict_reliable,
            },sites_,titles_d,starts
  return {'nodes':nodes,'times':times,'sites':np.reshape(dat[:,1],(dat[:,1].shape[0],1)),'survived':survived},sites_,titles_d,starts
