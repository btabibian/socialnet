import pickle
from collections import OrderedDict
import argparse
import logging
import datetime

parser = argparse.ArgumentParser(description='merge vector files into one.')
parser.add_argument('-i','--input', type=str, required=True,
                   help = 'Input directory.')
parser.add_argument('-o','--output', type=str, required = True,
                   help = 'Output file')
parser.add_argument('--logging',default='I',type=str,help = 'logging level, can be [W]arning,[E]rror,[D]ebug,[I]nfo,[C]ritical')
parser.add_argument('--logging_dir',default='./experiments/',help='path for storing log files')
parser.add_argument('--step',default=None,type=int)
parser.add_argument('--index',default=0,type=int)

def update_dict_sites(dict_,sites):
  for site, count in sites.items():
    if site in dict_:
      dict_[site] += count
    else:
      dict_[site] = count
def update_index_append(sites_new,data_new,site,base_document):
  result = []
  current_title = ""
  current_index = base_document-1
  sites_old = list(sites_new.items())
  site_index = list(site.keys())
  for item in data_new:
    if current_title != item[0]:
      current_title = item[0]
      current_index = current_index + 1
      #if current_index % 10000 == 0:
      logging.info("processing index %d, title %s, len %d" % (current_index, current_title,len(result)))
    res = (current_index,site_index.index(sites_old[item[1]][0]),item[2],item[3],item[4],item[5])
    result.append(res)
  return result,current_index
  
def loadFile(fi_list,count=None,min_count=None,min_items=0,predict=1):
  agg_sites = OrderedDict()
  final_set = []
  base_index = -1
  for fi in fi_list:
    data_complete = pickle.load(open(fi,'rb'))
    data = data_complete['data']
    sites_ = data_complete['sites']
    update_dict_sites(agg_sites,sites_)
    res = update_index_append(sites_,data,agg_sites,base_index)
    base_index = res[1]
    final_set.extend(res[0])
  return final_set,agg_sites,base_index  

if __name__ == "__main__":
  import os
  args = parser.parse_args()
  level = logging.WARNING
  if args.logging == 'D':
    level = logging.DEBUG
  if args.logging == 'I':
    level = logging.INFO
  if args.logging == 'W':
    level = logging.WARNING
  if args.logging == 'E':
    level = logging.ERROR
  if args.logging == 'C':
    level = logging.CRITICAL
  logging.basicConfig(filename=os.path.join(args.logging_dir,"wiki_merge_"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+".log"),level=level)
  if args.step is not None:
    fi_list = list(map(lambda x: os.path.join(args.input,x),os.listdir(args.input)))
    files = fi_list[args.index*args.step:(args.index+1)*args.step]
    print('starting %d, step %d, first file %s, last file %s' %(args.index,args.step,files[0],files[-1]))
  else:
    files = list(map(lambda x: os.path.join(args.input,x),os.listdir(args.input)))
  set_,sites,index = loadFile(files)
  output = open(args.output, 'wb')
  pickle.dump({"data":set_,"sites":sites}, output)
