'''
Created on Mar 1, 2015

@author: btabibian
'''
import itertools

def scanLeft(iterable,init):
  "s -> (init,s0),(s0,s1), (s1,s2), (s2, s3), ..."
  pre = init
  for elem in iterable:
    yield (pre,elem)
    pre = elem