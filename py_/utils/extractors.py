import zlib
from bs4 import BeautifulSoup
from array import array

def extractLink(orig):
  len_original = len(orig.SerializeToString())
  
  if orig.permalink_entry.content.encoding == "":
    data = ""
  else:
    data = zlib.decompress(orig.permalink_entry.content.data)
  bs = BeautifulSoup(data.decode('utf-8'),"lxml")
  links = map(lambda x:x['href'],bs.find_all('a', href=True))
  return links
